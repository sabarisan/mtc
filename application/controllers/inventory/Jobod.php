<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jobod extends CI_Controller {

	public function __construct()
	{
	    parent::__construct();
		
	}
	
	public function index()
	{
		$data['prsntbrdcrmb'] = "Job Order Management";
		$this->load->view('admin/jobodr',$data);
	}
	
		
}
