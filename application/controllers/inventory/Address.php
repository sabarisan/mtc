<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Address extends CI_Controller {

	public function __construct()
	{
	    parent::__construct();
	    $this->load->model('inventory/Address_model');
	}
	
	public function index(){
		$data['prsntbrdcrmb'] = 'Address Management';
		$this->load->view('inventory/address',$data);
	}

	public function datatbls(){
		$columns = array(	0 => 'id',
						 	// 1 => 'code',
							1 => 'street',
							2 => 'location',
							3 => 'landmark',
							4 => 'city',
							5 => 'dist',
							6 => 'state',
							7 => 'pincode',
							8 => 'country',
						);
		$limit = $this->input->post('length');
		$start = $this->input->post('start');
		
		
		$order = $columns[$this->input->post('order')[0]['column']];
		$dir = $this->input->post('order')[0]['dir'];

		$totalData = $this->Address_model->allposts_count();

		// print_r($totalData);

		$totalFiltered = $totalData;

		if(empty($this->input->post('search')['value']))
		{            
			$posts = $this->Address_model->allposts($limit,$start,$order,$dir);
		}
		else
		{
			$search = $this->input->post('search')['value'];
			$posts =  $this->Address_model->posts_search($limit,$start,$search,$order,$dir);
			$totalFiltered = $this->Address_model->posts_search_count($search);
		}

		// echo '<pre>';	print_r($posts);	echo '</pre>';
		// exit;
		$data = array();
		if(!empty($posts))
		{	
			$i=1;
			foreach ($posts as $post)
			{
				
				$nestedData['id'] = $i;
				// $nestedData['code'] = $post->code;
				$nestedData['street'] = $post->street;
				$nestedData['location'] = $post->location;
				$nestedData['landmark'] = $post->landmark;
				$nestedData['city'] = $post->city;
				$nestedData['dist'] = $post->dist;
				$nestedData['state'] = $post->state;
				$nestedData['pincode'] = $post->pincode;
				$nestedData['country'] = $post->country;
				// $nestedData['createddate'] = date('j M Y h:i a',strtotime($post->createddate));
				$nestedData['remove'] = '<div><center><i class="btn btn-danger glyphicon glyphicon-trash" style="font-size:12px;text-transform:lowercase;" onclick="addrmv(\''.$post->id.'\')"></i> <i class="btn btn-success glyphicon" style="font-size:12px;text-transform:lowecase;" onclick="addrupdte(\''.$post->code.'\',\''.$post->id.'\',\''.$post->street.'\',\''.$post->location.'\',\''.$post->landmark.'\',\''.$post->city.'\',\''.$post->dist.'\',\''.$post->state.'\',\''.$post->country.'\',\''.$post->pincode.'\');"><i class="glyphicon glyphicon-pencil" style="font-size:12px;"></i></i></center></div>';
				$data[] = $nestedData;
				//'\''.$post->depotname.'\'';
				$i++;
			}


		}



		$json_data = array(
					"draw"            => intval($this->input->post('draw')),
					"recordsTotal"    => intval($totalData),
					"recordsFiltered" => intval($totalFiltered),
					"data"            => $data
					);
		echo json_encode($json_data);
	}


	// Activity Update
		public function updteaddr(){

    		$street   = $this->input->post('stret');
    		$rwid     = $this->input->post('rwid');
    		$location = $this->input->post('loc');
    		$lndmrk   = $this->input->post('lndmrk');
    		$cty      = $this->input->post('cty');
    		$distrct  = $this->input->post('distrct');
    		$state    = $this->input->post('state');
    		$pin      = $this->input->post('pin');
    		$slct     = $this->input->post('slct');
			
			$data = array(
				'street'   => $street,
				'location' => $location,
				'landmark' => $lndmrk,
				'city'     => $cty,
				'dist'     => $distrct,
				'state'    => $state,
				'pincode'  => $pin,
				'country'  => $slct,
				);   		
			
			$depotupdte = $this->Address_model->updtaddr($rwid,$data);
		}
	// Activity Update Ending

	//  Activity Insert
		public function addrins(){
			// print_r($_POST);

			 // [actvcd] => dasdad [actvnme]

			 $street   = $this->input->post('stret');
    		$rwid     = $this->input->post('rwid');
    		$location = $this->input->post('loc');
    		$lndmrk   = $this->input->post('lndmrk');
    		$cty      = $this->input->post('cty');
    		$distrct  = $this->input->post('distrct');
    		$state    = $this->input->post('state');
    		$pin      = $this->input->post('pin');
    		$slct     = $this->input->post('slct');
			
			$data = array(
				'street'   => $street,
				'location' => $location,
				'landmark' => $lndmrk,
				'city'     => $cty,
				'dist'     => $distrct,
				'state'    => $state,
				'pincode'  => $pin,
				'country'  => $slct,
				);   		

			 $actvtins = $this->Address_model->addrins($data);

			 redirect('inventory/Address/', 'refresh');

		}
	// Activity Insert Ending


		// Remove Depot
	public function rmvaddr($id){
		if (isset($id)) {
			$revmodept = $this->Address_model->rmvaddr($id);
		}
	}
	// Remove Depot Ended

}


