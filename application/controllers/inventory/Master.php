<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master extends CI_Controller {

	public function __construct()
	{
	    parent::__construct();
		
		if(!$this->session->userdata('userid') || $this->session->userdata('userid')=='')
		{
			redirect(base_url().'admin/login');
		}
		$this->load->model('admin/master_model');
	}
	
	public function index()
	{
		//$this->load->view('login');
		$param	=	'@p0=0';
		$sp		=	'CALL Adm_Brand_Get ('.$param.')';
		
		$query	=	$this->db->query($sp);
		
		if($query)
		{
			if($query->num_rows()>0)
			{
				foreach($query->result_array() as $row)
				{
					echo $row['id'].' - '.$row['brandname'].'<br/>';
				}
			}
		}
		
	}
	
	
	function updateDelete($table=NULL,$checkcolumn=NULL,$checkvalue=NULL,$updatecolumn=NULL,$updatevalue=NULL,$currentvalue=NULL)
	{
		$table			=	'master_category';
		$checkcolumn	=	'id';
		$checkvalue		=	'1';
		$updatecolumn	=	'status';
		$updatevalue	=	'1';
		$currentvalue	=	'0';
		$updateDelete	=	$this->master_model->updateDelete($table,$checkcolumn,$checkvalue,$updatecolumn,$updatevalue,$currentvalue);
		//echo $updateDelete;
	}
	
	function updateActivatestatus($table=NULL,$checkcolumn=NULL,$checkvalue=NULL,$updatecolumn=NULL,$updatevalue=NULL,$currentvalue=NULL)
	{
		$table				=	'master_category';
		$checkcolumn		=	'id';
		$checkvalue			=	'2';
		$updatecolumn		=	'activestatus';
		$updatevalue		=	'1';
		$currentvalue		=	'0';
		$updateActivatestatus	=	$this->master_model->updateActivatestatus($table,$checkcolumn,$checkvalue,$updatecolumn,$updatevalue,$currentvalue);
		//echo $updateActivatestatus;
	}
	
	//  Add Category
	function categoryAddData()
	{
		//$_POST['categoryname']	=	'ddd';
		//$_POST['description']	=	'fdgdfgdgd';
		
		if(isset($_POST))
		{
			if($_POST['categoryname']!='' && $_POST['description']!='')
			{
				// Insert
				$categoryAddData	=	$this->master_model->categoryAddData($_POST);
				
				if($categoryAddData)
				{
					$response['status']	=	1;
					$response['message']=	'Category Successfully Created';
				}
				else
				{
					$response['status']	=	0;
					$response['message']=	'Failed. Try again';
				}
				
			}
			else
			{
				// Please Enter Category Name & Description
				$response['status']	=	0;
				$response['message']=	'Please Enter Category Name & Description';
			}
		}
		else
		{
			// No Post Parameter
			$response['status']	=	0;
			$response['message']=	'Please Enter Category Name & Description';
		}
		//echo '<pre>';	print_r($response);	echo '</pre>';
		// Redirect to URL
		$this->session->set_userdata('status',$response['status']);
		$this->session->set_userdata('message',$response['message']);
	}
	
	
	//  Update Category
	function categoryUpdateData()
	{
		//$_POST['categoryname']	=	'retrerere';
		//$_POST['description']	=	'cccccccccccccccc';
		//$_POST['categoryid']	=	'1';
		
		if(isset($_POST))
		{
			if($_POST['categoryname']!='' && $_POST['categoryname']!='' && $_POST['categoryid']!='')
			{
				// Insert
				$categoryUpdateData		=	$this->master_model->categoryUpdateData($_POST);
				if($categoryUpdateData)
				{
					$response['status']	=	1;
					$response['message']=	'Category Successfully Updated';
				}
				else
				{
					$response['status']	=	0;
					$response['message']=	'Failed. Try again';
				}
			}
			else
			{
				// Please Enter Category Name & Description
				$response['status']	=	0;
				$response['message']=	'Please Enter Category Name & Description';
			}
		}
		else
		{
			// No Post Parameter
			$response['status']	=	0;
			$response['message']=	'Please Enter Category Name & Description';
		}
		//echo '<pre>';	print_r($response);	echo '</pre>';
		// Redirect to URL
		$this->session->set_userdata('status',$response['status']);
		$this->session->set_userdata('message',$response['message']);
	}
	
	
	function categoryGetData($id=NULL)
	{
		
	}
	
}
