<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Activity extends CI_Controller {

	public function __construct()
	{
	    parent::__construct();
	    $this->load->model('inventory/Activity_model');
	}
	
	public function index(){
		$data['prsntbrdcrmb'] = "Activity Management";
		$this->load->view('inventory/activty',$data);
	}

	public function datatbls(){
		$columns = array(	0 => 'id',
						 	1 => 'activitycode',
							2 => 'activityname',
							3 => 'createddate',
						);
		$limit = $this->input->post('length');
		$start = $this->input->post('start');
		
		
		$order = $columns[$this->input->post('order')[0]['column']];
		$dir = $this->input->post('order')[0]['dir'];

		$totalData = $this->Activity_model->allposts_count();

		$totalFiltered = $totalData;

		if(empty($this->input->post('search')['value']))
		{            
			$posts = $this->Activity_model->allposts($limit,$start,$order,$dir);
		}
		else
		{
			$search = $this->input->post('search')['value'];
			$posts =  $this->Activity_model->posts_search($limit,$start,$search,$order,$dir);
			$totalFiltered = $this->Activity_model->posts_search_count($search);
		}

		//echo '<pre>';	print_r($posts);	echo '</pre>';
		//exit;
		$data = array();
		if(!empty($posts))
		{	
			$i=1;
			foreach ($posts as $post)
			{
				
				$nestedData['id'] = $i;
				$nestedData['activitycode'] = $post->activitycode;
				$nestedData['activityname'] = substr(strip_tags($post->activityname),0,50)."...";
				$nestedData['createddate'] = date('j M Y h:i a',strtotime($post->createddate));
				$nestedData['remove'] = '<div><center><i class="btn btn-danger glyphicon glyphicon-trash" style="font-size:12px;text-transform:lowercase;" onclick="actvtrmv(\''.$post->id.'\')"></i> <i class="btn btn-success glyphicon" style="font-size:12px;text-transform:lowecase;" onclick="actvtupdte(\''.$post->id.'\',\''.$post->activitycode.'\',\''.$post->activityname.'\');"><i class="glyphicon glyphicon-pencil" style="font-size:12px;"></i></i></center></div>';
				$data[] = $nestedData;
				//'\''.$post->depotname.'\'';
				$i++;
			}


		}



		$json_data = array(
					"draw"            => intval($this->input->post('draw')),
					"recordsTotal"    => intval($totalData),
					"recordsFiltered" => intval($totalFiltered),
					"data"            => $data
					);
		echo json_encode($json_data);
	}


	// Activity Update
		public function updteactvt(){
			// print_r($_POST);

			$rwid = $this->input->post('rwid');
    		$actvnme = $this->input->post('actvnme');
			
			$data = array(
				'activityname' => $actvnme,
				);   		
			
			$depotupdte = $this->Activity_model->updtactvt($rwid,$data);
		}
	// Activity Update Ending

	//  Activity Insert
		public function actvtins(){
			// print_r($_POST);

			 // [actvcd] => dasdad [actvnme]

			 $actvcd = $this->input->post('actvcd');
			 $actvnme = $this->input->post('actvnme');

			 $data = array(
			 	'activitycode' => $actvcd,
			 	'activityname' => $actvnme
			 	);

			 $actvtins = $this->Activity_model->actvtins($data);

			 redirect('inventory/Activity/', 'refresh');

		}
	// Activity Insert Ending


		// Remove Depot
	public function rmvactvt($id){
		if (isset($id)) {
			$revmodept = $this->Activity_model->rmvactvt($id);
		}
	}
	// Remove Depot Ended

}


