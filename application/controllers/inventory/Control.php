<?php 
defined("BASEPATH") OR exit('No direct script access allowed');


class Control extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('inventory/Control_model');
	}

	public function index(){
		$data['prsntbrdcrmb'] = "Control Management";
		$this->load->view('inventory/ctrlmgmt',$data);
	}

	public function datatbls(){
		$columns = array(	0 => 'id',
						 	1 => 'type',
							2 => 'descrip',
							3 => 'foliogroup',
						);
		$limit = $this->input->post('length');
		$start = $this->input->post('start');
		
		
		$order = $columns[$this->input->post('order')[0]['column']];
		$dir = $this->input->post('order')[0]['dir'];

		$totalData = $this->Control_model->allposts_count();

		$totalFiltered = $totalData;

		if(empty($this->input->post('search')['value']))
		{            
			$posts = $this->Control_model->allposts($limit,$start,$order,$dir);
		}
		else
		{
			$search = $this->input->post('search')['value'];
			$posts =  $this->Control_model->posts_search($limit,$start,$search,$order,$dir);
			$totalFiltered = $this->Control_model->posts_search_count($search);
		}

		//echo '<pre>';	print_r($posts);	echo '</pre>';
		//exit;
		$data = array();
		if(!empty($posts))
		{	
			$i=1;
			foreach ($posts as $post)
			{
				
				$nestedData['id'] = $i;
				$nestedData['type'] = $post->type;
				$nestedData['descrip'] = $post->description;
				$nestedData['foliogroup'] = $post->foliogroup;
				$nestedData['createddate'] = date('j M Y h:i a',strtotime($post->createddate));
				$nestedData['remove'] = '<div><center><i class="btn btn-danger glyphicon glyphicon-trash" style="font-size:12px;text-transform:lowercase;" onclick="ctrlrmv(\''.$post->id.'\')"></i> <i class="btn btn-success glyphicon" style="font-size:12px;text-transform:lowecase;" onclick="ctrlupdte(\''.$post->id.'\',\''.$post->type.'\',\''.$post->description.'\',\''.$post->foliogroup.'\');"><i class="glyphicon glyphicon-pencil" style="font-size:12px;"></i></i></center></div>';
				$data[] = $nestedData;
				//'\''.$post->depotname.'\'';
				$i++;
			}


		}



		$json_data = array(
					"draw"            => intval($this->input->post('draw')),
					"recordsTotal"    => intval($totalData),
					"recordsFiltered" => intval($totalFiltered),
					"data"            => $data
					);
		echo json_encode($json_data);
	}


	// Activity Update
		public function updtectrl(){


			$rwid = $this->input->post('ctrlid');
    		$descrip = $this->input->post('descrip');
    		$folgrp = $this->input->post('folgrp');
    		$type = $this->input->post('type');
				
			$data = array(
				'foliogroup' => $folgrp,
				'type'  => $type,
				'description'  => $descrip,
				);   		
			
			$capupdte = $this->Control_model->updtctrl($rwid,$data);

			echo "1";
		}
	// Activity Update Ending

	//  Activity Insert
		public function ctrlins(){
			// print_r($_POST);
			// exit;
			 // [actvcd] => dasdad [actvnme]
			$descrip = $this->input->post('descrip');
    		$folgrp = $this->input->post('folgrp');
    		$type = $this->input->post('type');
				
			$data = array(
				'foliogroup' => $folgrp,
				'type'  => $type,
				'description'  => $descrip,
				);   		

			 $actvtins = $this->Control_model->ctrlins($data);

			 redirect('inventory/Control/', 'refresh');

		}
	// Activity Insert Ending


		// Remove Depot
	public function rmvctrl($id){
		if (isset($id)) {
			$revmoctrl = $this->Control_model->rmvctrl($id);
		}
	}
	// Remove Depot Ended

}

 ?>