<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Depot extends CI_Controller {

	public function __construct()
	{
	    parent::__construct();
		$this->load->model("inventory/Depot_model");
	}
	
	public function index()
	{
		$data['prsntbrdcrmb'] = "Depot Management";
		$this->load->view('inventory/depot',$data);
	}


	public function posts()
	{
		

		$columns = array(	0 => 'id',
						 	1 => 'depotid',
							2 => 'depotname',
							3 => 'depotopt',
							4 => 'createddate',
						);
		$limit = $this->input->post('length');
		$start = $this->input->post('start');
		
		
		$order = $columns[$this->input->post('order')[0]['column']];
		$dir = $this->input->post('order')[0]['dir'];

		$totalData = $this->Depot_model->allposts_count();

		$totalFiltered = $totalData;

		if(empty($this->input->post('search')['value']))
		{            
			$posts = $this->Depot_model->allposts($limit,$start,$order,$dir);
		}
		else
		{
			$search = $this->input->post('search')['value'];
			$posts =  $this->Depot_model->posts_search($limit,$start,$search,$order,$dir);
			$totalFiltered = $this->Depot_model->posts_search_count($search);
		}
		$data = array();
		if(!empty($posts))
		{	
			$i=1;
			foreach ($posts as $post)
			{
				
				$nestedData['id'] = $i;
				$nestedData['depotid'] = $post->depotid;
				$nestedData['depotname'] = $post->depotname;
				$nestedData['depotopt'] = substr(strip_tags($post->depotopt),0,50)."...";
				$nestedData['createddate'] = date('j M Y h:i a',strtotime($post->createddate));
				$nestedData['remove'] = '<div><center><i class="btn btn-danger glyphicon glyphicon-trash" style="font-size:12px;text-transform:lowercase;" onclick="rmve(\''.$post->id.'\')"></i>  <i class="btn btn-success glyphicon" style="font-size:12px;text-transform:lowecase;" onclick="deptupdte(\''.$post->id.'\',\''.$post->depotid.'\',\''.$post->depotname.'\',\''.$post->depotopt.'\');"><i class="glyphicon glyphicon-pencil" style="font-size:12px;"></i></i></center></div>';
				$data[] = $nestedData;
				//'\''.$post->depotname.'\'';
				$i++;
			}


		}



		$json_data = array(
					"draw"            => intval($this->input->post('draw')),
					"recordsTotal"    => intval($totalData),
					"recordsFiltered" => intval($totalFiltered),
					"data"            => $data
					);
		echo json_encode($json_data);
	}


	public 	function proced(){
			
			$rwid  = $this->input->post('rwid');
			$depid = $this->input->post('depid');
    		$depnme = $this->input->post('depnme');
    		$depopt = $this->input->post('depopt');
			
			if (isset($rwid) && $rwid!="") {
				
				$data = array(
						'depotname' => $depnme,
						'depotopt'  => $depopt,
						);   		
					
					$depotupdte = $this->Depot_model->updtdpte($rwid,$data);
					echo "2";
			}else if($rwid == ""){

					$data = array(
					 	'depotid'    => $depid,
					 	'depotname'  => $depnme,
					 	'depotopt'   => $depopt
					 	);

			        $deptins = $this->Depot_model->depins($data);
			        echo "1";
			}


	}

	// Remove Depot
	public function rmvdpte($id){
		if (isset($id)) {
			$revmodept = $this->Depot_model->rmvdpte($id);
		}
	}
	// Remove Depot Ended

}
