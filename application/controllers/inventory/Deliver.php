<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Deliver extends CI_Controller {

	public function __construct()
	{
	    parent::__construct();
		$this->load->model('inventory/Deliver_model');
	}
	
	public function index()
	{
		$data['prsntbrdcrmb'] = "Delivery Terms Management";
		$this->load->view('inventory/delivry',$data);
	}
	


	public function datatbls(){
		$columns = array(	0 => 'id',
							1 => 'code',
						 	2 => 'descrip',
							3 => 'name1',
							4 => 'createddate',
						);
		$limit = $this->input->post('length');
		$start = $this->input->post('start');
		
		
		$order = $columns[$this->input->post('order')[0]['column']];
		$dir = $this->input->post('order')[0]['dir'];

		$totalData = $this->Deliver_model->allposts_count();

		$totalFiltered = $totalData;

		if(empty($this->input->post('search')['value']))
		{            
			$posts = $this->Deliver_model->allposts($limit,$start,$order,$dir);
		}
		else
		{
			$search = $this->input->post('search')['value'];
			$posts =  $this->Deliver_model->posts_search($limit,$start,$search,$order,$dir);
			$totalFiltered = $this->Deliver_model->posts_search_count($search);
		}

		//echo '<pre>';	print_r($posts);	echo '</pre>';
		//exit;
		$data = array();
		if(!empty($posts))
		{	
			$i=1;
			foreach ($posts as $post)
			{
				
				$nestedData['id'] = $i;
				$nestedData['code'] = $post->code;
				$nestedData['descrip'] = $post->description;
				$nestedData['name1'] = $post->name1;
				$nestedData['createddate'] = date('j M Y h:i a',strtotime($post->createddate));
				$nestedData['remove'] = '<div><center><i class="btn btn-danger glyphicon glyphicon-trash" style="font-size:12px;text-transform:lowercase;" onclick="dlvrrmv(\''.$post->id.'\')"></i> <i class="btn btn-success glyphicon" style="font-size:12px;text-transform:lowecase;" onclick="dlvrupdte(\''.$post->id.'\',\''.$post->code.'\',\''.$post->description.'\',\''.$post->name1.'\');"><i class="glyphicon glyphicon-pencil" style="font-size:12px;"></i></i></center></div>';
				$data[] = $nestedData;
				//'\''.$post->depotname.'\'';
				$i++;
			}


		}



		$json_data = array(
					"draw"            => intval($this->input->post('draw')),
					"recordsTotal"    => intval($totalData),
					"recordsFiltered" => intval($totalFiltered),
					"data"            => $data
					);
		echo json_encode($json_data);
	}


	public function proced(){

			$rwid = $this->input->post('delvrid');
    		$descrip = $this->input->post('descrip');
    		$nme = $this->input->post('nme');
			
			if (isset($rwid) && $rwid!="") {
				
				$data = array(
				'description' => $descrip,
				'name1'  => $nme,
				);   			
				$capupdte = $this->Deliver_model->updtdlvr($rwid,$data);
				echo "2";
			}else{

				$descrip = $this->input->post('descrip');
	    		$nme = $this->input->post('nme');
				
			$data = array(
				'description' => $descrip,
				'name1'  => $nme,
				);   		

			 $dlvrins = $this->Deliver_model->dlvrins($data);
				
				echo "1";

			}

			
	}

	
	// //  Activity Insert
	// 	public function dlvrins(){
	// 		// print_r($_POST);
	// 		// exit;
	// 		 // [actvcd] => dasdad [actvnme]
			
	// 		 redirect('inventory/Deliver/', 'refresh');

	// 	}
	// // Activity Insert Ending


		// Remove Depot
	public function rmvdlvr($id){
		if (isset($id)) {
			$revmoctrl = $this->Deliver_model->rmvdlvr($id);
		}
	}
	// Remove Depot Ended
		
}
