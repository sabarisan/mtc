<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class IndentPurchase extends CI_Controller {

	public function __construct()
	{
	    parent::__construct();
	    $this->load->model('inventory/Address_model');
	    $this->load->model('inventory/Indent_model');
	}
	
	public function index(){
		$data['prsntbrdcrmb'] = 'Indent Purchase Management';
		$data['selct'] = $this->Indent_model->search(); 
		$this->load->view('inventory/indentpurch',$data);
	}

		// Remove Depot
	public function rmvaddr($id){
		if (isset($id)) {
			$revmodept = $this->Address_model->rmvaddr($id);
		}
	}
	// Remove Depot Ended

	// Calendar of purchase view
	public function colp(){
		$data['prsntbrdcrmb'] = 'Calendar of Purchase';
		$this->load->view('inventory/colp',$data);
	}
	// Calendar of purchase view Ending


	public function indnt_chcklst(){
		$data['prsntbrdcrmb'] = 'Indent Checklist';
		$data['chcklist_tabl'] = $this->Indent_model->get_indt_checklst();
		$this->load->view('inventory/ind_chcklst',$data);
	}


	// Auto Suggest value
	// public function getdata()
 //   {
 //      // Get data from db 
 //      $data = $this->Indent_model->search();
	//   echo json_encode($data);

	//   //echo $data['ajaxdata'];


 //   }

}





