<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payment extends CI_Controller {

	public function __construct()
	{
	    parent::__construct();
	    $this->load->model('inventory/Payment_model');
	}
	
	public function index(){
		$data['prsntbrdcrmb'] = "Payment Terms Management";
		$data['pymntmgmt'] = $this->Payment_model->getpymntdtls();
		$this->load->view('inventory/payment',$data);
	}


	public function proced(){
		// print_r($_POST);
		// exit;

	
	$name   = $this->input->post('nme');
	$descrp = $this->input->post('descrp');
	$rwid   = $this->input->post('rwid');

	if (isset($rwid) && $rwid!="") {
			
			$data = array(
				'description'  => $descrp,
				'name1'        => $name,
				);
				$pmntupdte = $this->Payment_model->pmntupdte($rwid,$data);
				    echo "2";
		}else{

			$data = array(
				'description'  => $descrp,
				'name1'        => $name,
				);
				$pmntins = $this->Payment_model->pmntins($data);
					echo "1";
		}

		
	}



	// Payment management Remove
    function rmvpymnt($id){

        $this->db->where('id',$id);
        $query = $this->db->delete('master_paymentterms');

        if ($query) {
             $result['mymsg'] = "Payment Termss Removed Successfully";
        }else{
              $result['mymsg'] = "DB Error please check the connection";
        }
    }
    // Payment management Remove


}


