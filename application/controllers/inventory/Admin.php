<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function __construct()
	{
	    parent::__construct();
		$this->load->model('admin/Login_model');
		$this->load->model('admin/Asset_model');
		// $this->load->model('Login_model');
	}
	
	public function index(){

		$this->load->view('login');
	}

	public function dashboard()
	{


		$menus = $this->Login_model->menus();
				// print_r($menus);
		$this->session->set_userdata('mymenu',$menus);
		 $this->load->view('admin/landing');
	}
	

	public function addrs()
	{
		$data['prsntbrdcrmb'] = 'Address Management';
		$this->load->view('admin/address',$data);
	}


	
}


