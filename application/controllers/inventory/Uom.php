<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Uom extends CI_Controller {

	public function __construct()
	{
	    parent::__construct();
	}
	
	public function index(){
		$data['prsntbrdcrmb'] = "Uom Management";
		$this->load->view('admin/uom_mgmt',$data);
	}
	
}


