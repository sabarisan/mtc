<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tyre extends CI_Controller {

	public function __construct()
	{
	    parent::__construct();
	    $this->load->model('inventory/Tyre_model');
	}
	
	public function index(){
		$data['prsntbrdcrmb'] = "Tyre Management";
		$data['tyrmgmt'] =  $this->Tyre_model->gttyrmgmt();
		$this->load->view('inventory/tyre',$data);
	}


	public function proced(){
		// print_r($_POST);

		$rwid  = $this->input->post('rwid');
		$cde   = $this->input->post('cde');
		$fol   = $this->input->post('fol');
		$tocde = $this->input->post('tocde');
		$dte   = $this->input->post('dte');
		$mtcno = $this->input->post('mtcno');
		$manf  = $this->input->post('manf');
		$mak   = $this->input->post('mak');

	if ($this->session->userdata('status')=="1") {
					  if (isset($rwid) && $rwid!="") {
						$data = array(
							'code'   => $cde,
							'folio'  => $fol,
							'tocode' => $tocde,
							'date'   => $dte,
							'mtcno'  => $mtcno,
							'manf'   => $manf,
							'make'   => $mak
							);
							$pmntupdte = $this->Tyre_model->tyrupdte($rwid,$data);
							if ($pmntupdte) {
								echo "2";
							}
							    
					}else{
						$data = array(
							'code'   => $cde,
							'folio'  => $fol,
							'tocode' => $tocde,
							'date'   => $dte,
							'mtcno'  => $mtcno,
							'manf'   => $manf,
							'make'   => $mak
							);
							$pmntins = $this->Tyre_model->tyrins($data);
								// if ($pmntins) {
									echo "1";	
								// }
								
					}
	  	}
		
	}



	// Payment management Remove
    function rmvtyr($id){

        $this->db->where('id',$id);
        $query = $this->db->delete('master_tyre');

        if ($query) {
             $result['mymsg'] = "Tyre Removed Successfully";
        }else{
              $result['mymsg'] = "DB Error please check the connection";
        }
    }
    // Payment management Remove
	
}


