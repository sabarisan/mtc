<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Store extends CI_Controller {

	public function __construct()
	{
	    parent::__construct();
	    $this->load->model('inventory/Store_model');
	}
	
	public function index(){
		$data['prsntbrdcrmb'] = "Store Management";
		$data['stremgmt']     = $this->Store_model->stregetdta();
		$this->load->view('inventory/store_mgmt',$data);
	}


	public function proced(){
		// print_r($_POST);
		// exit;
    	$rwid      = $this->input->post('rwid');
    	$typ       = $this->input->post('typ');
    	$nme       = $this->input->post('nme');
    	$nme1      = $this->input->post('nme1');
    	$addr1     = $this->input->post('addr1');
    	$addr2     = $this->input->post('addr2');
    	$addr3     = $this->input->post('addr3');
    	$addr4     = $this->input->post('addr4');
    	$pincd     = $this->input->post('pincd');
    	$state     = $this->input->post('state');
    	$stdcde    = $this->input->post('stdcde');
    	$phne1     = $this->input->post('phne1');
    	$phne2     = $this->input->post('phne2');
    	$phne3     = $this->input->post('phne3');
    	$fax       = $this->input->post('fax');
    	$telx      = $this->input->post('telx');
    	$vfrm      = $this->input->post('vfrm');
    	$vto       = $this->input->post('vto');
    	$asrtrf    = $this->input->post('asrtrf');
    	$asrtudt   = $this->input->post('asrtudt');
    	$venrte    = $this->input->post('venrte');
    	$acde      = $this->input->post('acde');
    	$tino      = $this->input->post('tino');
    	$pan       = $this->input->post('pan');
    	$gstin     = $this->input->post('gstin');


		if (isset($rwid) && $rwid!="") {
			
			$data = array(
				'type'      => $typ,
		    	'name'      => $nme,
		    	'name1'     => $nme1,
		    	'add1'      => $addr1,
		    	'add2'      => $addr2,
		    	'add3'      => $addr3,
		    	'add4'      => $addr4,
		    	'pincode'   => $pincd,
		    	'state'     => $state,
		    	'stdcode'   => $stdcde,
		    	'phone1'    => $phne1,
		    	'phone2'    => $phne2,
		    	'phone3'    => $phne3,
		    	'fax'       => $fax,
		    	'telex'     => $telx,
		    	'vfrom'     => $vfrm,
		    	'vto'       => $vto,
		    	'asrturef'  => $asrtrf,
		    	'asrtudt'   => $asrtudt,
		    	'venrate'   => $venrte,
		    	'acode'     => $acde,
		    	'tinno'     => $tino,
		    	'panno'     => $pan,
		    	'gstin'     => $gstin
				);
				$streupdte = $this->Store_model->streupdte($rwid,$data);
					if ($streupdte) {
						echo "2";	
					}

				// print_r($streupdte);
				// exit;

				    
		}else{

			$data = array(
				'type'      => $typ,
		    	'name'      => $nme,
		    	'name1'     => $nme1,
		    	'add1'      => $addr1,
		    	'add2'      => $addr2,
		    	'add3'      => $addr3,
		    	'add4'      => $addr4,
		    	'pincode'   => $pincd,
		    	'state'     => $state,
		    	'stdcode'   => $stdcde,
		    	'phone1'    => $phne1,
		    	'phone2'    => $phne2,
		    	'phone3'    => $phne3,
		    	'fax'       => $fax,
		    	'telex'     => $telx,
		    	'vfrom'     => $vfrm,
		    	'vto'       => $vto,
		    	'asrturef'  => $asrtrf,
		    	'asrtudt'   => $asrtudt,
		    	'venrate'   => $venrte,
		    	'acode'     => $acde,
		    	'tinno'     => $tino,
		    	'panno'     => $pan,
		    	'gstin'     => $gstin
				);
				$streins = $this->Store_model->streins($data);
					echo "1";
		}
	}


	// Store management Remove
    function rmvstre($id){

        if (isset($id)) {
        	$rmvgp = $this->Store_model->rmvstre($id);
        	echo "0";
        }

        
    }
    // Store management Remove

	
}


