<?php 
defined("BASEPATH") OR exit('No direct script access allowed');


class Grupmgmt extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('inventory/Group_model'); 
	}

	public function index(){
		$data['prsntbrdcrmb'] = "Group Management";
		$data['grpmgmt'] = $this->Group_model->getgrpdtls();
		$this->load->view('inventory/grpmgmt',$data);
	}


	public function proced(){
		$grp    = $this->input->post('grp');
		$grpnme = $this->input->post('grpnme');
		$rwid   = $this->input->post('rwid');

		if (isset($rwid) && $rwid!="") {
			
			$data = array(
				'grp'         => $grp,
				'grpname'     => $grpnme,
				);
				$grpupdte = $this->Group_model->grpupdte($rwid,$data);
				    echo "2";
		}else{

			$data = array(
				'grp'         => $grp,
				'grpname'     => $grpnme,
				);
				$grptins = $this->Group_model->grpins($data);
					echo "1";
		}
	}


	// Group management Remove
    function rmvgrp($id){

        $this->db->where('id',$id);
        $query = $this->db->delete('master_group');

        if ($query) {
             $result['mymsg'] = "Group Removed Successfully";
        }else{
              $result['mymsg'] = "DB Error please check the connection";
        }
    }
    // Group management Remove



}

 ?>