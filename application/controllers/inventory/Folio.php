<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Folio extends CI_Controller {

	public function __construct()
	{
	    parent::__construct();
		$this->load->model('inventory/Folio_model');
	}
	
	public function index(){
		$data['prsntbrdcrmb'] = "Folio Management";
		$this->load->view('inventory/folio',$data);
	}


	// data tables
	public function datatbls(){
		$columns = array(	0  => 'id',
							1  => 'folio',
						 	2  => 'grp',
							3  => 'nom1',
							4  => 'nom2',
							5  => 'nom3',
							6  => 'par1',
							7  => 'par2',
							8  => 'par3',
							9  => 'lyc1',
							10 => 'lyc2',
							11 => 'lyc3',
							12 => 'type',
							13 => 'class',
							14 => 'ved',
							15 => 'uom',
							16 => 'max',
							17 => 'min',
							18 => 'icod',
							19 => 'rcod',
		                    20 => 'rate',
		                    21 => 'grp1',
		                    22 => 'stkcode',
		                    23 => 'isscode',
		                    24 => 'newfield',
		                    25 => 'createddate',
		                    26 => 'remove', 
						);
		$limit = $this->input->post('length');
		$start = $this->input->post('start');
		
		
		$order = $columns[$this->input->post('order')[0]['column']];
		$dir = $this->input->post('order')[0]['dir'];

		$totalData = $this->Folio_model->allposts_count();

		$totalFiltered = $totalData;

		if(empty($this->input->post('search')['value']))
		{            
			$posts = $this->Folio_model->allposts($limit,$start,$order,$dir);
		}
		else
		{
			$search = $this->input->post('search')['value'];
			$posts =  $this->Folio_model->posts_search($limit,$start,$search,$order,$dir);
			$totalFiltered = $this->Folio_model->posts_search_count($search);
		}

		// echo '<pre>';	print_r($posts);	echo '</pre>';
		// exit;
		$data = array();
		if(!empty($posts))
		{	
			$i=1;
			foreach ($posts as $post)
			{
				
				$nestedData['id']      = $i;
				$nestedData['folio']   = $post->folio;
				$nestedData['grp']     = $post->grp;
				$nestedData['nom1']    = $post->nom1;
				$nestedData['nom2']    = $post->nom2;
				$nestedData['nom3']    = $post->nom3;
				$nestedData['par1']    = $post->par1;
				$nestedData['par2']    = $post->par2;
				$nestedData['par3']    = $post->par3;
				$nestedData['lyc1']    = $post->lyc1;
				$nestedData['lyc2']    = $post->lyc2;
				$nestedData['lyc3']    = $post->lyc3;
				$nestedData['type']    = $post->type;
				$nestedData['class']   = $post->class;
				$nestedData['ved']     = $post->ved;
				$nestedData['uom']     = $post->uom;
				$nestedData['max']     = $post->max;
				$nestedData['min']     = $post->min;
				$nestedData['icod']    = $post->icod;
				$nestedData['rcod']    = $post->rcod;
				$nestedData['rate']    = $post->rate;
				$nestedData['grp1']    = $post->grp1;
				$nestedData['stkcode'] = $post->stkcode;
				$nestedData['isscode'] = $post->isscode;
				$nestedData['newfield'] = $post->newfield;
				$nestedData['createddate'] = date('j M Y h:i a',strtotime($post->createddate));
				$nestedData['remove'] = '<div><center><i class="btn btn-danger glyphicon glyphicon-trash" style="font-size:12px;text-transform:lowercase;" onclick="folrmv(\''.$post->id.'\')"></i> <i class="btn btn-success glyphicon" style="font-size:12px;text-transform:lowecase;" onclick="folupdte(\''.$post->id.'\',\''.$post->folio.'\',\''.$post->grp.'\',\''.$post->nom1.'\',\''.$post->nom2.'\',\''.$post->nom3.'\',\''.$post->par1.'\',\''.$post->par2.'\',\''.$post->par3.'\',\''.$post->lyc1.'\',\''.$post->lyc2.'\',\''.$post->lyc3.'\',\''.$post->type.'\',\''.$post->class.'\',\''.$post->ved.'\',\''.$post->uom.'\',\''.$post->max.'\',\''.$post->min.'\',\''.$post->icod.'\',\''.$post->rcod.'\',\''.$post->rate.'\',\''.$post->grp1.'\',\''.$post->stkcode.'\',\''.$post->isscode.'\',\''.$post->newfield.'\');"><i class="glyphicon glyphicon-pencil" style="font-size:12px;"></i></i></center></div>';
				$data[] = $nestedData;
				//'\''.$post->depotname.'\'';
				$i++;
			}


		}



		$json_data = array(
					"draw"            => intval($this->input->post('draw')),
					"recordsTotal"    => intval($totalData),
					"recordsFiltered" => intval($totalFiltered),
					"data"            => $data
					);
		echo json_encode($json_data);
	}
	// data table Ending


	public function proced(){


		$rwid  = $this->input->post('rwid');
		$grp   = $this->input->post('grp');
		$nom1  = $this->input->post('nom1');
		$nom2  = $this->input->post('nom2');
		$nom3  = $this->input->post('nom3');
		$prt1  = $this->input->post('prt1');
		$prt2  = $this->input->post('prt2');
		$prt3  = $this->input->post('prt3');
		$lyc1  = $this->input->post('lyc1');
		$lyc2  = $this->input->post('lyc2');
		$lyc3  = $this->input->post('lyc3');
		$typ   = $this->input->post('typ');
		$cls   = $this->input->post('cls');
		$ved   = $this->input->post('ved');
		$uom   = $this->input->post('uom');
		$maxi  = $this->input->post('maxi');
		$mini  = $this->input->post('mini');
		$icod  = $this->input->post('icod');
		$rcod  = $this->input->post('rcod');
		$rate  = $this->input->post('rate');
		$grp1  = $this->input->post('grp1');
		$stkcd = $this->input->post('stkcd');
		$isscd = $this->input->post('isscd');
		$nwfld = $this->input->post('nwfld');

		if (isset($rwid) && $rwid!="") {
			
			$data = array(
				'grp'      => $grp,
				'nom1'     => $nom1,
				'nom2'     => $nom2,
				'nom3'     => $nom3,
				'par1'     => $prt1,
				'par2'     => $prt2,
				'par3'     => $prt3,
				'lyc1'     => $lyc1,
				'lyc2'     => $lyc2,
				'lyc3'     => $lyc3,
				'type'     => $typ,
				'class'    => $cls,
				'ved'      => $ved,
				'uom'      => $uom,
				'max'      => $maxi,
				'min'      => $mini,
				'icod'     => $icod,
				'rcod'     => $rcod,
				'rate'     => $rate,
				'grp1'     => $grp1,
				'stkcode'  => $stkcd,
				'isscode'  => $isscd,
				'newfield' => $nwfld
				);
				$folupdte = $this->Folio_model->folupdte($rwid,$data);
				    echo "2";
		}else{

			$data = array(
				'grp'      => $grp,
				'nom1'     => $nom1,
				'nom2'     => $nom2,
				'nom3'     => $nom3,
				'par1'     => $prt1,
				'par2'     => $prt2,
				'par3'     => $prt3,
				'lyc1'     => $lyc1,
				'lyc2'     => $lyc2,
				'lyc3'     => $lyc3,
				'type'     => $typ,
				'class'    => $cls,
				'ved'      => $ved,
				'uom'      => $uom,
				'max'      => $maxi,
				'min'      => $mini,
				'icod'     => $icod,
				'rcod'     => $rcod,
				'rate'     => $rate,
				'grp1'     => $grp1,
				'stkcode'  => $stkcd,
				'isscode'  => $isscd,
				'newfield' => $nwfld
				);
				$foltins = $this->Folio_model->folins($data);
					echo "1";
		}
	}


	// Fleet Remove
    function rmvfol($id){

        $this->db->where('id',$id);
        $query = $this->db->delete('master_folio');

        if ($query) {
             $result['mymsg'] = "Folio Removed Successfully";
        }else{
              $result['mymsg'] = "DB Error please check the connection";
        }
    }
    // Fleet Remove

}


