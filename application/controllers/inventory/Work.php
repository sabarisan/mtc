<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Work extends CI_Controller {

	public function __construct()
	{
	    parent::__construct();
	    $this->load->model('inventory/Work_model');
	}
	
	public function index(){
		$data['prsntbrdcrmb'] = "Work Code Management";
		$data['wrkmgmt']     = $this->Work_model->wrkgetdta();
		$this->load->view('inventory/wrkcde_mgmt',$data);
	}


	public function proced(){
		// print_r($_POST);
		// exit;

    	$rwid     = $this->input->post('rwid');
    	$usr1     = $this->input->post('usr1');
    	$usrq     = $this->input->post('usrq');
    	$usr2     = $this->input->post('usr2');
    	$usr3     = $this->input->post('usr3');
    	$nom1     = $this->input->post('nom1');


		if (isset($rwid) && $rwid!="") {
			
			$data = array(
				'user1'      => $usr1,
		    	'userq'      => $usrq,
		    	'user2'      => $usr2,
		    	'user3'      => $usr3,
		    	'nom1'       => $nom1,
				);
				$wrkupdte = $this->Work_model->wrkupdte($rwid,$data);
					if ($wrkupdte) {
						echo "2";	
					}

				// print_r($wrkupdte);
				// exit;

				    
		}else{

			$data = array(
				'user1'      => $usr1,
		    	'userq'      => $usrq,
		    	'user2'      => $usr2,
		    	'user3'      => $usr3,
		    	'nom1'       => $nom1,
				);
				$wrkins = $this->Work_model->wrkins($data);
					echo "1";
		}
	}


	// Work management Remove
    function rmvwrk($id){

        if (isset($id)) {
        	$rmvgp = $this->Work_model->rmvwrk($id);
        	echo "0";
        }

        
    }
    // Work management Remove
	
}


