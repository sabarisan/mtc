<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Stats extends CI_Controller {

	public function __construct()
	{
	    parent::__construct();
	    $this->load->model('inventory/Status_model');
	}
	
	public function index(){
		$data['prsntbrdcrmb'] = "Status Management";
		$data['stsmgmt']      = $this->Status_model->gtstsmgmt();
		$this->load->view('inventory/status_mgmt',$data);
	}
	


	public function proced(){
		$styp   = $this->input->post('styp');
		$rwid   = $this->input->post('rwid');
		$nam    = $this->input->post('nam');
		$name   = $this->input->post('name');
		$name1  = $this->input->post('name1');
		$name2  = $this->input->post('name2');
	
	if ($this->session->userdata('status')=="1") {
					  if (isset($rwid) && $rwid!="") {
						$data = array(
							'styp'   => $styp,
							'nam'    => $rwid,
							'name'   => $nam,
							'name1'  => $name1,
							'name2'  => $name2,
							);
							$pmntupdte = $this->Status_model->statsupdte($rwid,$data);
							if ($pmntupdte) {
								echo "2";
							}
							    
					}else{
						$data = array(
							'styp'   => $styp,
							'nam'    => $rwid,
							'name'   => $nam,
							'name1'  => $name1,
							'name2'  => $name2,
							);
							$pmntins = $this->Status_model->statsins($data);
								// if ($pmntins) {
									echo "1";	
								// }
								
					}
	  	}
		
	}



	// Payment management Remove
    function rmvsts($id){

        $this->db->where('id',$id);
        $query = $this->db->delete('master_status');

        if ($query) {
             $result['mymsg'] = "Status Removed Successfully";
        }else{
              $result['mymsg'] = "DB Error please check the connection";
        }
    }
    // Payment management Remove



}


