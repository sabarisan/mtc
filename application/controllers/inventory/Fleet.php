<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Fleet extends CI_Controller {

	public function __construct()
	{
	    parent::__construct();
	    $this->load->model('inventory/Fleet_model');
	}
	
	public function index(){
		$data['prsntbrdcrmb'] = "Fleet Management";
		$this->load->view('inventory/fleet',$data);
	}


	// data tables
	public function datatbls(){
		$columns = array(	0 => 'id',
							1 => 'fleetid',
						 	2 => 'depotid',
							3 => 'regno',
							4 => 'type',
							5 => 'service',
							6 => 'createddate',
						);
		$limit = $this->input->post('length');
		$start = $this->input->post('start');
		
		
		$order = $columns[$this->input->post('order')[0]['column']];
		$dir = $this->input->post('order')[0]['dir'];

		$totalData = $this->Fleet_model->allposts_count();

		$totalFiltered = $totalData;

		if(empty($this->input->post('search')['value']))
		{            
			$posts = $this->Fleet_model->allposts($limit,$start,$order,$dir);
		}
		else
		{
			$search = $this->input->post('search')['value'];
			$posts =  $this->Fleet_model->posts_search($limit,$start,$search,$order,$dir);
			$totalFiltered = $this->Fleet_model->posts_search_count($search);
		}

		//echo '<pre>';	print_r($posts);	echo '</pre>';
		//exit;
		$data = array();
		if(!empty($posts))
		{	
			$i=1;
			foreach ($posts as $post)
			{
				
				$nestedData['id'] = $i;
				$nestedData['fleetid'] = $post->fleet;
				$nestedData['depotid'] = $post->depotid;
				$nestedData['regno'] = $post->regno;
				$nestedData['type'] = $post->type;
				$nestedData['service'] = $post->service;
				$nestedData['createddate'] = date('j M Y h:i a',strtotime($post->createddate));
				$nestedData['remove'] = '<div><center><i class="btn btn-danger glyphicon glyphicon-trash" style="font-size:12px;text-transform:lowercase;" onclick="fltrmv(\''.$post->id.'\')"></i> <i class="glyphicon" style="font-size:12px;text-transform:lowecase;" onclick="fletupdte(\''.$post->id.'\',\''.$post->fleet.'\',\''.$post->depotid.'\',\''.$post->regno.'\',\''.$post->type.'\',\''.$post->service.'\');"><i class="btn btn-success glyphicon glyphicon-pencil" style="font-size:12px;"></i></i></center></div>';
				$data[] = $nestedData;
				//'\''.$post->depotname.'\'';
				$i++;
			}


		}



		$json_data = array(
					"draw"            => intval($this->input->post('draw')),
					"recordsTotal"    => intval($totalData),
					"recordsFiltered" => intval($totalFiltered),
					"data"            => $data
					);
		echo json_encode($json_data);
	}
	// data table Ending


	public function proced(){
		// print_r($_POST);

		$rwid = $this->input->post('rwid');
		$fleetno = $this->input->post('fleetno');
		$deptid = $this->input->post('deptid');
		$regno = $this->input->post('regno');
		$typ = $this->input->post('typ');
		$serv = $this->input->post('serv');

		if (isset($rwid) && $rwid!="") {
			
			$data = array(
				'depotid' => $deptid,
				'regno' =>  $regno,
				'type' => $typ,
				'service' => $serv
				);
				$fltupdte = $this->Fleet_model->fltupdte($rwid,$data);

				echo "2";
		}else{

			$data = array(
				'fleet'   => $fleetno,
				'depotid' => $deptid,
				'regno' =>  $regno,
				'type' => $typ,
				'service' => $serv
				);
				$fltins = $this->Fleet_model->fltins($data);

				echo "1";
		}


	}


	// Fleet Remove
    function rmvflt($id){

        $this->db->where('id',$id);
        $query = $this->db->delete('master_fleet');

        if ($query) {
             $result['mymsg'] = "Fleet Removed Successfully";
        }else{
              $result['mymsg'] = "DB Error please check the connection";
        }
    }
    // Fleet Remove

	
	
}


