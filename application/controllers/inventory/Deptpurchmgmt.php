<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Deptpurchmgmt extends CI_Controller {

	public function __construct()
	{
	    parent::__construct();
		$this->load->model('inventory/Deptpurch_model');

	}
	
	public function index(){
		$data['prsntbrdcrmb'] = "Depot Purchase Section Management";
		$this->load->view('inventory/dept_prch_mgmt',$data);
	}



	public function datatbls(){
		$columns = array(	0 => 'id',
							1 => 'depid',
						 	2 => 'name',
							3 => 'name1',
							4 => 'name2',
							5 => 'nm',
							6 => 'alpha',
							7 => 'createddate',
						);
		$limit = $this->input->post('length');
		$start = $this->input->post('start');
		
		
		$order = $columns[$this->input->post('order')[0]['column']];
		$dir = $this->input->post('order')[0]['dir'];

		$totalData = $this->Deptpurch_model->allposts_count();

		$totalFiltered = $totalData;

		if(empty($this->input->post('search')['value']))
		{            
			$posts = $this->Deptpurch_model->allposts($limit,$start,$order,$dir);
		}
		else
		{
			$search = $this->input->post('search')['value'];
			$posts =  $this->Deptpurch_model->posts_search($limit,$start,$search,$order,$dir);
			$totalFiltered = $this->Deptpurch_model->posts_search_count($search);
		}

		//echo '<pre>';	print_r($posts);	echo '</pre>';
		//exit;
		$data = array();
		if(!empty($posts))
		{	
			$i=1;
			foreach ($posts as $post)
			{
				
				$nestedData['id'] = $i;
				$nestedData['code'] = $post->dep;
				$nestedData['name'] = $post->name;
				$nestedData['name1'] = $post->name1;
				$nestedData['name2'] = $post->name2;
				$nestedData['nm'] = $post->nm;	
				$nestedData['alpha'] = $post->alpha;
				$nestedData['createddate'] = date('j M Y h:i a',strtotime($post->createddate));
				$nestedData['remove'] = '<div><center><i class="btn btn-danger glyphicon glyphicon-trash" style="font-size:12px;text-transform:lowercase;" onclick="deptpurchrmv(\''.$post->id.'\')"></i> <i class="glyphicon" style="color:font-size:12px;text-transform:lowecase;" onclick="deptpurupdte(\''.$post->id.'\',\''.$post->dep.'\',\''.$post->name.'\',\''.$post->name1.'\',\''.$post->name2.'\',\''.$post->nm.'\',\''.$post->alpha.'\');"><i class="btn btn-success glyphicon glyphicon-pencil" style="font-size:12px;"></i></i></center></div>';
				$data[] = $nestedData;
				//'\''.$post->depotname.'\'';
				$i++;
			}


		}



		$json_data = array(
					"draw"            => intval($this->input->post('draw')),
					"recordsTotal"    => intval($totalData),
					"recordsFiltered" => intval($totalFiltered),
					"data"            => $data
					);
		echo json_encode($json_data);
	}

	public function proced(){

    		$rwid = $this->input->post('rwid');
    		$nme = $this->input->post('nam');
			$nme1 = $this->input->post('nam1');
			$nme2 = $this->input->post('nam2');
			$nm = $this->input->post('nm');
			$alpha = $this->input->post('alp');

			if (isset($rwid) && $rwid!="") {
				$data = array(
				'name'   => $nme,
				'name1'  => $nme1,
				'name2'  => $nme2,
				'nm'     => $nm,
				'alpha'  => $alpha,
				);   		
			
				$capupdte = $this->Deptpurch_model->updtdptprch($rwid,$data);	

				echo "2";
			}elseif ($rwid=="") {
				$dep = $this->input->post('dep');
				$data = array(
				'dep'    => $dep,
				'name'   => $nme,
				'name1'  => $nme1,
				'name2'  => $nme2,
				'nm'     => $nm,
				'alpha'  => $alpha,
				);   		
			 $dlvrins = $this->Deptpurch_model->dptprchins($data);
			 echo "1";
			}
	}

		// Remove Depot
	public function rmvdptprch($id){
		if (isset($id)) {
			$revmoctrl = $this->Deptpurch_model->rmvdptprch($id);
		}
	}
	// Remove Depot Ended

}


