<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('inventory/Login_model');
        /*
		$REQUEST_URI	=	$_SERVER['REQUEST_URI'];    //    /ecart/inventory/Login
        $HTTP_REFERER	=	$_SERVER['HTTP_REFERER'];    //    http://localhost/ecart/inventory/dashboard
		$requri			=	explode('/', str_replace('/ecart/inventory/','',$REQUEST_URI));
		*/
		/*
		$urlfolder		=	$this->uri->segment(1);
        $urlcontroller	=	$this->uri->segment(2);
        $urlfunction	=	$this->uri->segment(3);
		*/
    }
   
    public function index()
    {
              
        $this->load->view('inventory/login');
    }
   

    /*function landing()
	{
        $this->load->view('inventory/landing');
    }*/

    //Get Device OS, OS Version & Browser
    function userdevicedata()
    {
        $this->load->library('user_agent');
        if ($this->agent->is_browser())
        {
            $agent = $this->agent->browser().' '.$this->agent->version();
        }
        elseif ($this->agent->is_robot())
        {
            $agent = $this->agent->robot();
        }
        elseif ($this->agent->is_mobile())
        {
            $agent = $this->agent->mobile();
        }
        else
        {
            $agent = 'Unidentified User Agent';
        }
        $data['browser']    =    $agent;
        //$data['deviceos']    =    $this->agent->platform(); // Platform info (Windows, Linux, Mac, etc.)
        $platformarray        =    explode(' ',$this->agent->platform());
        $data['deviceos']    =    $platformarray[0];
        $data['osversion']    =    $this->agent->platform();
        return $data;
    }
   
    // Get Device Type
    function detectDevice()
    {
        $userAgent        =    $_SERVER["HTTP_USER_AGENT"];
        $devicesTypes    =    array(
            "computer"    =>    array("msie 10", "msie 9", "msie 8", "windows.*firefox", "windows.*chrome", "x11.*chrome", "x11.*firefox", "macintosh.*chrome", "macintosh.*firefox", "opera"),
            "tablet"    =>    array("tablet", "android", "ipad", "tablet.*firefox"),
            "mobile"    =>    array("mobile ", "android.*mobile", "iphone", "ipod", "opera mobi", "opera mini"),
            "bot"        =>    array("googlebot", "mediapartners-google", "adsbot-google", "duckduckbot", "msnbot", "bingbot", "ask", "facebook", "yahoo", "addthis")
        );
        foreach($devicesTypes as $deviceType => $devices)
        {          
            foreach($devices as $device)
            {
                if(preg_match("/" . $device . "/i", $userAgent))
                {
                    $deviceName = $deviceType;
                }
            }
        }
        return ucfirst($deviceName);
     }
   
    // get IP Address
    // Function to get the client IP address
    function get_client_ip()
    {
        $ipaddress = '';
        if (getenv('HTTP_CLIENT_IP'))
            $ipaddress = getenv('HTTP_CLIENT_IP');
        else if(getenv('HTTP_X_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        else if(getenv('HTTP_X_FORWARDED'))
            $ipaddress = getenv('HTTP_X_FORWARDED');
        else if(getenv('HTTP_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        else if(getenv('HTTP_FORWARDED'))
           $ipaddress = getenv('HTTP_FORWARDED');
        else if(getenv('REMOTE_ADDR'))
            $ipaddress = getenv('REMOTE_ADDR');
        else
            $ipaddress = 'UNKNOWN';
       
        return $ipaddress;
    }
   
    // Function to get the client ip address
    function get_client_ip_env()
    {
        $ipaddress = '';
        if (getenv('HTTP_CLIENT_IP'))
            $ipaddress = getenv('HTTP_CLIENT_IP');
        else if(getenv('HTTP_X_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        else if(getenv('HTTP_X_FORWARDED'))
            $ipaddress = getenv('HTTP_X_FORWARDED');
        else if(getenv('HTTP_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        else if(getenv('HTTP_FORWARDED'))
            $ipaddress = getenv('HTTP_FORWARDED');
        else if(getenv('REMOTE_ADDR'))
            $ipaddress = getenv('REMOTE_ADDR');
        else
            $ipaddress = 'UNKNOWN';

        return $ipaddress;
    }
   
    // Function to get the client ip address
    function get_client_ip_server()
    {
        $ipaddress = '';
        if ($_SERVER['HTTP_CLIENT_IP'])
            $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
        else if($_SERVER['HTTP_X_FORWARDED_FOR'])
            $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
        else if($_SERVER['HTTP_X_FORWARDED'])
            $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
        else if($_SERVER['HTTP_FORWARDED_FOR'])
            $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
        else if($_SERVER['HTTP_FORWARDED'])
            $ipaddress = $_SERVER['HTTP_FORWARDED'];
        else if($_SERVER['REMOTE_ADDR'])
            $ipaddress = $_SERVER['REMOTE_ADDR'];
        else
            $ipaddress = 'UNKNOWN';

        return $ipaddress;
    }
   
    function getipaddress()
    {
        $ipaddress1    =    '';
        $ipaddress2    =    '';
        $ipaddress3    =    '';
       
        $ipaddress1    =    $this->get_client_ip();
        $ipaddress2    =    $this->get_client_ip_env();
        $ipaddress3    =    $this->get_client_ip();
        if($ipaddress1!='')
        {
            $ipaddress    =    $ipaddress1;
        }
        else if($ipaddress2!='')
        {
            $ipaddress    =    $ipaddress2;
        }
        else if($ipaddress3!='')
        {
            $ipaddress    =    $ipaddress3;
        }
        else
        {
            $ipaddress    =    '';
        }
        return $ipaddress;
    }
   
    // Login Process
    function userlogin()
    {
        if ($this->input->post('username')!="" && $this->input->post('password')!="") {
        $userdevicedata            =    $this->userdevicedata();
        $data = array(
            'username'   => $this->input->post('username'),
            'password'   => md5($this->input->post('password')),
            'ipaddress'  => $this->getipaddress(),
            'devicetype' => $this->detectDevice(),
            'deviceid'   => '',
            'deviceos'   => $userdevicedata['deviceos'],
            'osversion'  => $userdevicedata['osversion'],
            'browser'    => $userdevicedata['browser'],
            'logintime'  => date('Y-m-d H:i:s')
            );
        $userlogin    =    $this->Login_model->userlogin($data);
        // print_r($userlogin);
        // exit;
        $this->session->set_userdata('status',$userlogin['status']);
        $this->session->set_userdata('message',$userlogin['message']);
        if($userlogin['status']=='1')
        {
            // User Data
            $this->session->set_userdata('userid',$userlogin['userid']);
            $this->session->set_userdata('usertype',$userlogin['usertype']);
            $this->session->set_userdata('username',$userlogin['username']);
            $this->session->set_userdata('lastlogin',$userlogin['lastlogin']);
            $this->session->set_userdata('status',$userlogin['status']);
            // Menu Data
            $menus = $this->Login_model->menus();
            if (isset($menus)) {
                $this->session->set_userdata('mymenu',$menus);
            }
            // if(isset($userlogin['umenudata']))
            // {
            //     $this->session->set_userdata('rolemenuids',$userlogin['umenudata']['menuids']);
            //     $this->session->set_userdata('rolesubmenuids',$userlogin['umenudata']['submenuids']);
            //     $this->session->set_userdata('menudata',$userlogin['umenudata']['menudata']);
            // }
            redirect(base_url().'inventory/home');
        }

        else
        {
            redirect(base_url().'inventory/login');
        }
    }else{
        redirect(base_url().'inventory/login');
    }

    }
   
    function userlogout($userid)
    {
        $userdevicedata            =    $this->userdevicedata();
        $postdata['userid']        =    $userid;
       
        $postdata['ipaddress']    =    $this->getipaddress();
        $postdata['devicetype']    =    $this->detectDevice();
        $postdata['deviceid']    =    '';
        $postdata['deviceos']    =    $userdevicedata['deviceos'];
        $postdata['osversion']    =    $userdevicedata['osversion'];
        $postdata['browser']    =    $userdevicedata['browser'];
        $postdata['logouttime']    =    date('Y-m-d H:i:s');
       
        $userlogout    =    $this->Login_model->userlogout($postdata);
        //echo '<pre>';    print_r(userlogout);    echo '</pre>';
       
        $this->session->set_userdata('status',$userlogout['status']);
        $this->session->set_userdata('message',$userlogout['message']);
        if($userlogout['status']=='1')
        {
            // $this->session->sess_destroy();
            $this->session->set_userdata('userid','');
            $this->session->set_userdata('usertype','');
            $this->session->set_userdata('username','');
            $this->session->set_userdata('lastlogin','');
            redirect(base_url().'inventory/Login');
        }
        else
        {
            redirect(base_url().'inventory/Login');
        }
    }
   
}