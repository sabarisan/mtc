<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Capgoodscd extends CI_Controller {

	public function __construct()
	{
	    parent::__construct();
	    $this->load->model('inventory/Capgoods_model');
	}
	
	public function index(){
		$data['prsntbrdcrmb'] = "Capital Goods Code";
		$this->load->view('inventory/capt_goods_code',$data);
	}


	public function datatbls(){
		$columns = array(	0 => 'id',
						 	1 => 'code',
							2 => 'description',
							3 => 'foliogroup',
						);
		$limit = $this->input->post('length');
		$start = $this->input->post('start');
		
		
		$order = $columns[$this->input->post('order')[0]['column']];
		$dir = $this->input->post('order')[0]['dir'];

		$totalData = $this->Capgoods_model->allposts_count();

		$totalFiltered = $totalData;

		if(empty($this->input->post('search')['value']))
		{            
			$posts = $this->Capgoods_model->allposts($limit,$start,$order,$dir);
		}
		else
		{
			$search = $this->input->post('search')['value'];
			$posts =  $this->Capgoods_model->posts_search($limit,$start,$search,$order,$dir);
			$totalFiltered = $this->Capgoods_model->posts_search_count($search);
		}

		//echo '<pre>';	print_r($posts);	echo '</pre>';
		//exit;
		$data = array();
		if(!empty($posts))
		{	
			$i=1;
			foreach ($posts as $post)
			{
				
				$nestedData['id'] = $i;
				$nestedData['code'] = $post->code;
				$nestedData['description'] = substr(strip_tags($post->description),0,50)."...";
				$nestedData['foliogroup'] = substr(strip_tags($post->foliogroup),0,50)."...";
				$nestedData['createddate'] = date('j M Y h:i a',strtotime($post->createddate));
				$nestedData['remove'] = '<div><center><i class="btn btn-danger glyphicon glyphicon-trash" style="font-size:12px;text-transform:lowercase;" onclick="captrmv(\''.$post->id.'\')"></i> <i class="btn btn-success glyphicon" style="font-size:12px;text-transform:lowecase;" onclick="capgodupdte(\''.$post->id.'\',\''.$post->code.'\',\''.$post->description.'\',\''.$post->foliogroup.'\');"><i class="glyphicon glyphicon-pencil" style="font-size:12px;"></i></i></center></div>';
				$data[] = $nestedData;
				//'\''.$post->depotname.'\'';
				$i++;
			}


		}



		$json_data = array(
					"draw"            => intval($this->input->post('draw')),
					"recordsTotal"    => intval($totalData),
					"recordsFiltered" => intval($totalFiltered),
					"data"            => $data
					);
		echo json_encode($json_data);
	}

	// data upadte
	public function proced(){

		        $rwid = $this->input->post('rwid');
    		$descrip = $this->input->post('descrip');
    		$folgrp = $this->input->post('folgrp');

		if (isset($rwid) && $rwid!="") {
			
			$data = array(
				'description' => $descrip,
				'foliogroup'  => $folgrp,
				);   		
			
			    $capupdte = $this->Capgoods_model->updtcaptfrm($rwid,$data);
				    echo "2";
		}else{

			$data = array(
				'description' => $descrip,
				'foliogroup'  => $folgrp,
				);   		
			 $actvtins = $this->Capgoods_model->capins($data);
					echo "1";
		}
	}
	// data upadte

	// Activity Update
		public function updtecapgds(){
			$rwid = $this->input->post('capgdid');
    		$descrip = $this->input->post('descrip');
    		$folgrp = $this->input->post('folgrp');
				
			$data = array(
				'description' => $descrip,
				'foliogroup'  => $folgrp,
				);   		
			
			$capupdte = $this->Capgoods_model->updtcaptfrm($rwid,$data);
		}
	// Activity Update Ending

	//  Activity Insert
		public function capins(){
			// print_r($_POST);

			 // [actvcd] => dasdad [actvnme]

			 $descrip = $this->input->post('descrip');
    		 $folgrp = $this->input->post('folgrp');
				
			

			 redirect('inventory/Capgoodscd/', 'refresh');

		}
	// Activity Insert Ending


		// Remove Depot
	public function rmvcapt($id){
		if (isset($id)) {
			$revmodept = $this->Capgoods_model->rmvcap($id);
		}
	}
	// Remove Depot Ended

}


