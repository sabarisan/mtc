<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class master_model extends CI_Model {

	public function __construct()
	{
	    parent::__construct();
	}
	
	function updateDelete($table=NULL,$checkcolumn=NULL,$checkvalue=NULL,$updatecolumn=NULL,$updatevalue=NULL,$currentvalue=NULL)
	{
		$updateqry	=	'UPDATE '.$table.' SET 
							'.$updatecolumn.'=\''.$updatevalue.'\'
						WHERE '.$checkcolumn.'=\''.$checkvalue.'\' AND '.$updatecolumn.'=\''.$currentvalue.'\'';
		$exquery	=	$this->db->query($updateqry);
		//echo $updateqry;
		return $exquery;
	}
	
	function updateActivatestatus($table=NULL,$checkcolumn=NULL,$checkvalue=NULL,$updatecolumn=NULL,$updatevalue=NULL,$currentvalue=NULL)
	{
		$updateqry	=	'UPDATE '.$table.' SET 
							'.$updatecolumn.'=\''.$updatevalue.'\'
						WHERE '.$checkcolumn.'=\''.$checkvalue.'\' AND '.$updatecolumn.'=\''.$currentvalue.'\'';
		$exquery	=	$this->db->query($updateqry);
		//echo $updateqry;
		return $exquery;
	}
	
	function categoryAddData($postdata)
	{
		$createdby	=	$this->session->userdata('userid');
		$insquery	=	'INSERt INTO master_category SET 
							categoryname	=	\''.$postdata['categoryname'].'\',
							description		=	\''.$postdata['description'].'\',
							createdby		=	\''.$createdby.'\'';
		$exinsqry	=	$this->db->query($insquery);
		return $exinsqry;
	}
	
	
	function categoryGetData($id=NULL)
	{
		if($id!=NULL)	{	$cond	=	' AND id=\''.$id.'\' ';	} else { $cond=''; }
		
		//$query	=	'S';
	}
	
	
	function categoryUpdateData($postdata)
	{
		$updatedby	=	$this->session->userdata('userid');
		$updateddate=	date('Y-m-d H:i:s');
		$insquery	=	'UPDATE master_category SET 
							categoryname	=	\''.$postdata['categoryname'].'\',
							description		=	\''.$postdata['description'].'\',
							updatedby		=	\''.$updatedby.'\',
							updateddate		=	\''.$updateddate.'\'
						WHERE id=\''.$postdata['categoryid'].'\'';
		$exinsqry	=	$this->db->query($insquery);
		return $exinsqry;
	}
}
