<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class Capgoods_model extends CI_Model 
{
    function __construct() {
        parent::__construct(); 
        
    }

    function allposts_count()
    {   
        $query = $this
                ->db
                ->get('master_capitalgoodscode');
    
        return $query->num_rows();  

    }
    
    function allposts($limit,$start,$col,$dir)
    {   
       $query = $this
                ->db
                ->limit($limit,$start)
                ->order_by($col,$dir)
                ->get('master_capitalgoodscode');
        
        if($query->num_rows()>0)
        {
            return $query->result(); 
        }
        else
        {
            return null;
        }
        
    }
   
    function posts_search($limit,$start,$search,$col,$dir)
    {
        $query = $this
                ->db
                ->like('id',$search)
				->or_like('code',$search)
				->or_like('description',$search)
                ->or_like('foliogroup',$search)
                ->limit($limit,$start)
                ->order_by($col,$dir)
                ->get('master_capitalgoodscode');
		
        if($query->num_rows()>0)
        {
            return $query->result();  
        }
        else
        {
            return null;
        }
    }

    function posts_search_count($search)
    {
        $query = $this
                ->db
                ->like('id',$search)
                ->like('id',$search)
				->or_like('code',$search)
				->or_like('description',$search)
                ->or_like('foliogroup',$search)
                ->get('master_capitalgoodscode');
        return $query->num_rows();
    } 


    // Update Capital Goods Model
    function updtcaptfrm($rwid,$data){

        $this->db->where('id',$rwid);
        $query = $this->db->update('master_capitalgoodscode', $data);

        if ($query) {
          $result['mymsg'] = "Capital Goods Updated Successfully";
          $result['status']  = 1;
      }else{
          $result['mymsg'] = "DB Error please check the connection";
          $result['status']  = 0;
      }

      return $result; 
    }
    // Update Capital Goods Model Ending


    // Capital Goods Insert
    function capins($data){
        $query = $this->db->insert('master_capitalgoodscode',$data);
        if ($query) {
             $result['mymsg'] = "Capital Goods Inserted Successfully";
              $result['status']  = 1;
          }else{
              $result['mymsg'] = "DB Error please check the connection";
              $result['status']  = 0;
          }

    }
    // Capital Goods Insert Ending

    // Capital Goods Delete Record
    function rmvcap($id){
        $this->db->where('id',$id);
        $query = $this->db->delete('master_capitalgoodscode');

        if ($query) {
             $result['mymsg'] = "Capital goods Removed Successfully";
        }else{
              $result['mymsg'] = "DB Error please check the connection";
        }
    }
    // Capital Goods Delete Record Ending
}
?>