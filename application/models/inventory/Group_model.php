<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class Group_model extends CI_Model 
{

    public function getgrpdtls(){
        $this->db->select('*');
        $this->db->where('activestatus','1');
        $query = $this->db->get('master_group');
        return $query->result();
    }


    // Update Group Management Model
    function grpupdte($rwid,$data){

        $this->db->where('id',$rwid);
        $query = $this->db->update('master_group', $data);

        if ($query) {
          $result['mymsg'] = "Group Management Updated Successfully";
          $result['status']  = 1;
      }else{
          $result['mymsg'] = "DB Error please check the connection";
          $result['status']  = 0;
      }

      return $result; 
    }
    // Update Group Management Model Ending


    // Group Management Insert
    function grpins($data){
        $query = $this->db->insert('master_group',$data);
        if ($query) {
             $result['mymsg'] = "Group Management Inserted Successfully";
              $result['status']  = 1;
          }else{
              $result['mymsg'] = "DB Error please check the connection";
              $result['status']  = 0;
          }

    }
    // Group Management Insert Ending

}
?>