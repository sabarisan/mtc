<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class Deptpurch_model extends CI_Model 
{
    function __construct() {
        parent::__construct(); 
        
    }

    function allposts_count()
    {   
        $query = $this
                ->db
                ->get('master_depot_purchasesection');
    
        return $query->num_rows();  

    }
    
    function allposts($limit,$start,$col,$dir)
    {   
       $query = $this
                ->db
                ->limit($limit,$start)
                ->order_by($col,$dir)
                ->get('master_depot_purchasesection');
        
        if($query->num_rows()>0)
        {
            return $query->result(); 
        }
        else
        {
            return null;
        }
        
    }
   
    function posts_search($limit,$start,$search,$col,$dir)
    {
        $query = $this
                ->db
                ->like('id',$search)
				->or_like('dep',$search)
                ->or_like('name',$search)
                ->or_like('name1',$search)
                ->or_like('name2',$search)
                ->or_like('nm',$search)
                ->or_like('alpha',$search)
                ->limit($limit,$start)
                ->order_by($col,$dir)
                ->get('master_depot_purchasesection');
		
        if($query->num_rows()>0)
        {
            return $query->result();  
        }
        else
        {
            return null;
        }
    }

    function posts_search_count($search)
    {
        $query = $this
                ->db
                ->like('id',$search)
                ->or_like('dep',$search)
                ->or_like('name',$search)
                ->or_like('name1',$search)
                ->or_like('name2',$search)
                ->or_like('nm',$search)
                ->or_like('alpha',$search)
                ->get('master_depot_purchasesection');
        return $query->num_rows();
    } 


    // Update Depot Purchase Section Model
    function updtdptprch($rwid,$data){

        $this->db->where('id',$rwid);
        $query = $this->db->update('master_depot_purchasesection', $data);

        if ($query) {
          $result['mymsg'] = "Depot Purchase Section Updated Successfully";
          $result['status']  = 1;
      }else{
          $result['mymsg'] = "DB Error please check the connection";
          $result['status']  = 0;
      }

      return $result; 
    }
    // Update Depot Purchase Section Model Ending


    // Depot Purchase Section Insert
    function dptprchins($data){
        $query = $this->db->insert('master_depot_purchasesection',$data);
        if ($query) {
             $result['mymsg'] = "Depot Purchase Section Inserted Successfully";
              $result['status']  = 1;
          }else{
              $result['mymsg'] = "DB Error please check the connection";
              $result['status']  = 0;
          }

    }
    // Depot Purchase Section Insert Ending

    // Depot Purchase Section Delete Record
    function rmvdptprch($id){
        $this->db->where('id',$id);
        $query = $this->db->delete('master_depot_purchasesection');

        if ($query) {
             $result['mymsg'] = "Depot Purchase Section Successfully";
        }else{
              $result['mymsg'] = "DB Error please check the connection";
        }
    }
    // Depot Purchase Section Delete Record Ending
}
?>