<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class Tyre_model extends CI_Model 
{
    function __construct() {
        parent::__construct(); 
        
    }

     public function gttyrmgmt(){
        $this->db->select('*');
        $this->db->where('activestatus','1');
        $query = $this->db->get('master_tyre');
        return $query->result();
    }

    // Update Tyre Model
    function tyrupdte($rwid,$data){
        $this->db->where('id',$rwid);
        $query = $this->db->update('master_tyre', $data);
        if ($query) {
          $result['mymsg'] = "Tyre Updated Successfully";
          $result['Tyre']  = 1;
      }else{
          $result['mymsg'] = "DB Error please check the connection";
          $result['Tyre']  = 0;
      }
      return $result; 
    }
    // Update Tyre Model Ending


    // Tyre Insert
    function tyrins($data){
        $query = $this->db->insert('master_tyre',$data);
        if ($query) {
             $result['mymsg'] = "Tyre Inserted Successfully";
              $result['Tyre']  = 1;
          }else{
              $result['mymsg'] = "DB Error please check the connection";
              $result['Tyre']  = 0;
          }
    }
    // Tyre Insert Ending

    // Tyre Delete Record
    function rmvactvt($id){
        $this->db->where('id',$id);
        $query = $this->db->delete('master_tyre');

        if ($query) {
             $result['mymsg'] = "Depot Removed Successfully";
        }else{
              $result['mymsg'] = "DB Error please check the connection";
        }
    }
    // Tyre Delete Record Ending
}
?>
