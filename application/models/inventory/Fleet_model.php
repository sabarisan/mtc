<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class Fleet_model extends CI_Model 
{
    function __construct() {
        parent::__construct(); 
        
    }

    function allposts_count()
    {   
        $query = $this
                ->db
                ->get('master_fleet');
    
        return $query->num_rows();  

    }
    
    function allposts($limit,$start,$col,$dir)
    {   
       $query = $this
                ->db
                ->limit($limit,$start)
                ->order_by($col,$dir)
                ->get('master_fleet');
        
        if($query->num_rows()>0)
        {
            return $query->result(); 
        }
        else
        {
            return null;
        }
        
    }
   
    function posts_search($limit,$start,$search,$col,$dir)
    {
        $query = $this
                ->db
                ->like('id',$search)
				->or_like('fleet',$search)
                ->or_like('depotid',$search)
                ->or_like('regno',$search)
                ->or_like('type',$search)
                ->or_like('service',$search)
                ->limit($limit,$start)
                ->order_by($col,$dir)
                ->get('master_fleet');
		
        if($query->num_rows()>0)
        {
            return $query->result();  
        }
        else
        {
            return null;
        }
    }

    function posts_search_count($search)
    {
        $query = $this
                ->db
                ->like('id',$search)
               ->or_like('fleet',$search)
                ->or_like('depotid',$search)
                ->or_like('regno',$search)
                ->or_like('type',$search)
                ->or_like('service',$search)
                ->get('master_fleet');
        return $query->num_rows();
    } 


    // Update Fleet Management Model
    function fltupdte($rwid,$data){

        $this->db->where('id',$rwid);
        $query = $this->db->update('master_fleet', $data);

        if ($query) {
          $result['mymsg'] = "Fleet Management Updated Successfully";
          $result['status']  = 1;
      }else{
          $result['mymsg'] = "DB Error please check the connection";
          $result['status']  = 0;
      }

      return $result; 
    }
    // Update Fleet Management Model Ending


    // Fleet Management Insert
    function fltins($data){
        $query = $this->db->insert('master_fleet',$data);
        if ($query) {
             $result['mymsg'] = "Fleet Management Inserted Successfully";
              $result['status']  = 1;
          }else{
              $result['mymsg'] = "DB Error please check the connection";
              $result['status']  = 0;
          }

    }
    // Fleet Management Insert Ending

    // Fleet Management Delete Record
    function rmvdptprch($id){
        $this->db->where('id',$id);
        $query = $this->db->delete('master_fleet');

        if ($query) {
             $result['mymsg'] = "Fleet Management Successfully";
        }else{
              $result['mymsg'] = "DB Error please check the connection";
        }
    }
    // Fleet Management Delete Record Ending
}
?>