<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class Work_model extends CI_Model 
{
    function __construct() {
        parent::__construct(); 
        
    }

     public function wrkgetdta(){
        $this->db->select('*');
        $this->db->where('activestatus','1');
        $query = $this->db->get('master_workcode');
        return $query->result();
    }

    // Update Work Model
    function wrkupdte($rwid,$data){
        $this->db->where('id',$rwid);
        $query = $this->db->update('master_workcode', $data);
        if ($query) {
          $result['mymsg'] = "Work Updated Successfully";
          $result['Work']  = 1;
      }else{
          $result['mymsg'] = "DB Error please check the connection";
          $result['Work']  = 0;
      }
      return $result; 
    }
    // Update Work Model Ending


    // Work Insert
    function wrkins($data){
        $query = $this->db->insert('master_workcode',$data);
        if ($query) {
             $result['mymsg'] = "Work Inserted Successfully";
              $result['Work']  = 1;
          }else{
              $result['mymsg'] = "DB Error please check the connection";
              $result['Work']  = 0;
          }
    }
    // Work Insert Ending

    // Work Delete Record
    function rmvwrk($id){
        $this->db->where('id',$id);
        $query = $this->db->delete('master_workcode');

        if ($query) {
             $result['mymsg'] = "Work Removed Successfully";
        }else{
              $result['mymsg'] = "DB Error please check the connection";
        }
    }
    // Work Delete Record Ending
}
?>
