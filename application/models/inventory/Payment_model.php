<?php
	if (!defined('BASEPATH'))
    	exit('No direct script access allowed');

class Payment_model extends CI_Model 
{

    public function getpymntdtls(){
        $this->db->select('*');
        $this->db->where('activestatus','1');
        $query = $this->db->get('master_paymentterms');

        return $query->result();
    }




    // Update Payment terms Management Model
    function pmntupdte($rwid,$data){

        $this->db->where('id',$rwid);
        $query = $this->db->update('master_paymentterms', $data);

        if ($query) {
          $result['mymsg'] = "Payment terms Management Updated Successfully";
          $result['status']  = 1;
      }else{
          $result['mymsg'] = "DB Error please check the connection";
          $result['status']  = 0;
      }

      return $result; 
    }
    // Update Payment terms Management Model Ending


    // Payment terms Management Insert
    function pmntins($data){
        $query = $this->db->insert('master_paymentterms',$data);
        if ($query) {
             $result['mymsg'] = "Payment terms Management Inserted Successfully";
              $result['status']  = 1;
          }else{
              $result['mymsg'] = "DB Error please check the connection";
              $result['status']  = 0;
          }

    }
    // Payment terms Management Insert Ending

}
?>