<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Login_model extends CI_Model {

	public function __construct()
	{
	    parent::__construct();
		$this->load->helper('url');
	}
	

	// ***** Temp Menu access
	function menus(){
		$this->db->select('*');
		$this->db->from('admin_menu');
		$this->db->where('activestatus','1');
		$this->db->where('status','1');
		$this->db->order_by('displayorder','asc');
		$mnu = $this->db->get();

		if ($mnu) {
			if ($mnu->num_rows()>0) {
				

				$i=0;
				foreach($mnu->result_array() as $menus)
				{
					$menudata[$i]['menuid']		=	$menus['id'];
					$menudata[$i]['menuname']	=	$menus['name'];
					$menudata[$i]['menuicon']	=	$menus['icon'];
					$menudata[$i]['menulink']	=	$menus['menulink'];
					$menudata[$i]['menuaccess'] =   $menus['nav_access'];
					// Sub-Menu of Menu
					/*$smqry		=	'SELECT id,menuid,name,icon,submenulink FROM admin_submenu WHERE id IN('.$submenuids.') AND menuid=\''.$menus['id'].'\' AND activestatus=\'1\' AND status=\'1\'';
					$xsmqry		=	$this->db->query($smqry);*/
					// $submenuarray	=	explode(',',$submenuids);
					$this->db->select('*');
					$this->db->from('admin_submenu');
					// $this->db->where('menuid',$menus['id']);
					// $this->db->where_in('id',$submenuarray);
					$this->db->where('activestatus','1');
					$this->db->where('status','1');
					$xsmqry	=	$this->db->get();
					$submenudata	=	array();
					if($xsmqry->num_rows()>0)
					{
						$j=0;
						foreach($xsmqry->result_array() as $submenus)
						{
							$submenudata[$j]['submenuid']	   =	$submenus['id'];
							$submenudata[$j]['menuid']		   =	$submenus['menuid'];
							$submenudata[$j]['submenuname']	   =	$submenus['name'];
							$submenudata[$j]['submenuicon']	   =	$submenus['icon'];
							$submenudata[$j]['submenulink']	   =	$submenus['submenulink'];
							$submenudata[$j]['submenuaccess']  =    $submenus['nav_access'];
						$j++;
						}
					}
					$menudata[$i]['submenudata']	=	$submenudata;
				$i++;	
				}

			}
		}
		// $usermenudatas['usertypeid']	=	$usertype;
		// $usermenudatas['menuids']		=	$menuids;
		// $usermenudatas['submenuids']	=	$submenuids;
		 if (isset($menudata) && $submenudata) {
			
			$usermenudatas['menudata']		    =	$menudata;
			$usermenudatas['submenudata']		=	$submenudata;

			return $usermenudatas;

		}

		
		
		
	}

	// ***** Temp Menu access Ending




	function userlogin($postdata)
	{
		
			// $username  = $postdata['username'];
			// $password  = $postdata['password'];

			$this->db->select('*');
			$this->db->from('userlogin');
			$this->db->where('username',$postdata['username']);
			$this->db->where('password',$postdata['password']);
			$this->db->where('status','Active');
			$exquery =  $this->db->get();

			// echo $this->db->last_query();

			// exit;

		// $query	=	'SELECT * FROM userlogin WHERE username=\''.$postdata['username'].'\' AND password=\''.$postdata['password'].'\' AND status=\'Active\'';
		// $exquery=	$this->db->query($query);	
		$msg 	=	$this->db->conn_id->error_list;
		if($exquery)
		{
			if($exquery->num_rows()>0)
			{
				// User Data
				$row				=	$exquery->result_array();
				$result['status']	=	1;
				$result['message']	=	'Login Successful';
				$result['userid']	=	$row[0]['userid'];
				$result['usertype']	=	$row[0]['usertype'];
				$result['username']	=	$row[0]['username'];
				$result['lastlogin']=	$row[0]['logged_in'];

				$trackdetail = array(
					'userid'     => $result['userid'],
					'ipaddress'  => $postdata['ipaddress'],
					'devicetype' => $postdata['devicetype'],
					'deviceid'   => $postdata['deviceid'],
					'deviceos'   => $postdata['deviceos'],
					'browser'    => $postdata['browser'],
					'osversion'  => $postdata['osversion'],
					 );
				
				$exinslog = $this->db->insert('userlogtrack',$trackdetail);
				$exinslog	=	$this->db->query($inslog);
			}
			else
			{
				// No Record
				//Update Login Attempt Count 
				$result['status']	=	0;
				$result['message']	=	'Login Failed';
			}
		}
		else
		{
			// MySQL Error occurs
			$result['status']	=	0;
			$result['message']	=	'DB Error! Try Again';
		}
		return $result;
	}
	
	function userlogout($postdata)
	{
		$query	=	'SELECT id FROM userlogtrack WHERE userid=\''.$postdata['userid'].'\' AND devicetype=\''.$postdata['devicetype'].'\' AND deviceid=\''.$postdata['deviceid'].'\' AND deviceos=\''.$postdata['deviceos'].'\' AND osversion=\''.$postdata['osversion'].'\' AND browser=\''.$postdata['browser'].'\' AND status=\'1\' ORDER BY id DESC LIMIT 0,1';
		$exquery=	$this->db->query($query);	
		$msg 	=	$this->db->conn_id->error_list;
		if($exquery)
		{
			$row	=	$exquery->result_array();
			$logid	=	$row[0]['id'];
			// Logout Update
			$query1		=	'UPDATE userlogtrack SET
								logouttime	=	\''.$postdata['logouttime'].'\',
								status		=	\'0\'
							WHERE id=\''.$logid.'\' AND userid=\''.$postdata['userid'].'\' AND devicetype=\''.$postdata['devicetype'].'\' AND deviceid=\''.$postdata['deviceid'].'\' AND deviceos=\''.$postdata['deviceos'].'\' AND osversion=\''.$postdata['osversion'].'\' AND browser=\''.$postdata['browser'].'\' AND status=\'1\'';	
			
			$exquery1	=	$this->db->query($query1);
			
			if($query1)
			{
				$result['status']	=	1;
				$result['message']	=	'Logout Successful';
			}
			else
			{
				$result['status']	=	0;
				$result['message']	=	'Logout Failed. Try again';
			}
			
		}
		else
		{
			// MySQL Error occurs
			$result['status']	=	0;
			$result['message']	=	'DB Error! Try Again';
		}
		return $result;
	}		
}

?>