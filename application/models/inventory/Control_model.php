<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class Control_model extends CI_Model 
{
    function __construct() {
        parent::__construct(); 
        
    }

    function allposts_count()
    {   
        $query = $this
                ->db
                ->get('master_control');
    
        return $query->num_rows();  

    }
    
    function allposts($limit,$start,$col,$dir)
    {   
       $query = $this
                ->db
                ->limit($limit,$start)
                ->order_by($col,$dir)
                ->get('master_control');
        
        if($query->num_rows()>0)
        {
            return $query->result(); 
        }
        else
        {
            return null;
        }
        
    }
   
    function posts_search($limit,$start,$search,$col,$dir)
    {
        $query = $this
                ->db
                ->like('id',$search)
				->or_like('foliogroup',$search)
				->or_like('type',$search)
                ->or_like('description',$search)
                ->limit($limit,$start)
                ->order_by($col,$dir)
                ->get('master_control');
		
        if($query->num_rows()>0)
        {
            return $query->result();  
        }
        else
        {
            return null;
        }
    }

    function posts_search_count($search)
    {
        $query = $this
                ->db
                ->like('id',$search)
				->or_like('foliogroup',$search)
                ->or_like('type',$search)
                ->or_like('description',$search)
                ->get('master_control');
        return $query->num_rows();
    } 


    // Update Control  Model
    function updtctrl($rwid,$data){

        $this->db->where('id',$rwid);
        $query = $this->db->update('master_control', $data);

        if ($query) {
          $result['mymsg'] = "Control  Updated Successfully";
          $result['status']  = 1;
      }else{
          $result['mymsg'] = "DB Error please check the connection";
          $result['status']  = 0;
      }

      return $result; 
    }
    // Update Control  Model Ending


    // Control  Insert
    function ctrlins($data){
        $query = $this->db->insert('master_control',$data);
        if ($query) {
             $result['mymsg'] = "Control  Inserted Successfully";
              $result['status']  = 1;
          }else{
              $result['mymsg'] = "DB Error please check the connection";
              $result['status']  = 0;
          }

    }
    // Control  Insert Ending

    // Control  Delete Record
    function rmvctrl($id){
        $this->db->where('id',$id);
        $query = $this->db->delete('master_control');

        if ($query) {
             $result['mymsg'] = "Depot Removed Successfully";
        }else{
              $result['mymsg'] = "DB Error please check the connection";
        }
    }
    // Control  Delete Record Ending
}
?>