<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Asset_model extends CI_Model {

	public function __construct()
	{
	    parent::__construct();
	}
	
	public function categry()
	{
		$this->db->select('*');
        $this->db->from('master_category');
        $this->db->where('activestatus',1);
        $this->db->where('status',1);
        $query =  $this->db->get();
        return $query->result(); 
	}

	//To Fetching all product categories

	public function allcategry(){
		$this->db->select('*');
        $this->db->from('master_category');
        $query =  $this->db->get();
        return $query->result(); 
	}

	//To Fetching all product categories Ending


	public function subcategry(){
		$this->db->select('*');
        $this->db->from('master_subcategory');
        $this->db->where('activestatus',1);
        $this->db->where('status',1);
        $query =  $this->db->get();
        return $query->result();
	}
}
