<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class Depot_model extends CI_Model 
{
    function __construct() {
        parent::__construct(); 
        
    }

    function allposts_count()
    {   
        $query = $this
                ->db
                ->get('master_depot');
    
        return $query->num_rows();  

    }
    
    function allposts($limit,$start,$col,$dir)
    {   
       $query = $this
                ->db
                ->limit($limit,$start)
                ->order_by($col,$dir)
                ->get('master_depot');
        
        if($query->num_rows()>0)
        {
            return $query->result(); 
        }
        else
        {
            return null;
        }
        
    }
   
    function posts_search($limit,$start,$search,$col,$dir)
    {
        $query = $this
                ->db
                ->like('id',$search)
				->or_like('depotid',$search)
                ->or_like('depotname',$search)
				->or_like('depotopt',$search)
                ->limit($limit,$start)
                ->order_by($col,$dir)
                ->get('master_depot');
		
        if($query->num_rows()>0)
        {
            return $query->result();  
        }
        else
        {
            return null;
        }
    }

    function posts_search_count($search)
    {
        $query = $this
                ->db
                ->like('id',$search)
				->or_like('depotid',$search)
                ->or_like('depotname',$search)
				->or_like('depotopt',$search)
                ->get('master_depot');
        return $query->num_rows();
    } 

    // function for specific depot
    function spec_dept($dept_id){

        $this->db->select('*');
        $this->db->from('master_depot');
        $this->db->where('depotid',$dept_id);
        $query = $this->db->get();

        return $query->result();

    }


    // 
    function updtdpte($rwid,$data){

        $this->db->where('id',$rwid);
        $query = $this->db->update('master_depot', $data);

        if ($query) {
          $result['mymsg'] = "Depot Updated Successfully";
          $result['status']  = 1;
      }else{
          $result['mymsg'] = "DB Error please check the connection";
          $result['status']  = 0;
      }

      return $result; 
    }

    // depot insert
    function depins($data){

        $query = $this->db->insert('master_depot',$data);

        if ($query) {
             $result['mymsg'] = "Depot Inserted Successfully";
              $result['status']  = 1;
          }else{
              $result['mymsg'] = "DB Error please check the connection";
              $result['status']  = 0;
          }
    }
    // depot insert Ended


    // Depot Remove
    function rmvdpte($id){

        $this->db->where('id',$id);
        $query = $this->db->delete('master_depot');

        if ($query) {
             $result['mymsg'] = "Depot Removed Successfully";
        }else{
              $result['mymsg'] = "DB Error please check the connection";
        }
    }
    // Depot Remove
   
}