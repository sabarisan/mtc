<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class Folio_model extends CI_Model 
{
    function __construct() {
        parent::__construct(); 
        
    }

    function allposts_count()
    {   
        $query = $this
                ->db
                ->get('master_folio');
    
        return $query->num_rows();  

    }
    
    function allposts($limit,$start,$col,$dir)
    {   
       $query = $this
                ->db
                ->limit($limit,$start)
                ->order_by($col,$dir)
                ->get('master_folio');
        
        if($query->num_rows()>0)
        {
            return $query->result(); 
        }
        else
        {
            return null;
        }
        
    }
   
    function posts_search($limit,$start,$search,$col,$dir)
    {
        $query = $this
                ->db
                ->like('id',$search)
				->or_like('folio',$search)
                ->or_like('grp',$search)
                ->or_like('nom1',$search)
                ->or_like('nom2',$search)
                ->or_like('nom3',$search)
                ->or_like('par1',$search)
                ->or_like('par2',$search)
                ->or_like('par3',$search)
                ->or_like('lyc1',$search)
                ->or_like('lyc2',$search)
                ->or_like('lyc3',$search)
                ->or_like('type',$search)
                ->or_like('class',$search)
                ->or_like('ved',$search)
                ->or_like('uom',$search)
                ->or_like('max',$search)
                ->or_like('min',$search)
                ->or_like('icod',$search)
                ->or_like('rcod',$search)
                ->or_like('rate',$search)
                ->or_like('grp1',$search)
                ->or_like('stkcode',$search)
                ->or_like('isscode',$search)
                ->or_like('newfield',$search)
                ->limit($limit,$start)
                ->order_by($col,$dir)
                ->get('master_folio');
		
        if($query->num_rows()>0)
        {
            return $query->result();  
        }
        else
        {
            return null;
        }
    }

    function posts_search_count($search)
    {
        $query = $this
                ->db
                ->like('id',$search)
                ->or_like('folio',$search)
                ->or_like('grp',$search)
                ->or_like('nom1',$search)
                ->or_like('nom2',$search)
                ->or_like('nom3',$search)
                ->or_like('par1',$search)
                ->or_like('par2',$search)
                ->or_like('par3',$search)
                ->or_like('lyc1',$search)
                ->or_like('lyc2',$search)
                ->or_like('lyc3',$search)
                ->or_like('type',$search)
                ->or_like('class',$search)
                ->or_like('ved',$search)
                ->or_like('uom',$search)
                ->or_like('max',$search)
                ->or_like('min',$search)
                ->or_like('icod',$search)
                ->or_like('rcod',$search)
                ->or_like('rate',$search)
                ->or_like('grp1',$search)
                ->or_like('stkcode',$search)
                ->or_like('isscode',$search)
                ->or_like('newfield',$search)
                ->get('master_folio');
        return $query->num_rows();
    } 


    // Update Folio Management Model
    function folupdte($rwid,$data){

        $this->db->where('id',$rwid);
        $query = $this->db->update('master_folio', $data);

        if ($query) {
          $result['mymsg'] = "Folio Management Updated Successfully";
          $result['status']  = 1;
      }else{
          $result['mymsg'] = "DB Error please check the connection";
          $result['status']  = 0;
      }

      return $result; 
    }
    // Update Folio Management Model Ending


    // Folio Management Insert
    function folins($data){
        $query = $this->db->insert('master_folio',$data);
        if ($query) {
             $result['mymsg'] = "Folio Management Inserted Successfully";
              $result['status']  = 1;
          }else{
              $result['mymsg'] = "DB Error please check the connection";
              $result['status']  = 0;
          }

    }
    // Folio Management Insert Ending

   
}
?>