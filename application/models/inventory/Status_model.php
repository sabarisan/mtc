<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class Status_model extends CI_Model 
{
    function __construct() {
        parent::__construct(); 
        
    }

     public function gtstsmgmt(){
        $this->db->select('*');
        $this->db->where('activestatus','1');
        $query = $this->db->get('master_status');
        return $query->result();
    }

    // Update Status Model
    function statsupdte($rwid,$data){
        $this->db->where('id',$rwid);
        $query = $this->db->update('master_status', $data);
        if ($query) {
          $result['mymsg'] = "Status Updated Successfully";
          $result['status']  = 1;
      }else{
          $result['mymsg'] = "DB Error please check the connection";
          $result['status']  = 0;
      }
      return $result; 
    }
    // Update Status Model Ending


    // Status Insert
    function statsins($data){
        $query = $this->db->insert('master_status',$data);
        if ($query) {
             $result['mymsg'] = "Status Inserted Successfully";
              $result['status']  = 1;
          }else{
              $result['mymsg'] = "DB Error please check the connection";
              $result['status']  = 0;
          }
    }
    // Status Insert Ending

    // Status Delete Record
    function rmvactvt($id){
        $this->db->where('id',$id);
        $query = $this->db->delete('master_status');

        if ($query) {
             $result['mymsg'] = "Depot Removed Successfully";
        }else{
              $result['mymsg'] = "DB Error please check the connection";
        }
    }
    // Status Delete Record Ending
}
?>
