<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class Store_model extends CI_Model 
{
    function __construct() {
        parent::__construct(); 
        
    }

     public function stregetdta(){
        $this->db->select('*');
        $this->db->where('activestatus','1');
        $query = $this->db->get('master_store');
        return $query->result();
    }

    // Update Store Model
    function streupdte($rwid,$data){
        $this->db->where('id',$rwid);
        $query = $this->db->update('master_store', $data);
        if ($query) {
          $result['mymsg'] = "Store Updated Successfully";
          $result['Store']  = 1;
      }else{
          $result['mymsg'] = "DB Error please check the connection";
          $result['Store']  = 0;
      }
      return $result; 
    }
    // Update Store Model Ending


    // Store Insert
    function streins($data){
        $query = $this->db->insert('master_store',$data);
        if ($query) {
             $result['mymsg'] = "Store Inserted Successfully";
              $result['Store']  = 1;
          }else{
              $result['mymsg'] = "DB Error please check the connection";
              $result['Store']  = 0;
          }
    }
    // Store Insert Ending

    // Store Delete Record
    function rmvstre($id){
        $this->db->where('id',$id);
        $query = $this->db->delete('master_store');

        if ($query) {
             $result['mymsg'] = "Depot Removed Successfully";
        }else{
              $result['mymsg'] = "DB Error please check the connection";
        }
    }
    // Store Delete Record Ending
}
?>
