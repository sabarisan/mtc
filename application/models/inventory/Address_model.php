<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class Address_model extends CI_Model 
{
    function __construct() {
        parent::__construct(); 
        
    }

    function allposts_count()
    {   
        $query = $this
                ->db
                ->get('master_address');
    
        return $query->num_rows();  

    }
    
    function allposts($limit,$start,$col,$dir)
    {   
       $query = $this
                ->db
                ->limit($limit,$start)
                ->order_by($col,$dir)
                ->get('master_address');
        
        if($query->num_rows()>0)
        {
            return $query->result(); 
        }
        else
        {
            return null;
        }
        
    }
   
    function posts_search($limit,$start,$search,$col,$dir)
    {
        $query = $this
                ->db
                ->like('id',$search)
				->or_like('code',$search)
                ->or_like('street',$search)
                ->or_like('location',$search)
                ->or_like('landmark',$search)
                ->or_like('city',$search)
                ->or_like('dist',$search)
                ->or_like('state',$search)
                ->or_like('pincode',$search)
                ->or_like('country',$search)
                ->limit($limit,$start)
                ->order_by($col,$dir)
                ->get('master_address');
		
        if($query->num_rows()>0)
        {
            return $query->result();  
        }
        else
        {
            return null;
        }
    }

    function posts_search_count($search)
    {
        $query = $this
                ->db
                ->like('id',$search)
                ->or_like('code',$search)
                ->or_like('street',$search)
                ->or_like('location',$search)
                ->or_like('landmark',$search)
                ->or_like('city',$search)
                ->or_like('dist',$search)
                ->or_like('state',$search)
                ->or_like('pincode',$search)
                ->or_like('country',$search)
                ->get('master_address');
        return $query->num_rows();
    } 


    // Update Activity Model
    function updtaddr($rwid,$data){

        $this->db->where('id',$rwid);
        $query = $this->db->update('master_address', $data);

        if ($query) {
          $result['mymsg'] = "Address Updated Successfully";
          $result['status']  = 1;
      }else{
          $result['mymsg'] = "DB Error please check the connection";
          $result['status']  = 0;
      }

      return $result; 
    }
    // Update Activity Model Ending


    // Activity Insert
    function addrins($data){
        $query = $this->db->insert('master_address',$data);
        if ($query) {
             $result['mymsg'] = "Activity Inserted Successfully";
              $result['status']  = 1;
          }else{
              $result['mymsg'] = "DB Error please check the connection";
              $result['status']  = 0;
          }

    }
    // Activity Insert Ending

    // Activity Delete Record
    function rmvaddr($id){
        $this->db->where('id',$id);
        $query = $this->db->delete('master_address');

        if ($query) {
             $result['mymsg'] = "Depot Removed Successfully";
        }else{
              $result['mymsg'] = "DB Error please check the connection";
        }
    }
    // Activity Delete Record Ending
}
?>