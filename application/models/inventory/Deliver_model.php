<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class Deliver_model extends CI_Model 
{
    function __construct() {
        parent::__construct(); 
        
    }

    function allposts_count()
    {   
        $query = $this
                ->db
                ->get('master_deliveryterms');
    
        return $query->num_rows();  

    }
    
    function allposts($limit,$start,$col,$dir)
    {   
       $query = $this
                ->db
                ->limit($limit,$start)
                ->order_by($col,$dir)
                ->get('master_deliveryterms');
        
        if($query->num_rows()>0)
        {
            return $query->result(); 
        }
        else
        {
            return null;
        }
        
    }
   
    function posts_search($limit,$start,$search,$col,$dir)
    {
        $query = $this
                ->db
                ->like('id',$search)
				->or_like('code',$search)
				->or_like('description',$search)
                ->or_like('name1',$search)
                ->limit($limit,$start)
                ->order_by($col,$dir)
                ->get('master_deliveryterms');
		
        if($query->num_rows()>0)
        {
            return $query->result();  
        }
        else
        {
            return null;
        }
    }

    function posts_search_count($search)
    {
        $query = $this
                ->db
                ->like('id',$search)
                ->or_like('code',$search)
                ->or_like('description',$search)
                ->or_like('name1',$search)
                ->get('master_deliveryterms');
        return $query->num_rows();
    } 


    // Update Deliver Terms  Model
    function updtdlvr($rwid,$data){

        $this->db->where('id',$rwid);
        $query = $this->db->update('master_deliveryterms', $data);

        if ($query) {
          $result['mymsg'] = "Deliver Terms  Updated Successfully";
          $result['status']  = 1;
      }else{
          $result['mymsg'] = "DB Error please check the connection";
          $result['status']  = 0;
      }

      return $result; 
    }
    // Update Deliver Terms  Model Ending


    // Deliver Terms  Insert
    function dlvrins($data){
        $query = $this->db->insert('master_deliveryterms',$data);
        if ($query) {
             $result['mymsg'] = "Deliver Terms  Inserted Successfully";
              $result['status']  = 1;
          }else{
              $result['mymsg'] = "DB Error please check the connection";
              $result['status']  = 0;
          }

    }
    // Deliver Terms  Insert Ending

    // Deliver Terms  Delete Record
    function rmvdlvr($id){
        $this->db->where('id',$id);
        $query = $this->db->delete('master_deliveryterms');

        if ($query) {
             $result['mymsg'] = "Depot Removed Successfully";
        }else{
              $result['mymsg'] = "DB Error please check the connection";
        }
    }
    // Deliver Terms  Delete Record Ending
}
?>