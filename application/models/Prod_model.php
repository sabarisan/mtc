<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Prod_model extends CI_Model {

	public function __construct()
	{
	    parent::__construct();
	}
	


  // *****Function for Product New Arrival *****
  public function newarriv(){

          // $date = date('Y-m-d H:i:s', strtotime('2018-10-04 12:05:60'));

            $this->db->select('product.*,productimages.*')
              ->from('product')
              ->join('productimages','product.productcode=productimages.productid')
              ->where('product.createddate BETWEEN NOW() - INTERVAL 3 DAY AND NOW()')
              ->where('product.status',1)
              ->where('product.activestatus',1);
              $query = $this->db->get();      
              return $query->result();
    
  }
  // ***** Function for Product New Arrival Ending *****

  // ***** Function for All Product *****
  public function allprd(){
            $this->db->select('product.*,productimages.*')
              ->from('product')
              ->join('productimages','product.productcode=productimages.productid')
              ->where('product.status',1)
              ->where('product.activestatus',1)
              ->where('productimages.activestatus',1);
              $query = $this->db->get();      
              return $query->result();
  }
  // ***** Function for All Product Ending*****

	public function prodts($cat,$subcat){

        	$this->db->select('product.*,productimages.*')
              ->from('product')
              ->join('productimages','product.productcode=productimages.productid')
              ->where('product.category',$cat)
              ->where('product.subcategory',$subcat)
              ->where('product.status',1)
              ->where('product.activestatus',1)
              ->where('productimages.activestatus',1);
              $query = $this->db->get();      
              return $query->result();

	}

	public function prodts_ct($cat){
				$this->db->select('product.*,productimages.*')
              ->from('product')
              ->join('productimages','product.productcode=productimages.productid')
              ->where('product.category',$cat)
              ->where('product.status',1)
              ->where('product.activestatus',1)
              ->where('productimages.activestatus',1);
              $query = $this->db->get();      
              return $query->result();
	}


  // function for Product Details
  public function prd_dtl($prd_cod){

            $this->db->select('product.*,productimages.*')
              ->from('product')
              ->join('productimages','product.productcode=productimages.productid')
              ->where('productimages.productid',$prd_cod);
              $query = $this->db->get();      
              return $query->result();
  }
  // Product Details

}
