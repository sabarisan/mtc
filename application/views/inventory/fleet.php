<!DOCTYPE html>
<html>
   <head>
      <title>MTC | Fleet Management</title>
      <?php include('top.php'); ?>
   </head>
   <body>
      <?php include('header.php'); ?>
         
      <?php include('brdcrmb.php'); ?>

      <?php 
         include('side.php');
       ?>
      <!-- col-lg-10 starting -->
      <!-- <div class="col-lg-9"> -->
         <div class="panel" style="position: static;display: none;" id="fleetfm">
            <!-- Sales stats -->
                  <div class="panel panel-flat">
                  <!-- <br> -->
                     <div class="container-fluid">
                        <div class="row text-center">
                           <div class="col-md-4 bg-mybrwn-400">
                           <br>
                              <div class="content-group">
                                 <h5 class="text-semibold no-margin"><i class="icon-bus position-left text-slate"></i> 4016</h5>
                                 <span class="text-muted text-size-small">TOTAL NO OF FLEET</span>
                              </div>
                           </div>

                           <div class="col-md-4 bg-mybrwn-400">
                           <br>
                              <div class="content-group">
                                 <h5 class="text-semibold no-margin"><i class="icon-calendar52 position-left text-slate"></i> 70</h5>
                                 <span class="text-muted text-size-small">FLEET ADDED IN LAST MONTH</span>
                              </div>
                           </div>

                           <div class="col-md-4 bg-mybrwn-400">
                           <br>
                              <div class="content-group">
                                 <h5 class="text-semibold no-margin"><i class="icon-task position-left text-slate"></i> 4016</h5>
                                 <span class="text-muted text-size-small">TOTAL NO OF ACTIVE FLEET</span>
                              </div>
                           </div>
                        </div>

                  </div>
                  <!-- /sales stats -->
                  <div class="mystrip">
                     <center><b>FLEET MANAGEMENT</b></center>   
                  </div>
               </div>
            <div class="panel-body">
               <form action="#" method="post" id="fleet">
                  <div class="container-fluid"> 
                  <!-- row1 -->
                     <div class="row">
                        <div class="col-md-6">
                           <div class="form-group">
                              <label>Fleet No</label>
                                 <div class="input-group">
                                          <span class="input-group-addon"><span class="icon icon-bus"></span></span>
                                          <input class="form-control" value="" name="fleetno" id="fletnotxt" placeholder="Fleet No" type="text" style="display: none;">
                                          <p id="fltxtpra"  ></p>
                                 </div>
                           </div>
                        </div>
                        <input type="hidden" name="rwid" id="rwid">
                        <div class="col-md-6">
                           <div class="form-group">
                              <label>Depotid</label>
                                 <div class="input-group">
                                          <span class="input-group-addon"><span class="icon icon-pen"></span></span>
                                          <input class="form-control" name="deptid" value="" id="dptidtxt" placeholder="Depotid" type="text">
                                 </div>
                           </div>
                        </div>
                     </div>
                     <!-- row1 Ending -->

                     <!-- row2 -->
                     <div class="row">
                        <div class="col-md-6">
                           <div class="form-group">
                              <label>Registered No</label>
                                 <div class="input-group">
                                          <span class="input-group-addon"><span class="icon icon-insert-template"></span></span>
                                          <input class="form-control" name="regno" value="" id="fltregtxt" placeholder="Registered No" type="text">
                                 </div>
                           </div>
                        </div>
                        <div class="col-md-6">
                           <div class="form-group">
                              <label>Type</label>
                                 <div class="input-group">
                                          <span class="input-group-addon"><span class="icon icon-radio-unchecked"></span></span>
                                          <input class="form-control" name="typ" value="" id="typtxt" placeholder="Type" type="text">
                               </div>
                           </div>
                        </div>
                     </div>
                     <!-- row2 Ending -->

                     <!-- row3 -->
                     <div class="row">
                        <div class="col-md-12">
                           <div class="form-group">
                              <label>Service</label>
                                 <div class="input-group">
                                          <span class="input-group-addon"><span class="icon icon-split"></span></span>
                                          <input class="form-control" name="serv" value="" id="servtxt" placeholder="Service" type="text">
                                 </div>
                           </div>
                        </div>
                        
                     </div>
                     <!-- row3 Ending -->

                  </div>

                  <!-- container Ending -->
            </div>
            <div class="panel-footer">
            <div class="text-right">
            <button type="button" id="fltfrm" class="btn btn-danger" style="margin-right: 2%;">Submit <i class="icon-arrow-right14 position-right"></i></button>
            </div>	
            </div>
            </form>
         </div>

         <!-- Basic initialization -->
        <div class="panel panel-flat">
          <div class="panel-heading">
            <h5 class="panel-title">Fleet Management Details</h5>
            <div class="heading-elements">
              &nbsp;&nbsp;&nbsp;<div class="btn btn-info" onclick="fletins()"><i class="icon icon-add" style="color: #fff; "></i> Add</div>
              <ul class="icons-list">
                        <li><a data-action="collapse"></a></li>
                        <!-- <li><a data-action="reload"></a></li> -->
                        <!-- <li><a data-action="close"></a></li> -->
                      </ul>
                    </div>
          </div>

          <div class="container-fluid">
              <table class="table datatable-button-init-basic" id="fleetfrm">
            <thead>
            <th>Sno</th>
            <th>Fleetid</th>
            <th>Depotid</th>
            <th>Regno</th>
            <th>Type</th>
            <th>Sevice</th>
            <th><center>Created At</center></th>
            <th><center>Operation</center></th>
         </thead>
          </table>  
          </div>
        </div>
        <!-- /basic initialization -->

      <!-- div col-lg-10 Ending -->
      </div>
</div>
</div>


 


      <?php include 'footer.php'; ?>
      <?php include('bottom.php'); ?>
   </body>


</html>
