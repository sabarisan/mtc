<!DOCTYPE html>
<html>
   <head>
      <title>MTC | CONTROL MANAGEMENT</title>
      <?php 
         include('top.php');
         
         ?>
   </head>
   <body>
      <?php 
         include('header.php');
         ?>
      <?php 
         include('brdcrmb.php');
         ?>
      <?php 
         include('side.php');
         ?>
      <!-- <div class="col-sm-9"> -->

      <!-- Control data Insert -->
      <!-- Sales stats -->
      <div class="panel panel-flat" style="display: none;" id="captfrmins">
         <!-- <br> -->
         <!-- /sales stats -->
         <div class="bg-mybrwn-400-hed">
            <b>CONTROL MANAGEMENT</b>
         </div>
         <!-- panelbody -->
         <div class="panel-body">
            <form method="post" action="<?php echo base_url().'inventory/Control/ctrlins' ?>" id="ctrl_mgmt1">
               <!-- row1 -->
               <div class="row">
                  <div class="col-md-12">
                     <div class="form-group">
                        <label>FolioGroup</label>
                        <div class="input-group">
                           <span class="input-group-addon"><span class="icon icon-podium"></span></span>
                           <input class="form-control" name="folgrp" value="" id="" placeholder="FolioGroup" type="text">
                           <!-- <input type="hidden" id="ctrlid" name="ctrlid" value=""> -->
                        </div>
                     </div>
                  </div>
               </div>
               <!-- row1 Ending -->
               <!-- row2 -->
               <div class="row">
                  <div class="col-md-12">
                     <div class="form-group">
                        <label>Type</label>
                        <div class="input-group">
                           <span class="input-group-addon"><span class="icon icon-split"></span></span>
                           <input class="form-control" name="type" value="" id="" placeholder="Type" type="text">
                        </div>
                     </div>
                  </div>
               </div>
               <!-- row2 Ending -->
               <!-- row3 -->
               <div class="row">
                  <div class="col-md-12">
                     <div class="form-group">
                        <label>Description</label>
                        <div class="input-group">
                           <textarea cols="150" rows="3" class="form-control" value="" id="" placeholder="Enter Description" name="descrip"></textarea>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- row3 Ending -->
         </div>
         <!-- panelbody Ending -->
         <div class="panel-footer">
         <div class="text-right">
         <button type="submit" class="btn btn-danger" style="margin-right: 2%;">Submit <i class="icon-arrow-right14 position-right"></i></button>
         </div>
         </form>
         </div>
      </div>
      <!-- Control data Insert Ending -->

      <!-- Sales stats -->
      <div class="panel panel-flat" style="display: none;" id="ctrlupdte">
         <!-- <br> -->
         <!-- /sales stats -->
         <div class="bg-mybrwn-400-hed">
            <b>CONTROL MANAGEMENT</b>
         </div>
         <!-- panelbody -->
         <div class="panel-body">
            <form method="post" action="#" id="ctrl_mgmtupd">
               <!-- row1 -->
               <div class="row">
                  <div class="col-md-12">
                     <div class="form-group">
                        <label>FolioGroup</label>
                        <div class="input-group">
                           <span class="input-group-addon"><span class="icon icon-podium"></span></span>
                           <input class="form-control" name="folgrp" value="" id="folgrp" placeholder="FolioGroup" type="text">
                           <input type="hidden" id="ctrlid" name="ctrlid" value="">
                        </div>
                     </div>
                  </div>
               </div>
               <!-- row1 Ending -->
               <!-- row2 -->
               <div class="row">
                  <div class="col-md-12">
                     <div class="form-group">
                        <label>Type</label>
                        <div class="input-group">
                           <span class="input-group-addon"><span class="icon icon-split"></span></span>
                           <input class="form-control" name="type" value="" id="type" placeholder="Type" type="text">
                        </div>
                     </div>
                  </div>
               </div>
               <!-- row2 Ending -->
               <!-- row3 -->
               <div class="row">
                  <div class="col-md-12">
                     <div class="form-group">
                        <label>Description</label>
                        <div class="input-group">
                           <textarea cols="150" rows="3" class="form-control" value="" id="descrip" placeholder="Enter Description" name="descrip"></textarea>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- row3 Ending -->
         </div>
         <!-- panelbody Ending -->
         <div class="panel-footer">
         <div class="text-right">
         <button type="button" id="ctrlfrmdt" class="btn btn-danger" style="margin-right: 2%;">Submit <i class="icon-arrow-right14 position-right"></i></button>
         </div>
         </form>
         </div>
      </div>

        <!-- Basic initialization -->
        <div class="panel panel-flat">
          <div class="panel-heading">
            <h5 class="panel-title">Control Management Details</h5>
            <div class="heading-elements">
              &nbsp;&nbsp;&nbsp;<div class="btn btn-info" onclick="ctrlins()"><i class="icon icon-add" style="color: #fff; "></i> Add</div>
              <ul class="icons-list">
                        <li><a data-action="collapse"></a></li>
                        <!-- <li><a data-action="reload"></a></li> -->
                        <!-- <li><a data-action="close"></a></li> -->
                      </ul>
                    </div>
          </div>

          <div class="container-fluid">
              <table class="table datatable-button-init-basic" id="ctrlfrm">
            <thead>
            <th>Sno</th>
            <th>Type</th>
            <th>Description</th>
            <th>Folio Group</th>
            <th><center>Created At</center></th>
            <th><center>Operation</center></th>
         </thead>
          </table>  
          </div>
        </div>
        <!-- /basic initialization -->

      </div>
      </div>
      <!-- Datatables -->
     

      <?php 
         include('footer.php');
         ?>
   </body>
   <?php 
      include('bottom.php');
      ?>
</html>
