<!DOCTYPE html>
<html>
   <head>
      <title>MTC | Folio Management</title>
      <?php include('top.php'); ?>
   </head>
   <body>
      <?php include('header.php'); ?>
      
      <?php include('brdcrmb.php'); ?>

      <?php include('side.php'); ?>

      <!-- col-lg-10 starting -->
      <!-- <div class="col-lg-9"> -->
         <div class="panel" style="position: static;display: none;" id="folidfrm">
            <!-- Sales stats -->
                  <div class="panel panel-flat">
                  <!-- /sales stats -->
                  <div class="bg-mybrwn-400-hed">
                     <b>FOLIO MANAGEMENT</b>
                  </div>
               </div>
            <div class="panel-body">
               <form action="#" method="post" id="folio">
                  <div class="container-fluid">
                     <!-- row1 -->
                     <div class="row">
                        <div class="col-md-6">
                           <div class="form-group">
                              <label>Grp</label>
                                 <div class="input-group">
                                          <span class="input-group-addon"><span class="icon icon-bus"></span></span>
                                          <input class="form-control" value="" id="grptxt" name="grp" placeholder="Grp" type="text">
                                 </div>

                                 <input type="hidden" id="rwidtxt" name="rwid">
                           </div>
                        </div>
                        <div class="col-md-6">
                           <div class="form-group">
                              <label>Nomenclature 1</label>
                                 <div class="input-group">
                                          <span class="input-group-addon"><span class="icon icon-pushpin"></span></span>
                                          <input class="form-control" value="" id="nom1txt" name="nom1" placeholder="Nomenclature 1" type="text">
                                 </div>
                           </div>
                        </div>
                     </div>
                     <!-- row1 Ending -->

                     <!-- row2 -->
                     <div class="row">
                        <div class="col-md-6">
                           <div class="form-group">
                              <label>Nomenclature 2</label>
                                 <div class="input-group">
                                          <span class="input-group-addon"><span class="icon icon-pushpin"></span></span>
                                          <input class="form-control" value="" id="nom2txt" name="nom2" placeholder="Nomenclature 2" type="text">
                                 </div>
                           </div>
                        </div>
                        <div class="col-md-6">
                           <div class="form-group">
                              <label>Nomenclature 3</label>
                                 <div class="input-group">
                                          <span class="input-group-addon"><span class="icon icon-pushpin"></span></span>
                                          <input class="form-control" value="" id="nom3txt" name="nom3" placeholder="Nomenclature 3" type="text">
                                 </div>
                           </div>
                        </div>
                     </div>
                     <!-- row2 Ending -->

                     <!-- row3 -->
                     <div class="row">
                        <div class="col-md-6">
                           <div class="form-group">
                              <label>Part 1</label>
                                  <div class="input-group">
                                          <span class="input-group-addon"><span class="icon icon-split"></span></span>
                                          <input class="form-control" value="" id="prt1txt" name="prt1" placeholder="Part 1" type="text">
                                  </div>
                           </div>
                        </div>
                        <div class="col-md-6">
                           <div class="form-group">
                              <label>Part 2</label>
                                 <div class="input-group">
                                          <span class="input-group-addon"><span class="icon icon-split"></span></span>
                                          <input class="form-control" value="" id="prt2txt" name="prt2" placeholder="Part 2" type="text">
                                 </div>
                           </div>
                        </div>
                     </div>
                     <!-- row3 Ending -->

                     <!-- row4 -->
                     <div class="row">
                        <div class="col-md-6">
                           <div class="form-group">
                              <label>Part 3</label>
                                  <div class="input-group">
                                          <span class="input-group-addon"><span class="icon icon-split"></span></span>
                                          <input class="form-control" value="" id="prt3txt" name="prt3" placeholder="Part 3" type="text">
                                  </div>
                           </div>
                        </div>
                        <div class="col-md-6">
                           <div class="form-group">
                              <label>Lyc 1</label>
                                 <div class="input-group">
                                          <span class="input-group-addon"><span class="icon icon-highlight"></span></span>
                                          <input class="form-control" value="" id="lyc1txt" name="lyc1" placeholder="Lyc 1" type="text">
                                 </div>
                           </div>
                        </div>
                     </div>
                     <!-- row4 Ending -->

                     <!-- row5 -->
                     <div class="row">
                        <div class="col-md-6">
                           <div class="form-group">
                              <label>Lyc 2</label>
                                  <div class="input-group">
                                          <span class="input-group-addon"><span class="icon icon-highlight"></span></span>
                                          <input class="form-control" value="" id="lyc2txt" name="lyc2" placeholder="Lyc 2" type="text">
                                  </div>
                           </div>
                        </div>
                        <div class="col-md-6">
                           <div class="form-group">
                              <label>Lyc 3</label>
                                 <div class="input-group">
                                          <span class="input-group-addon"><span class="icon icon-highlight"></span></span>
                                          <input class="form-control" value="" id="lyc3txt" name="lyc3" placeholder="Lyc 3" type="text">
                                 </div>
                           </div>
                        </div>
                     </div>
                     <!-- row5 Ending -->

                     <!-- row6 -->
                     <div class="row">
                        <div class="col-md-6">
                           <div class="form-group">
                              <label>Type</label>
                                  <div class="input-group">
                                          <span class="input-group-addon"><span class="icon icon-steering-wheel"></span></span>
                                          <input class="form-control" value="" id="typtxt" name="typ" placeholder="Type" type="text">
                                  </div>
                           </div>
                        </div>
                        <div class="col-md-6">
                           <div class="form-group">
                              <label>Class</label>
                                 <div class="input-group">
                                          <span class="input-group-addon"><span class="icon icon-strategy"></span></span>
                                          <input class="form-control" value="" id="clstxt" name="cls" placeholder="Class" type="text">
                                 </div>
                           </div>
                        </div>
                     </div>
                     <!-- row6 Ending -->

                     <!-- row7 -->
                     <div class="row">
                        <div class="col-md-6">
                           <div class="form-group">
                              <label>Ved</label>
                                  <div class="input-group">
                                          <span class="input-group-addon"><span class="icon icon-watch2"></span></span>
                                          <input class="form-control" value="" id="vedtxt" name="ved" placeholder="Ved" type="text">
                                  </div>
                           </div>
                        </div>
                        <div class="col-md-6">
                           <div class="form-group">
                              <label>Uom</label>
                                 <div class="input-group">
                                          <span class="input-group-addon"><span class="icon icon-strategy"></span></span>
                                          <input class="form-control" value="" id="uomtxt" name="uom" placeholder="Uom" type="text">
                                 </div>
                           </div>
                        </div>
                     </div>
                     <!-- row7 Ending -->

                     <!-- row8 -->
                     <div class="row">
                        <div class="col-md-6">
                           <div class="form-group">
                              <label>Maximum</label>
                                  <div class="input-group">
                                          <span class="input-group-addon"><span class="icon icon-move-up2"></span></span>
                                          <input class="form-control" value="" id="maxtxt" name="maxi" placeholder="Maximum" type="text">
                                  </div>
                           </div>
                        </div>
                        <div class="col-md-6">
                           <div class="form-group">
                              <label>Minimum</label>
                                 <div class="input-group">
                                          <span class="input-group-addon"><span class="icon icon-move-down2"></span></span>
                                          <input class="form-control" value="" id="mintxt" name="mini" placeholder="Minimum" type="text">
                                 </div>
                           </div>
                        </div>
                     </div>
                     <!-- row8 Ending -->

                     <!-- row9 -->
                     <div class="row">
                        <div class="col-md-6">
                           <div class="form-group">
                              <label>Icod</label>
                                 <div class="input-group">
                                          <span class="input-group-addon"><span class="icon icon-grid"></span></span>
                                          <input class="form-control" value="" id="icodtxt" name="icod" placeholder="Icod" type="text">
                                 </div>
                           </div>
                        </div>
                        <div class="col-md-6">
                           <div class="form-group">
                              <label>Rcod</label>
                                 <div class="input-group">
                                          <span class="input-group-addon"><span class="icon icon-link"></span></span>
                                          <input class="form-control" value="" id="rcodtxt" name="rcod" placeholder="Rcod" type="text">
                                 </div>
                           </div>
                        </div>
                     </div>
                     <!-- row9 Ending -->

                     <!-- row10 -->
                     <div class="row">
                        <div class="col-md-6">
                           <div class="form-group">
                              <label>Rate</label>
                                 <div class="input-group">
                                          <span class="input-group-addon"><span class="icon icon-stars"></span></span>
                                          <input class="form-control" value="" id="ratetxt" name="rate" placeholder="Rate" type="text">
                                 </div>
                           </div>
                        </div>
                        <div class="col-md-6">
                           <div class="form-group">
                              <label>Grp1</label>
                                 <div class="input-group">
                                          <span class="input-group-addon"><span class="icon icon-make-group"></span></span>
                                          <input class="form-control" value="" id="grp1txt" name="grp1" placeholder="Grp1" type="text">
                                 </div>
                           </div>
                        </div>
                     </div>
                     <!-- row10 Ending -->

                     <!-- row11 -->
                     <div class="row">
                        <div class="col-md-6">
                           <div class="form-group">
                              <label>Stk code</label>
                                 <div class="input-group">
                                          <span class="input-group-addon"><span class="icon icon-lastfm"></span></span>
                                          <input class="form-control" value="" id="stkcdtxt" name="stkcd" placeholder="Stk code" type="text">
                                 </div>
                           </div>
                        </div>
                        <div class="col-md-6">
                           <div class="form-group">
                              <label>Iss code</label>
                                 <div class="input-group">
                                          <span class="input-group-addon"><span class="icon icon-sort-numeric-asc"></span></span>
                                          <input class="form-control" value="" id="isscdtxt" name="isscd" placeholder="Iss code" type="text">
                                 </div>
                           </div>
                        </div>
                     </div>
                     <!-- row11 Ending -->


                     <!-- row12 -->
                     <div class="row">
                        <div class="col-md-12">
                           <div class="form-group">
                              <label>New Field</label>
                                 <div class="input-group">
                                          <span class="input-group-addon"><span class="icon icon-new"></span></span>
                                          <input class="form-control" value="" id="nwfldtxt" name="nwfld" placeholder="New Field" type="text">
                                 </div>
                           </div>
                        </div>
                     </div>
                     <!-- row12 Ending -->

                  </div>
                  <!-- container Ending  -->

            </div>
            <div class="panel-footer">
            <div class="text-right">
            <button type="button" id="folfrmsub" class="btn btn-danger" style="margin-right: 2%;">Submit <i class="icon-arrow-right14 position-right"></i></button>
            </div>	
            </div>
            </form>
         </div>

         <!-- Basic initialization -->
        <div class="panel panel-flat">
          <div class="panel-heading">
            <h5 class="panel-title">Folio Management Details</h5>
            <div class="heading-elements">
              &nbsp;&nbsp;&nbsp;<div class="btn btn-info" onclick="folins()"><i class="icon icon-add" style="color: #fff; "></i> Add</div>
              <ul class="icons-list">
                        <li><a data-action="collapse"></a></li>
                        <!-- <li><a data-action="reload"></a></li> -->
                        <!-- <li><a data-action="close"></a></li> -->
                      </ul>
                    </div>
          </div>

          <div class="container-fluid">
            <!-- division for scrollable -->
            <div class="table-responsive">
              <table class="table datatable-button-init-basic table-responsive" id="folfrm">
            <thead>
            <th>Sno</th>
            <th>Folio</th>
            <th>Grp</th>
            <th>Nom1</th>
            <!-- <th>Nom2</th>
            <th>Nom3</th>
            <th>Par1</th>
            <th>Par2</th>
            <th>Par3</th>
            <th>Lyc1</th>
            <th>Lyc2</th>
            <th>Lyc3</th> -->
            <!-- <th>Type</th> -->
            <th>Class</th>
            <!-- <th>Ved</th> -->
            <!-- <th>Uom</th>   -->
            <!-- <th>Max</th>
            <th>Min</th>
            <th>Icod</th>
            <th>Rcod</th> -->
            <th>rate</th>  
            <!-- <th>grp1</th> -->
            <!-- <th>stkcode</th>
            <th><center>Isscode</center></th>
            <th>New Field</th> -->
            <!-- <th>Createdat</th> -->
            <th><center>Operation</center></th>
         </thead>
          </table> 
          </div>
          <!-- division for scrollable Ending--> 
          </div>
        </div>
        <!-- /basic initialization -->
      <!-- div col-lg-10 Ending -->
      </div>
</div>
</div>

   

      <?php include 'footer.php'; ?>
      <?php include('bottom.php'); ?>
   </body>


</html>
