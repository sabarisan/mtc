<!DOCTYPE html>
<html>
<head>
	<title>MTC | Calendar Of Purchase</title>
	<?php include('top.php'); ?>
</head>
<body>
	<?php include 'header.php'; ?>
	<?php /* include 'brdcrmb.php'; */?>
	

	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">
			 <!-- Clickable title -->
	            <div class="panel panel-white">
					<div class="panel-heading">
						<h6 class="panel-title"><?php echo $prsntbrdcrmb; ?></h6>
						<div class="heading-elements">
							<ul class="icons-list">
		                		<li><a data-action="collapse"></a></li>
		                		<li><a data-action="reload"></a></li>
		                		<li><a data-action="close"></a></li>
		                	</ul>
	                	</div>
					</div>
					<div class="table-responsive">
								<?php 
									include 'ind_headr.php';
								 ?>
					</div>

							<div class="container">
								<form class="" action="#">
									<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label>Group:</label>
										<input type="text" name="group" placeholder="Group" class="form-control">
									</div>
								</div>

								<div class="col-md-6">
									<label>Nam1:</label>
									<div class="row">
										<input type="text" name="" class="form-control" placeholder="Nam1">
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label>Month:</label>
										<select class="select">
											<option value="">Select the month</option>
											<option value="01-07">January &amp; July</option>
											<option value="02-08">February &amp; August</option>
											<option value="03-09">March &amp; September</option>
											<option value="04-10">April &amp; October</option>
											<option value="05-11">May &amp; November</option>
											<option value="06-12">June &amp; December</option>
										</select>
									</div>
									
								</div>

								<div class="col-md-6">
									<div class="form-group">
										<label>Nam2:</label>
										<input type="text" name="" class="form-control" placeholder="Nam2">
									</div>
								</div>
							</div>

							<div class="row">
							  <div class="col-md-12">
								<div class="form-group"> 
										<label>Nam:</label>
										<div class="row">
											<input type="text" name="" class="form-control" placeholder="Nam">
										</div>	
									</div>
								</div>
							</div>
						<br>
						
						<button type="submit" class="btn btn-primary stepy-finish">Submit <i class="icon-check position-right"></i></button>
						<br><br>
					</form>
							</div>
                		
	            </div>
	            <!-- /clickable title -->
			</div>
			<!-- Main content -->
		</div>
		<!-- Page content -->
	</div>
	<!-- Page container -->
	


	<?php /* ?><?php include 'footer.php'; ?> <?php */ ?>
	<?php include 'bottom.php'; ?>
</body>