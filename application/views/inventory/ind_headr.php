

<div class="panel" style="position: static;" id="fleetfm">
            <!-- Sales stats -->
                  <div class="panel panel-flat">
                  <!-- <br> -->
                     <div class="container-fluid">
                        <div class="row text-center">
                           <div class="col-md-3 bg-mybrwn-400">
                           <br>
                              <div class="content-group">
                                 <h5 class="text-semibold no-margin"><i class="icon-add-to-list position-left text-slate"></i> </h5>
                                 <span class="text-muted text-size-small">INDENT SCHEDULE</span>
                              </div>
                           </div>

                           <div class="col-md-3 bg-mybrwn-400">
                           <br>
                              <div class="content-group">
                                 <h5 class="text-semibold no-margin"><i class="icon-calendar52 position-left text-slate"></i> </h5>
                                 <span class="text-muted text-size-small">CALENDAR OF PURCHASE</span>
                              </div>
                           </div>

                           <div class="col-md-2 bg-mybrwn-400">
                           <br>
                              <div class="content-group">
                                 <h5 class="text-semibold no-margin"><i class="icon-task position-left text-slate"></i> </h5>
                                 <span class="text-muted text-size-small">INDENT CHECKLIST</span>
                              </div>
                           </div>

                           <div class="col-md-2 bg-mybrwn-400">
                           <br>
                              <div class="content-group">
                                 <h5 class="text-semibold no-margin"><i class="icon-task position-left text-slate"></i> </h5>
                                 <span class="text-muted text-size-small">BACKORDER CHECKLIST</span>
                              </div>
                           </div>

                           <div class="col-md-2 bg-mybrwn-400">
                           <br>
                              <div class="content-group">
                                 <h5 class="text-semibold no-margin"><i class="icon-notebook position-left text-slate"></i> </h5>
                                 <span class="text-muted text-size-small">FINAL INDENT</span>
                              </div>
                           </div>
                        </div>

                  </div>
                  <!-- /sales stats -->
                  <div class="mystrip">
                     <b>INDENT SCHEDULE</b>   
                  </div>
               </div>
         </div>




<!-- <table class="table table-xlg text-nowrap">
									<tbody>
										<tr>
											<td class="col-md-3">
												<div class="media-right	 media-middle">
													<a href="<?php echo base_url().'inventory/IndentPurchase/'; ?>" class="btn border-indigo-400 text-indigo-400 btn-flat btn-rounded btn-xs btn-icon legitRipple"><i class="icon-calendar"></i></a>
												</div>

												<div class="media-left">
													<h5>Indent Schd</h5>
												</div>
											</td>

											<td class="col-md-3">
												<div class="media-right media-middle">
													<a href="<?php echo base_url().'inventory/IndentPurchase/colp'; ?>" class="btn border-indigo-400 text-indigo-400 btn-flat btn-rounded btn-xs btn-icon legitRipple"><i class="icon-calendar2"></i></a>
												</div>

												<div class="media-left">
													<h5> Calendar Of Purc
													</h5>
												</div>
											</td>

											<td class="col-md-3">
												<div class="media-right media-middle">
													<a href="<?php echo base_url().'inventory/IndentPurchase/indnt_chcklst'; ?>" class="btn border-indigo-400 text-indigo-400 btn-flat btn-rounded btn-xs btn-icon legitRipple"><i class="icon-notebook"></i></a>
												</div>

												<div class="media-left">
													<h5>
														Indent Checklist
													</h5>
												</div>
											</td>

											<td class="text-right col-md-3">
												<div class="media-right media-middle">
													<a href="#" class="btn border-indigo-400 text-indigo-400 btn-flat btn-rounded btn-xs btn-icon legitRipple"><i class="icon-notebook"></i></a>
												</div>

												<div class="media-left">
													<h5>
														Backorder Checklist
													</h5>
												</div>
											</td>

											<td class="text-right col-md-3">
												<div class="media-right media-middle">
													<a href="#" class="btn border-indigo-400 text-indigo-400 btn-flat btn-rounded btn-xs btn-icon legitRipple"><i class="icon-notebook"></i></a>
												</div>

												<div class="media-left">
													<h5>
														Final Indent
													</h5>
												</div>
											</td>
										</tr>
									</tbody>
								</table>	 -->