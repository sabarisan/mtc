<!DOCTYPE html>
<html>
<head>

<title>MTC | DEPOT PURCHASE SECTION MANAGEMENT</title>

<?php 
   include('top.php');
    ?>

</head>



 <body>
 	<?php 
 		include('header.php');
 	 ?>

    <?php 
      include('brdcrmb.php');
     ?>

 	 <?php 
 	 	include('side.php');
 	  ?>

     <!-- depot insert -->
     <!-- Sales stats -->
            
     <!-- depot insert Ending -->

 	  <!-- <div class="col-sm-9"> -->
 	  	 <!-- Sales stats -->
            <div class="panel panel-flat" style="display: none;" id="deptpurchupdte">
               <!-- <br> -->
               <!-- /sales stats -->
               <div class="bg-mybrwn-400-hed">
                  <b>DEPOT PURCHASE SECTION MANAGEMENT</b>
               </div>
               <!-- panelbody -->
               <div class="panel-body">
                  <form method="post" action="#" id="dept_prch">
                  <!-- row1 -->
                     <div class="row">
                        <div class="col-md-6">
                           <div class="form-group">
                              <label>Dep</label>
                                 <div class="input-group" style="display: none;" id="depvlu">
                                          <span class="input-group-addon"><span class="icon icon-podium"></span></span>
                                          <!-- <input class="form-control" name="dep" id="depttxt" placeholder="Dep" type="text"> -->
                                          <p id="depttxt"></p>
                                 </div>
                                 <div class="input-group" style="display: none;" id="deptxt">
                                    <span class="input-group-addon"><span class="icon icon-pencil"></span></span>
                                    <input type="text" id="dept" name="dep" class="form-control" placeholder="Dep">
                                 </div>
                           </div>
                        </div>
                        <div class="col-md-6">
                           <div class="form-group">
                              <label>Name</label>
                                 <div class="input-group">
                                          <span class="input-group-addon"><span class="icon icon-split"></span></span>
                                          <input class="form-control" name="nam" id="nmetxt" placeholder="Name" type="text">
                                 </div>
                           </div>
                        </div>
                     </div>
                     <!-- row1 Ending -->
                     <input type="hidden" name="rwid" id="rwid" value="">
                     <!-- row2 -->
                     <div class="row">
                        <div class="col-md-6">
                           <div class="form-group">
                              <label>Name1</label>
                                 <div class="input-group">
                                          <span class="input-group-addon"><span class="icon icon-user"></span></span>
                                          <input class="form-control" name="nam1" id="nme1txt" placeholder="Name1" type="text">
                                 </div>
                           </div>
                        </div>
                        <div class="col-md-6">
                           <div class="form-group">
                              <label>Name2</label>
                                 <div class="input-group">
                                          <span class="input-group-addon"><span class="icon icon-user"></span></span>
                                          <input class="form-control" name="nam2" id="nme2txt" placeholder="Name2" type="text">
                                 </div>
                           </div>
                        </div>
                     </div>
                     <!-- row2 Ending -->


                     <!-- row3 -->
                     <div class="row">
                        <div class="col-md-6">
                           <div class="form-group">
                              <label>Nm</label>
                                 <div class="input-group">
                                          <span class="input-group-addon"><span class="icon icon-lifebuoy"></span></span>
                                          <input class="form-control" name="nm" id="nmtxt" placeholder="Nm" type="text">
                                 </div>
                           </div>
                        </div>
                        <div class="col-md-6">
                           <div class="form-group">
                              <label>Alpha</label>
                                 <div class="input-group">
                                          <span class="input-group-addon"><span class="icon icon-sort-alpha-asc"></span></span>
                                          <input class="form-control" name="alp" id="alphtxt" placeholder="Alpha" type="text">
                                 </div>
                           </div>
                        </div>
                     </div>
                     <!-- row3 Ending -->

               </div>
               <!-- panelbody Ending -->
               <div class="panel-footer">
               		<div class="text-right">
                        <button type="submit" id="" class="btn btn-danger" style="margin-right: 2%;">Submit <i class="icon-arrow-right14 position-right"></i></button>
                  </div>
                </form>
               </div>
            </div>

             <!-- Basic initialization -->
        <div class="panel panel-flat">
          <div class="panel-heading">
            <h5 class="panel-title">Depot Purchase Section Management Details</h5>
            <div class="heading-elements">
              &nbsp;&nbsp;&nbsp;<div class="btn btn-info" onclick="deptprchins()"><i class="icon icon-add" style="color: #fff; "></i> Add</div>
              <ul class="icons-list">
                        <li><a data-action="collapse"></a></li>
                        <!-- <li><a data-action="reload"></a></li> -->
                        <!-- <li><a data-action="close"></a></li> -->
                      </ul>
                    </div>
          </div>

          <div class="container-fluid">
              <table class="table datatable-button-init-basic" id="deptpurchfrm">
            <thead>
            <th>Sno</th>
            <th>Code</th>
            <th>Name</th>
            <th>Name1</th>
            <th>Name2</th>
            <!-- <th>Nm</th> -->
            <th>Alpha</th>
            <th><center>Created At</center></th>
            <th><center>Operation</center></th>
         </thead>
          </table>  
          </div>
        </div>
        <!-- /basic initialization -->
 	  </div>
</div>

   


 	 <?php 
 	 	include('footer.php');
 	  ?>

 </body>

  <?php 
 	 	include('bottom.php');
 	  ?>
</html>