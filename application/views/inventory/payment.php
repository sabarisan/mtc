<!DOCTYPE html>
<html>
   <head>
      <title>MTC | Payment Terms Management</title>
      <?php include('top.php'); ?>
   </head>
   <body>
      <?php include('header.php'); ?>
      <!-- Page header -->
      <!-- col-sm-10 starting -->
      <?php include('brdcrmb.php'); ?>
      <?php include('side.php'); ?>
      <!-- Division for content wrapper -->
      <!-- <div class="col-sm-9"> -->
         <div class="panel" style="position: static;display: none;" id="paymntfrm">
            <!-- Sales stats -->
            <div class="panel panel-flat">
               <!-- /sales stats -->
               <div class="bg-mybrwn-400-hed">
                  <b>PAYMENT TERMS MANAGEMENT</b>
               </div>
            </div>
            <div class="panel-body">
               <form action="#" method="post" id="paymnt">

                  <!-- container song -->
                  <div class="container-fluid">
                     <!-- row1 -->
                     <div class="row">
                        <div class="col-md-12">
                           <div class="form-group">
                              <label>Name</label>
                                 <div class="input-group">
                                          <span class="input-group-addon"><span class="icon icon-user"></span></span>
                                          <input class="form-control" name="nme" value="" id="nmetxt" placeholder="Name" type="text">
                                       </div>
                           </div>
                        </div>
                     </div>
                     <!-- row1 Ending -->

                     <input type="hidden" id="idtxt" name="rwid">

                     <!-- row2 -->
                     <div class="row">
                        <div class="col-md-12">
                           <div class="form-group">
                              <label>Description</label>
                                 <div class="input-group">
                                 <textarea rows="3" cols="150" name="descrp" id="descrptxt" class="form-control" placeholder="Enter the Description"></textarea>
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-- row2 Ending -->

                  </div>
                  <!-- container-fluid Ending -->
            </div>
            <div class="panel-footer">
            <div class="text-right">
            <button type="button" id="pymntsbmt" class="btn btn-danger" style="margin-right: 2%;">Submit <i class="icon-arrow-right14 position-right"></i></button>
            </div>   
            </div>
            </form>
         </div>
         <!-- div col-sm-10 Ending -->
         <!-- Basic datatable -->
            <div class="panel panel-flat">
               <div class="panel-heading">
                  <h5 class="panel-title">Payment Terms Management</h5>
                  <div class="heading-elements">
                     <button type="button" name="" id="grpmgmtentr" onclick="pymntins()" class="btn btn-info"><i class="icon icon-add" style="color: #fff;"></i>ADD</button>
                     <ul class="icons-list">
                           <li><a data-action="collapse"></a></li>
                     <!--       <li><a data-action="reload"></a></li>
                           <li><a data-action="close"></a></li> -->
                        </ul>
                     </div>
               </div>

               <table id="DataTables_Table_0" class="table datatable-button-flash-name dataTable no-footer" role="grid" aria-describedby="DataTables_Table_0_info">
                  <thead>
                     <tr>
                        <th>Sno</th>
                        <th>Description</th>
                        <th>Name1</th>
                        <th>Createddate</th>
                        <th>Updateddate</th>
                        <th>Operation</th>
                     </tr>
                  </thead>
                  <tbody>
                     <?php if (isset($pymntmgmt)) { ?>

                        <?php $i=1; foreach ($pymntmgmt as $pymnt) { ?>
                           <tr>
                              <td><?php echo $i; ?></td>
                              <td style="width: 30%;"><?php echo $pymnt->description; ?></td>
                              <td><?php echo $pymnt->name1; ?></td>
                              <td><?php echo $pymnt->createddate; ?></td>
                              <td><?php echo $pymnt->updateddate; ?></td>
                              <!-- <td><?php echo $pymnt->updateddate; ?></td> -->

                              <td><div><center><i class="btn btn-danger glyphicon glyphicon-trash" style="font-size:12px;text-transform:lowercase;" onclick="pymntrmv('<?php echo $pymnt->id;?>')"></i> <a href="#"> <i class="glyphicon" style="font-size:12px;text-transform:lowecase;" onclick="pymntupdte('<?php echo $pymnt->id;?>','<?php echo $pymnt->description;?>','<?php echo $pymnt->name1;?>');"><i class="btn btn-success glyphicon glyphicon-pencil" style="font-size:12px;"></i></i></a></center></div></td>
                           </tr>

                        <?php $i++; } ?>
                   <?php } ?>
                  </tbody>
               </table>
            </div>
            <!-- /basic datatable -->
      </div>
      </div>
   </div>



      <!-- Division for content wrapper Ending -->
      <?php include 'footer.php'; ?>
      <?php include('bottom.php'); ?>
   </body>
</html>
