<!DOCTYPE html>
<html>
<head>
	<title>MTC | TYRE MANAGEMENT</title>
	<?php include('top.php'); ?>
</head>
<body>
	<?php include('header.php'); ?>

	 <?php include('brdcrmb.php'); ?>

	   <?php include('side.php'); ?>

	   <!-- <div class="col-sm-9"> -->
         <div class="panel" style="position: static;display: none;" id="tyrefrm">
            <!-- Sales stats -->
            <div class="panel panel-flat">
               <!-- /sales stats -->
               <div class="bg-mybrwn-400-hed">
                  <b>TYRE MANAGEMENT</b>
               </div>
            </div>
            <div class="panel-body">
               <form action="#" method="post" id="tyre">

                  <!-- container-fluid -->
                  <div class="container-fluid">
                     <!-- row1 -->
                     <div class="row">
                        <div class="col-md-6">
                           <div class="form-group">
                              <label>Code</label>
                                 <div class="input-group">
                                     <span class="input-group-addon"><span class="icon icon-weather-windy"></span></span>
                                     <input class="form-control" value="" id="codtxt" name="cde" placeholder="Code" type="text">
                                  </div>
                           </div>
                        </div>
                        <input type="hidden" id="rwidtxt" name="rwid">
                        <div class="col-md-6">
                           <div class="form-group">
                              <label>Folio</label>
                                 <div class="input-group">
                                       <span class="input-group-addon"><span class="icon icon-user"></span></span>
                                       <input class="form-control" value="" id="foltxt" name="fol" placeholder="Folio" type="text">
                                    </div>
                           </div>
                        </div>
                     </div>
                     <!-- row1 Ending -->

                     <!-- row2 -->
                     <div class="row">
                        <div class="col-md-6">
                           <div class="form-group">
                              <label>Tocode</label>
                                 <div class="input-group">
                                     <span class="input-group-addon"><span class="icon icon-user"></span></span>
                                     <input class="form-control" value="" id="tocdetxt" name="tocde" placeholder="Tocode" type="text">
                                  </div>
                           </div>
                        </div>

                        <div class="col-md-6">
                           <div class="form-group">
                              <label>Date</label>
                                 <div class="input-group">
                                     <span class="input-group-addon"><span class="icon icon-calendar2"></span></span>
                                     <input class="form-control" value="" id="dtetxt" name="dte" type="date">
                                  </div>
                           </div>
                        </div>
                     </div>
                     <!-- row2 Ending -->


                     <!-- row3 -->
                     <div class="row">
                        <div class="col-md-6">
                           <div class="form-group">
                              <label>Mtcno</label>
                                 <div class="input-group">
                                     <span class="input-group-addon"><span class="icon icon-file-spreadsheet"></span></span>
                                     <input class="form-control" value="" id="mtcnotxt" name="mtcno" placeholder="Mtcno" type="text">
                                  </div>
                           </div>
                        </div>

                        <div class="col-md-6">
                           <div class="form-group">
                              <label>Manf</label>
                                 <div class="input-group">
                                       <span class="input-group-addon"><span class="icon icon-file-spreadsheet"></span></span>
                                       <input class="form-control" value="" id="manftxt" name="manf" placeholder="Manf" type="text">
                                    </div>
                           </div>
                        </div>
                     </div>
                     <!-- row3 Ending -->

                     <!-- row4 -->
                     <div class="row">
                        <div class="col-md-12">
                           <div class="form-group">
                              <label>Make</label>
                                 <div class="input-group">
                                       <span class="input-group-addon"><span class="icon icon-file-spreadsheet"></span></span>
                                       <input class="form-control" value="" id="maktxt" name="mak" placeholder="Make" type="text">
                                    </div>
                           </div>
                        </div>
                     </div>
                     <!-- row4 Ending -->

                  </div>
                  <!-- container-Fluid Ending -->

            </div>
            <div class="panel-footer">
            <div class="text-right">
            <button type="button" id="tyrbtn" class="btn btn-danger" style="margin-right: 2%;">Submit <i class="icon-arrow-right14 position-right"></i></button>
            </div>   
            </div>
            </form>
         </div>
         <!-- div col-sm-10 Ending -->

         <!-- Basic datatable -->
            <div class="panel panel-flat">
               <div class="panel-heading">
                  <h5 class="panel-title">Tyre Management</h5>
                  <div class="heading-elements">
                     <button type="button" name="" id="grpmgmtentr" onclick="tyreins()" class="btn btn-info"><i class="icon icon-add" style="color: #fff;"></i>ADD</button>
                     <ul class="icons-list">
                           <li><a data-action="collapse"></a></li>
                     <!--       <li><a data-action="reload"></a></li>
                           <li><a data-action="close"></a></li> -->
                        </ul>
                     </div>
               </div>

               <table id="DataTables_Table_0" class="table datatable-button-flash-name dataTable no-footer" role="grid" aria-describedby="DataTables_Table_0_info">
                  <thead>
                     <tr>
                        <th>Sno</th>
                        <th>Code</th>
                        <th>Folio</th>
                        <th>Tocode</th>
                        <th>Date</th>
                        <th>Mtcno</th>
                        <th>Manf</th>
                        <!-- <th>Make</th> -->
                        <th>Createddate</th>
                        <!-- <th>Updateddate</th> -->
                        <th>Operation</th>
                     </tr>
                  </thead>
                  <tbody>
                     <?php if (isset($tyrmgmt)) { ?>

                        <?php $i=1; foreach ($tyrmgmt as $tyrmg) { ?>
                           <tr>
                              <td><?php echo $i; ?></td>
                              <td><?php echo $tyrmg->code; ?></td>
                              <td><?php echo $tyrmg->folio; ?></td>
                              <td><?php echo $tyrmg->tocode; ?></td>
                              <td><?php echo $tyrmg->date; ?></td>
                              <td><?php echo $tyrmg->mtcno; ?></td>
                              <td><?php echo $tyrmg->manf; ?></td>
                              <!-- <td><?php echo $tyrmg->make; ?></td> -->
                              <td><?php echo $tyrmg->createddate; ?></td>
                              <td>
                                  <div>
                                    <center>
                                      <i class="btn btn-danger glyphicon glyphicon-trash" style="font-size:12px;" onclick="tyrrmv('<?php echo $tyrmg->id;?>')">
                                      </i>
                                      <a href="#">
                                        <i class="btn btn-success glyphicon" style="font-size:12px;text-transform:lowercase;" onclick="tyrupdte('<?php echo $tyrmg->id;?>','<?php echo $tyrmg->code;?>','<?php echo $tyrmg->folio;?>','<?php echo $tyrmg->tocode;?>','<?php echo $tyrmg->date;?>','<?php echo $tyrmg->mtcno;?>','<?php echo $tyrmg->manf;?>','<?php echo $tyrmg->make;?>');">
                                          <i class="glyphicon glyphicon-pencil" style="font-size:12px;">
                                          </i>  
                                        </i>
                                      </a>
                                    </center>
                                  </div>
                              </td>
                           </tr>

                        <?php $i++; } ?>
                   <?php } ?>
                  </tbody>
               </table>
            </div>
            <!-- /basic datatable -->
      </div>
      </div>
      <!-- Division for content wrapper Ending -->
    </div>

	 <?php 
	 	include('footer.php'); 
	 ?>
</body>
	<?php 
		include('bottom.php');
	 ?>
</html>