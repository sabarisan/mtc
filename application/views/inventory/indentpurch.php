<!DOCTYPE html>
<html>
<head>
	<title>MTC |Indent Checklist</title>
	<?php include('top.php'); ?>
</head>
<body>
	<?php include 'header.php'; ?>
	<!-- <?php //include 'brdcrmb.php'; ?> -->
	

	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">
			 <!-- Clickable title -->
	            <div class="panel panel-white">
					<div class="panel-heading" style="background-color: #ed9522;">
						<h6 class="panel-title"><?php echo $prsntbrdcrmb; ?></h6>
						<div class="heading-elements">
							<!-- <ul class="icons-list">
		                		<li><a data-action="collapse"></a></li>
		                		<li><a data-action="reload"></a></li>
		                		<li><a data-action="close"></a></li>
		                	</ul> -->
	                	</div>
					</div>
					<div class="table-responsive">
								<?php 
									include 'ind_headr.php';
								 ?>
					</div>

							<div class="container">
								<form action="#" method="post">
							<!-- <legend class="text-semibold">Indent Schedule</legend> -->

							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label><b>Folio:</b></label>
										<select class="select" name="indnt_fol">
											<option>Select the Folio</option>
											<?php foreach ($selct as $sel) {?>
												<option value="<?php echo $sel->FOL; ?>"><?php echo $sel->FOL; ?></option>
											<?php } ?>
										</select>
										<!-- <input type="text" id="mysearch" class="form-control" placeholder="Folio (auto suggest)" name="fol"> -->
									</div>
								</div>

								<div class="col-md-6">
									<div class="form-group">
										<label><b>Folio Name:</b></label>
										<input type="text" class="form-control" disabled="disabled" name="" placeholder="">
									</div>
								</div>

							</div>

							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label><b>Partno1:</b></label>
										<input type="text" class="form-control" placeholder="Partno1" name="indnt_part1">
									</div>
								</div>

								<div class="col-md-6">
									<div class="form-group">
										<label><b>Partno2:</b></label>
										<input type="text" name="indnt_part2" class="form-control" placeholder="Partno2">
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label><b>Period:</b></label>
										<select class="select" name="indnt_perd">
											<option value="">Select the Period</option>
											<option value="H">Halfaly</option>
											<option value="A">Annual</option>
										</select>
									</div>
								</div>

								<div class="col-md-6">
								  <div class="form-group">
								  	<label><b>Month:</b></label>
										<select class="select">
											<option value="">Select the Month</option>
											<option value="01">January</option>
											<option value="02">February</option>
											<option value="03">March</option>
											<option value="04">April</option>
											<option value="05">May</option>
											<option value="06">June</option>
											<option value="07">July</option>
											<option value="08">August</option>
											<option value="09">September</option>
											<option value="10">October</option>
											<option value="11">November</option>
											<option value="12">December</option>
										</select>
								  </div>
								</div>
							</div>

							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label><b>Group1:</b></label>
										<input type="text" name="" class="form-control" placeholder="Group1">
									</div>
								</div>

								<div class="col-md-6">
									<label><b>Group2:</b></label>
									<div class="row">
										<input type="text" name="" class="form-control" placeholder="Group2">
									</div>
								</div>
							</div>

						<!-- </fieldset> -->

						
						<button type="submit" class="btn btn-primary stepy-finish">Submit <i class="icon-check position-right"></i></button>
						<br><br>
					</form>
							</div>
                		
	            </div>
	            <!-- /clickable title -->
			</div>
			<!-- Main content -->
		</div>
		<!-- Page content -->
	</div>
	<!-- Page container -->
	


	<?php include 'footer.php'; ?>
	<?php include 'bottom.php'; ?>
</body>