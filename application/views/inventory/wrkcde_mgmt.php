<!DOCTYPE html>
<html>
<head>
	<title>MTC | WORK CODE MANAGEMENT</title>
	<?php include('top.php'); ?>
</head>
<body>
	<?php include('header.php'); ?>

	 <?php include('brdcrmb.php'); ?>

	   <?php include('side.php'); ?>


	   <!-- <div class="col-sm-9"> -->
         <div class="panel" style="position: static;display: none;" id="wrkfrmupdte">
            <!-- Sales stats -->
            <div class="panel panel-flat">
               <!-- /sales stats -->
               <div class="bg-mybrwn-400-hed">
                  <b>WORK CODE MANAGEMENT</b>
               </div>
            </div>
            <div class="panel-body">
               <form action="#" method="post" id="wrk_cde">
                  <div class="container-fluid">
                   <!-- row1 -->
                     <div class="row">
                        <div class="col-md-6">
                           <div class="form-group">
                              <label>User1</label>
                                 <div class="input-group">
                                     <span class="input-group-addon"><span class="icon icon-user"></span></span>
                                     <input class="form-control" id="usr1txt" value="" name="usr1" placeholder="User1" type="text">
                                  </div>
                           </div>
                        </div>
                        <input type="hidden" name="rwid" id="rwid">
                        <div class="col-md-6">
                           <div class="form-group">
                              <label>Userq</label>
                                 <div class="input-group">
                                       <span class="input-group-addon"><span class="icon icon-user"></span></span>
                                       <input class="form-control" id="usrqtxt" value="" name="usrq" placeholder="Userq" type="text">
                                    </div>
                           </div>
                        </div>
                     </div>
                     <!-- row1 Ending -->

                     <!-- row2 -->
                     <div class="row">
                        <div class="col-md-6">
                           <div class="form-group">
                              <label>User2</label>
                                 <div class="input-group">
                                     <span class="input-group-addon"><span class="icon icon-user"></span></span>
                                     <input class="form-control" id="usr2txt" value="" name="usr2" placeholder="User2" type="text">
                                  </div>
                           </div>
                        </div>

                        <div class="col-md-6">
                           <div class="form-group">
                              <label>User3</label>
                                 <div class="input-group">
                                     <span class="input-group-addon"><span class="icon icon-user"></span></span>
                                     <input type="text" name="usr3" value="" id="usr3txt" class="form-control">
                                    <!--  <input class="form-control" id="usr3txt" value="" name="usr3" type="date"> -->
                                  </div>
                           </div>
                        </div>
                     </div>
                     <!-- row2 Ending -->

                      <!-- row3 -->
                     <div class="row">
                        <div class="col-md-12">
                           <div class="form-group">
                              <label>Nomenclature 1</label>
                                 <div class="input-group">
                                       <span class="input-group-addon"><span class="icon icon-pushpin"></span></span>
                                       <input class="form-control" id="nom1txt" value="" name="nom1" placeholder="Nom1" type="text">
                                    </div>
                           </div>
                        </div>
                     </div>
                     <!-- row3 Ending -->
                </div>
                <!-- container-fluid Ending -->
            </div>
            <div class="panel-footer">
            <div class="text-right">
            <button type="button" id="wrkupd" class="btn btn-danger" style="margin-right: 2%;">Submit <i class="icon-arrow-right14 position-right"></i></button>
            </div>   
            </div>
            </form>
         </div>
         <!-- div col-sm-10 Ending -->
         <!-- Basic datatable -->
            <div class="panel panel-flat">
               <div class="panel-heading">
                  <h5 class="panel-title">Work Management</h5>
                  <div class="heading-elements">
                     <button type="button" name="" id="wrkentr" onclick="wrkins()" class="btn btn-info"><i class="icon icon-add" style="color: #fff;"> </i> ADD</button>
                     <ul class="icons-list">
                           <li><a data-action="collapse"></a></li>
                     <!--       <li><a data-action="reload"></a></li>
                           <li><a data-action="close"></a></li> -->
                        </ul>
                     </div>
               </div>

               <table id="DataTables_Table_0" class="table datatable-button-flash-name dataTable no-footer" role="grid" aria-describedby="DataTables_Table_0_info">
                  <thead>
                     <tr>
                        <th>Sno</th>
                        <th>User1</th>
                        <th>Userq</th>
                        <th>user2</th>
                        <th>User3</th>
                        <th>Nom1</th>
                        <!-- <th>Addr3</th> -->
                        <!-- <th>Addr4</th> -->
                        <th>Createddate</th>
                        <th>Operation</th>
                     </tr>
                  </thead>
                  <tbody>
                     <?php if (isset($wrkmgmt)) { ?>

                        <?php $i=1; foreach ($wrkmgmt as $wrk) { ?>
                           <tr>
                              <td><?php echo $i; ?></td>
                              <td><?php echo $wrk->user1; ?></td>
                              <td><?php echo $wrk->userq; ?></td>
                              <td><?php echo $wrk->user2; ?></td>
                              <td><?php echo $wrk->user3; ?></td>
                              <td><?php echo $wrk->nom1; ?></td>
                              <?php /* ?><td><?php echo $wrk->add4; ?></td> <?php */?>
                              <td><?php echo $wrk->createddate; ?></td>
                              <!-- <td><?php echo $wrk->updateddate; ?></td> -->
                              <td><div><center><i class="btn btn-danger glyphicon glyphicon-trash" style="font-size:12px;text-transform:lowercase;" onclick="wrkrmv('<?php echo $wrk->id;?>')"></i> <a href="#"><i class="btn btn-success glyphicon" style="font-size:12px;text-transform:lowecase;" onclick="wrkupdte('<?php echo $wrk->id;?>','<?php echo $wrk->user1;?>','<?php echo $wrk->userq;?>','<?php echo $wrk->user2;?>','<?php echo $wrk->user3;?>','<?php echo $wrk->nom1;?>');"><i class="glyphicon glyphicon-pencil" style="font-size:12px;"></i></i></a></center></div></td>
                           </tr>

                        <?php $i++; } ?>
                   <?php } ?>
                  </tbody>
               </table>
            </div>
            <!-- /basic datatable -->
      </div>
      </div>
      <!-- Division for content wrapper Ending -->
    </div>
    <!-- <button onclick="confirmDelete2()">Click me</button> -->
    <!-- <script type="text/javascript">
      function confirmDelete2() {
    swal({
        title: "Are you sure?",
        text: "You will not be able to recover this imaginary file!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: false
    }, function (isConfirm) {
        if (!isConfirm) return;
        $.ajax({
            url: "/echo/json",
            type: "POST",
            data: {
                id: 5
            },
            dataType: "html",
            success: function () {
                swal("Done!", "It was succesfully deleted!", "success");
            },
            error: function (xhr, ajaxOptions, thrownError) {
                swal("Error deleting!", "Please try again", "error");
            }
        });
    });
}
    </script> -->

	 <?php 
	 	include('footer.php'); 
	 ?>
   <?php 
    include('bottom.php');
   ?>
</body>
	
</html>