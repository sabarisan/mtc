<!DOCTYPE html>
<html>
   <head>
      <title>MTC | Delivery Terms Management</title>
      <?php include('top.php'); ?>
   </head>
   <body>
      <?php include('header.php'); ?>
      
      <?php include('brdcrmb.php'); ?>

         <?php 
            include('side.php');
          ?>
         <!-- col-sm-8 starting -->

        


         <!-- <div class="col-sm-9"> -->
            <div class="panel" style="position: static;display: none;" id="dlvrupdte">
               <!-- Sales stats -->
               <div class="panel panel-flat">
                  <!-- <br> -->
                  <!-- /sales stats -->
                  <div class="bg-mybrwn-400-hed">
                     <b>DELIVERY TERMS MANAGEMENT</b>
                  </div>
               </div>
               <div class="panel-body">
                  <form action="#" id="delvr_trm_mgmt" method="post">
                     <!-- row1 -->
                     <div class="row">
                        <div class="col-sm-12">
                           <div class="form-group">
                              <label>Name</label>
                              <div class="input-group">
                                    <span class="input-group-addon"><span class="icon icon-user"></span></span>
                                        <select name="nme" class="form-control select" id="nmetxt">
                                          <option value="" selected="selected">Select Name</option>
                                          <option value="name1">Name1</option>
                                          <option value="name2">Name2</option>
                                    </select> 
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-- row1 Ending -->
                     <input type="hidden" id="rwidtxt" name="delvrid">
                      <!-- row2 -->
                     <div class="row">
                        <div class="col-sm-12">
                           <div class="form-group">
                              <label>Description</label>
                              <div class="input-group">
                                    <textarea cols="150" rows="3" class="form-control" id="descrtxt" placeholder="Enter Description" name="descrip"></textarea>
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-- row2 Ending -->
               </div>
               <div class="panel-footer">
               <div class="text-right">
               <button type="submit" id="" class="btn btn-danger" style="margin-right: 2%;">Submit <i class="icon-arrow-right14 position-right"></i></button>
               </div>	
               </div>
               </form>
            </div>

            <!-- Basic initialization -->
        <div class="panel panel-flat">
          <div class="panel-heading">
            <h5 class="panel-title">Delivery Terms Management Details</h5>
            <div class="heading-elements">
              &nbsp;&nbsp;&nbsp;<div class="btn btn-info" onclick="delvins()"><i class="icon icon-add" style="color: #fff; "></i> Add</div>
              <ul class="icons-list">
                        <li><a data-action="collapse"></a></li>
                        <!-- <li><a data-action="reload"></a></li> -->
                        <!-- <li><a data-action="close"></a></li> -->
                      </ul>
                    </div>
          </div>

          <div class="container-fluid">
              <table class="table datatable-button-init-basic" id="dlvrfrm">
            <thead>
            <th>Sno</th>
            <th>Code</th>
            <th>Description</th>
            <th>Name1</th>
            <th><center>Created At</center></th>
            <th><center>Operation</center></th>
         </thead>
          </table>  
          </div>
        </div>
        <!-- /basic initialization -->

         </div>
         <!-- div col-sm-8 Ending -->


      </div>
</div>

   
   

      <?php include('footer.php'); ?>	
      <?php include('bottom.php'); ?>
   </body>

   

   </html>
