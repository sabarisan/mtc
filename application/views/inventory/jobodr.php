<!DOCTYPE html>
<html>
   <head>
      <title>MTC | Job Order Management</title>
      <?php include('top.php'); ?>
   </head>
   <body>
      <?php include('header.php'); ?>
      
      <?php include('brdcrmb.php'); ?>

      <?php 
         include('side.php');
       ?>
      <!-- col-lg-10 starting -->
      <!-- <div class="col-lg-9"> -->
         <div class="panel" style="position: static;">
            <!-- Sales stats -->
                  <div class="panel panel-flat">
                  <!-- <br> -->
                     <!-- <div class="container-fluid">
                        <div class="row text-center">
                           <div class="col-md-4 bg-mybrwn-400">
                           <br>
                              <div class="content-group">
                                 <h5 class="text-semibold no-margin"><i class="icon-bus position-left text-slate"></i> 4016</h5>
                                 <span class="text-muted text-size-small">TOTAL NO OF FLEET</span>
                              </div>
                           </div>

                           <div class="col-md-4 bg-mybrwn-400">
                           <br>
                              <div class="content-group">
                                 <h5 class="text-semibold no-margin"><i class="icon-calendar52 position-left text-slate"></i> 70</h5>
                                 <span class="text-muted text-size-small">FLEET ADDED IN LAST MONTH</span>
                              </div>
                           </div>

                           <div class="col-md-4 bg-mybrwn-400">
                           <br>
                              <div class="content-group">
                                 <h5 class="text-semibold no-margin"><i class="icon-task position-left text-slate"></i> 4016</h5>
                                 <span class="text-muted text-size-small">TOTAL NO OF ACTIVE FLEET</span>
                              </div>
                           </div>
                        </div>

                  </div> -->
                  <!-- /sales stats -->
                  <div class="bg-mybrwn-400-hed">
                     <b>JOB ORDER MANAGEMENT</b>
                  </div>
               </div>
            <div class="panel-body">
               <form action="#" method="post" id="jobordr">
               <!-- container song -->
                  <div class="container-fluid">
                     <!-- row1 -->
                     <div class="row">
                        <div class="col-md-6">
                           <div class="form-group">
                              <label>Dt</label>
                                 <div class="input-group">
                                          <span class="input-group-addon"><span class="icon icon-drive"></span></span>
                                          <input class="form-control" name="dt" placeholder="Dt" type="text">
                                       </div>
                           </div>
                        </div>
                        <div class="col-md-6">
                           <div class="form-group">
                              <label>Type</label>
                                 <div class="input-group">
                                          <span class="input-group-addon"><span class="icon icon-pushpin"></span></span>
                                          <input class="form-control" name="typ" placeholder="Type" type="text">
                                       </div>
                           </div>
                        </div>
                     </div>
                     <!-- row1 Ending -->

                     <!-- row2 -->
                     <div class="row">
                        <div class="col-md-6">
                           <div class="form-group">
                              <label>Form</label>
                                 <div class="input-group">
                                          <span class="input-group-addon"><span class="icon icon-newspaper"></span></span>
                                          <input class="form-control" name="frm" placeholder="Form" type="text">
                                       </div>
                           </div>
                        </div>
                        <div class="col-md-6">
                           <div class="form-group">
                              <label>Bomno</label>
                                 <div class="input-group">
                                          <span class="input-group-addon"><span class="icon icon-split"></span></span>
                                          <input class="form-control" name="bomno" placeholder="Bomno" type="text">
                                       </div>
                           </div>
                        </div>
                     </div>
                     <!-- row2 Ending -->


                     <!-- row3 -->
                     <div class="row">
                        <div class="col-md-6">
                           <div class="form-group">
                              <label>Fleet</label>
                                 <div class="input-group">
                                          <span class="input-group-addon"><span class="icon icon-bus"></span></span>
                                          <input class="form-control" name="fleet" placeholder="Fleet" type="text">
                                       </div>
                           </div>
                        </div>
                        <div class="col-md-6">
                           <div class="form-group">
                              <label>Chasno</label>
                                 <div class="input-group">
                                          <span class="input-group-addon"><span class="icon icon-forrst"></span></span>
                                          <input class="form-control" name="chsno" placeholder="Chasno" type="text">
                                       </div>
                           </div>
                        </div>
                     </div>
                     <!-- row3 Ending -->


                     <!-- row4 -->
                     <div class="row">
                        <div class="col-md-6">
                           <div class="form-group">
                              <label>Engine no</label>
                                 <div class="input-group">
                                          <span class="input-group-addon"><span class="icon icon-hash"></span></span>
                                          <input class="form-control" name="engno" placeholder="Engine no" type="text">
                                       </div>
                           </div>
                        </div>
                        <div class="col-md-6">
                           <div class="form-group">
                              <label>Dtopn</label>
                                 <div class="input-group">
                                          <span class="input-group-addon"><span class="icon icon-highlight"></span></span>
                                          <input class="form-control" name="dtpn" placeholder="Dtopn" type="text">
                                       </div>
                           </div>
                        </div>
                     </div>
                     <!-- row4 Ending -->

                     <!-- row5 -->
                     <div class="row">
                        <div class="col-md-6">
                           <div class="form-group">
                              <label>Manhour</label>
                                 <div class="input-group">
                                          <span class="input-group-addon"><span class="icon icon-watch"></span></span>
                                          <input class="form-control" name="manhr" placeholder="Manhour" type="text">
                                       </div>
                           </div>
                        </div>
                        <div class="col-md-6">
                           <div class="form-group">
                              <label>Pmac</label>
                                 <div class="input-group">
                                          <span class="input-group-addon"><span class="icon icon-spotlight2"></span></span>
                                          <input class="form-control" name="pmac" placeholder="Pmac" type="text">
                                       </div>
                           </div>
                        </div>
                     </div>
                     <!-- row5 Ending -->

                     <!-- row6 -->
                     <div class="row">
                        <div class="col-md-6">
                           <div class="form-group">
                              <label>Pmcc</label>
                                 <div class="input-group">
                                          <span class="input-group-addon"><span class="icon icon-snowflake"></span></span>
                                          <input class="form-control" name="pmcc" placeholder="Pmcc" type="text">
                                       </div>
                           </div>
                        </div>
                        <div class="col-md-6">
                           <div class="form-group">
                              <label>Cmac</label>
                                 <div class="input-group">
                                          <span class="input-group-addon"><span class="icon icon-weather-windy"></span></span>
                                          <input class="form-control" name="cmac" placeholder="Cmac" type="text">
                                       </div>
                           </div>
                        </div>
                     </div>
                     <!-- row6 Ending -->


                     <!-- row7 -->
                     <div class="row">
                        <div class="col-md-6">
                           <div class="form-group">
                              <label>Cmcc</label>
                                  <div class="input-group">
                                          <span class="input-group-addon"><span class="icon icon-multitouch"></span></span>
                                          <input class="form-control" name="cmcc" placeholder="Cmcc" type="text">
                                       </div>
                           </div>
                        </div>
                        <div class="col-md-6">
                           <div class="form-group">
                              <label>Dtcomp</label>
                                 <div class="input-group">
                                          <span class="input-group-addon"><span class="icon icon-dots"></span></span>
                                          <input class="form-control" name="dtcmp" placeholder="Dtcomp" type="text">
                                       </div>
                           </div>
                        </div>
                     </div>
                     <!-- row7 Ending -->

                     <!-- row8 -->
                     <div class="row">
                        <div class="col-md-6">
                           <div class="form-group">
                              <label>Labcost</label>
                                  <div class="input-group">
                                          <span class="input-group-addon"><span class="icon"><b>&#x20B9;</b></span></span>
                                          <input class="form-control" name="lbcst" placeholder="Labcost" type="text">
                                       </div>
                           </div>
                        </div>
                        <div class="col-md-6">
                           <div class="form-group">
                              <label>Oh</label>
                                 <div class="input-group">
                                          <span class="input-group-addon"><span class="icon icon-grid"></span></span>
                                          <input class="form-control" name="oh" placeholder="Oh" type="text">
                                       </div>
                           </div>
                        </div>
                     </div>
                     <!-- row8 Ending -->

                     <!-- row9 -->
                     <div class="row">
                        <div class="col-md-6">
                           <div class="form-group">
                              <label>Cent</label>
                                  <div class="input-group">
                                          <span class="input-group-addon"><span class="icon icon-link"></span></span>
                                          <input class="form-control" name="cnt" placeholder="Cent" type="text">
                                       </div>
                           </div>
                        </div>
                        <div class="col-md-6">
                           <div class="form-group">
                              <label>Totalcost</label>
                                 <div class="input-group">
                                          <span class="input-group-addon"><span class="icon"><b>&#x20B9;</b></span></span>
                                          <input class="form-control" name="totcst" placeholder="Totalcost" type="text">
                                       </div>
                           </div>
                        </div>
                     </div>
                     <!-- row9 Ending -->


                  </div>
                  <!-- container Ending -->
               </div>
            <div class="panel-footer">
            <div class="text-right">
            <button type="submit" class="btn btn-danger" style="margin-right: 2%;">Submit <i class="icon-arrow-right14 position-right"></i></button>
            </div>	
            </div>
            </form>
         </div>

      <!-- div col-lg-10 Ending -->
      </div>
</div>
</div>
      <?php include 'footer.php'; ?>
      <?php include('bottom.php'); ?>
   </body>


</html>
