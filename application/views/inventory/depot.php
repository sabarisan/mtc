<!DOCTYPE html>
<html>
   <head>
      <title>MTC | Depot Management</title>
      <?php include('top.php'); ?>
  
   </head>
   <body>
      <?php include('header.php'); ?>
      
      <?php include('brdcrmb.php'); ?>

      <?php 
         include('side.php');
       ?>

      
      <!-- col-sm-10 starting -->
      <!-- <div class="col-sm-9"> -->
       <!--  <div  id="deptpnl">
          
        </div>
       -->

       

      <div id="whlechnge">
        
        <div class="panel" id="deptfrm" style="display: none;">
            <div class="panel panel-flat">
               <div class="bg-mybrwn-400-hed">
                  <b>Update Depot</b>
               </div>
            </div>
            <div class="panel-body">
              <form id="depot_mgmt" action="#">
                 <div class="row">
                        <div class="col-md-12">
                           <div class="form-group" style="display: none;" id="wottxtid">
                              <label>Depotid</label>
                                 <div class="input-group" style="display: none;">
                                          <span class="input-group-addon"><span class="icon icon-more"></span></span>
                                          <p><span id="did"></span></p>
                                 </div>
                                 
                           </div>

                           <div class="form-group" style="display: none;" id="wttxtid">
                              <label>Depotid</label>
                                 <div class="input-group">
                                          <span class="input-group-addon"><span class="icon icon-more"></span></span>
                                          <input type="text" class="form-control" value="" id="did" name="depid" placeholder="deptid">
                                 </div>
                           </div>

                        </div>
                     </div>

                     <input type="hidden" name="rwid" value="" id="deprwid">
                     <div class="row">
                        <div class="col-md-12">
                           <div class="form-group">
                              <label>Depotname</label>
                                 <div class="input-group">
                                          <span class="input-group-addon"><span class="icon icon-pen"></span></span>
                                          <input class="form-control" value="" id="dname" name="depnme" placeholder="Depotname" type="text">
                                 </div>
                           </div>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-md-12">
                           <div class="form-group">
                              <label>Depotopt</label>
                                 <div class="input-group">
                                          <span class="input-group-addon"><span class="icon icon-spotlight2"></span></span>
                                          <input class="form-control" value="" id="dopt" name="depopt" placeholder="Depotopt" type="text">
                                 </div>
                           </div>
                        </div>
                     </div>
                     </div>
                     <div class="panel-footer">
                  <div class="text-right">
                  <button type="submit" id="" class="btn btn-danger" style="margin-right: 2%;">Submit <i class="icon-arrow-right14 position-right"></i></button>
                  </div>  
              
                </div>
        
      </form>
         </div>
          
        
        <!-- Basic initialization -->
        <div class="panel panel-flat">
          <div class="panel-heading">
            <h5 class="panel-title">Depot Details</h5>
            <div class="heading-elements">
              &nbsp;&nbsp;&nbsp;<div class="btn btn-info" onclick="deptins()"><i class="icon icon-add" style="color: #fff; "></i> Add</div>
              <ul class="icons-list">
                        <li><a data-action="collapse"></a></li>
                        <!-- <li><a data-action="reload"></a></li> -->
                        <!-- <li><a data-action="close"></a></li> -->
                      </ul>
                    </div>
          </div>

          <div class="container-fluid">
              <table class="table datatable-button-init-basic" id="posts">
            <thead>
            <th>Sno</th>
            <th>Depot ID</th>
            <th>Depot Name</th>
            <th>Depot Opt</th>
            <th><center>Created At</center></th>
            <th><center>Operation</center></th>
         </thead>
          </table>  
          </div>
        </div>
        <!-- /basic initialization -->
      </div>
      <!-- whole change Ending -->
      <!-- div col-sm-10 Ending -->
      </div>
</div>
</div>


      <?php include 'footer.php'; ?>

      <?php include('bottom.php'); ?> 


   </body>
</html>
