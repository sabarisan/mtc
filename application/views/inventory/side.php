<div class="page-container ">
<div class="page-content">
<?php 
   if ($this->session->userdata('mymenu')) {
      $mydat  = array();
      $mydat =  $this->session->userdata('mymenu');
       // echo '<pre>'; print_r($mydat);   echo '</pre>';
   
      ?>
<!-- Main sidebar -->
<div class="sidebar sidebar-main sidebar-default">
   <div class="sidebar-content">
      <!-- Main navigation -->
      <div class="sidebar-category sidebar-category-visible">
         <div class="category-content no-padding">
            <div class="category-title bg-indigo">
               <span><b>Navigation</b></span>
               <!-- <ul class="icons-list">
                  <li><a href="#" data-action="collapse"></a></li>
                  </ul> -->
            </div>
            <ul class="navigation navigation-main navigation-accordion">
               <?php foreach ($mydat['menudata'] as $men) { ?>
               <?php 
                  if ($men['menulink'] != 'javascript:void(0)' && $men['menuaccess']=='0' || $men['menulink'] != 'javascript:void(0)' && $men['menuaccess']=='2') {
                  ?>
               <li><a href="<?php echo base_url().'admin/'.$men['menulink'] ?>"><i class="<?php echo $men['menuicon']; ?>"></i><?php echo $men['menuname'] ?> <span></span></a></li>
               <?php }else { ?>
               <li class="">
                  <a href="#"><i class="<?php echo $men['menuicon']; ?>"></i> <span><?php echo $men['menuname']; ?></span></a>
                  <ul>
                     <?php 
                        foreach ($men['submenudata'] as $submnu) {?>
                     <?php  if ($men['menuid']== $submnu['menuid'] && ($submnu['submenuaccess']!='' && $submnu['submenuaccess']=="0" || $submnu['submenuaccess']!='' && $submnu['submenuaccess']=="2")) { ?>
                     <li><a href="<?php echo base_url().'admin/'.$submnu['submenulink']; ?>"><!-- <i class="<?php //echo $submnu['submenuicon']; ?>"></i> --><?php echo $submnu['submenuname']; ?></a></li>
                     <?php }} ?>
                  </ul>
               </li>
               <?php }} ?>
            </ul>
         </div>
      </div>
      <!-- /main navigation -->
      <?php }?>
   </div>
</div>
<!-- /main sidebar -->
