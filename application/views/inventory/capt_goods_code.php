
<?php 
	include('top.php');
	
 ?>

 	<?php 
 		include('header.php');
 	 ?>

 	<?php 
      include('brdcrmb.php');
    ?>

 	 <?php 
 	 	include('side.php');
 	  ?>


 	  <!-- <div class="col-sm-9"> -->
 	  	 <!-- Sales stats -->
            <div class="panel panel-flat" style="display: none;" id="captfrmupdte">
               <!-- <br> -->
               <!-- /sales stats -->
               <div class="bg-mybrwn-400-hed">
                  <b>CAPITAL GOODS CODE</b>
               </div>
               <!-- panelbody -->
               <div class="panel-body">
                  <form action="#" id="cap_goods_cod" method="post">
                     <!-- row1 -->
                     <div class="row">
                        <div class="col-md-12">
                           <div class="form-group">
                              <label>Description</label>
                              <div class="input-group">
                                    <textarea cols="150" rows="3" class="form-control" id="desctxt" placeholder="Enter Description" name="descrip"></textarea>
                                    <input type="hidden" value="" id="rwid" name="rwid">
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-- row1 Ending -->

                     <!-- row2 -->
                     <div class="row">
                        <div class="col-md-12">
                           <div class="form-group">
                              <label>Folio Group</label>
                                 <div class="input-group">
                                          <span class="input-group-addon"><span class="icon icon-diff-renamed"></span></span>
                                          <input class="form-control" name="folgrp" id="folgrptxt" placeholder="Folio Group" type="text">
                                 </div>
                           </div>
                        </div>
                     </div>
                     <!-- row2 Ending -->
               </div>
               <!-- panelbody Ending -->
               <div class="panel-footer">
               		<div class="text-right">
                        <button type="submit" class="btn btn-danger" style="margin-right: 2%;">Submit <i class="icon-arrow-right14 position-right"></i></button>
                     </div>
               </div>
            </form>
            <!-- form Ending -->


         </div>

          <!-- Basic initialization -->
        <div class="panel panel-flat">
          <div class="panel-heading">
            <h5 class="panel-title">Capital Goods Management Details</h5>
            <div class="heading-elements">
              &nbsp;&nbsp;&nbsp;<div class="btn btn-info" onclick="captins()"><i class="icon icon-add" style="color: #fff; "></i> Add</div>
              <ul class="icons-list">
                        <li><a data-action="collapse"></a></li>
                        <!-- <li><a data-action="reload"></a></li> -->
                        <!-- <li><a data-action="close"></a></li> -->
                      </ul>
                    </div>
          </div>

          <div class="container-fluid">
              <table class="table datatable-button-init-basic" id="captfrm">
            <thead>
            <th>Sno</th>
            <th>Code</th>
            <th>Description</th>
            <th>Folio Group</th>
            <th><center>Created At</center></th>
            <th><center>Operation</center></th>
         </thead>
          </table>  
          </div>
        </div>
        <!-- /basic initialization -->
 	  </div>
 
</div>



 	 <?php 
 	 	include('footer.php');
 	  ?>

 </body>

  <?php 
 	 	include('bottom.php');
 	  ?>

    