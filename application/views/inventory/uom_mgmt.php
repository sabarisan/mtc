<!DOCTYPE html>
<html>
   <head>
      <title>MTC | UOM MANAGEMENT</title>
      <?php 
         include('top.php');
         ?>
   </head>
   <body>
      <?php 
         include('header.php');
         ?>
      <?php include('brdcrmb.php'); ?>
      <?php include('side.php'); ?>
      <!-- <div class="col-sm-9"> -->
      <div class="panel" style="position: static;">
         <!-- Sales stats -->
         <div class="panel panel-flat">
            <!-- /sales stats -->
            <div class="bg-mybrwn-400-hed">
               <b>UOM MANAGEMENT</b>
            </div>
         </div>
         <div class="panel-body">
            <form action="#" method="post" id="tyre">
               <!-- container-fluid -->
               <div class="container-fluid">
                  <!-- row1 -->
                  <div class="row">
                     <div class="col-md-12">
                        <div class="form-group">
                           <label>Uom</label>
                           <div class="input-group">
                              <span class="input-group-addon"><span class="icon icon-weather-windy"></span></span>
                              <input class="form-control" name="cde" placeholder="Code" type="text">
                           </div>
                        </div>
                     </div>
                  </div>
                  <!-- row1 Ending -->
               </div>
               <!-- container-Fluid Ending -->
         </div>
         <div class="panel-footer">
         <div class="text-right">
         <button type="submit" class="btn btn-danger" style="margin-right: 2%;">Submit <i class="icon-arrow-right14 position-right"></i></button>
         </div>   
         </div>
         </form>
      </div>
      <!-- div col-sm-10 Ending -->
      </div>
      </div>
      </div>
      <!-- Division for content wrapper Ending -->
      <?php 
         include('footer.php');
         
         ?>
   </body>
   <?php  
      include('bottom.php');
      ?>
</html>
