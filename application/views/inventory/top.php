
		<!-- Global stylesheets -->
		<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
		<link href="<?php echo base_url().'assets/css/icons/icomoon/styles.css';?>" rel="stylesheet" type="text/css">
		<link href="<?php echo base_url().'assets/css/bootstrap.css';?>" rel="stylesheet" type="text/css">
		<link href="<?php echo base_url().'assets/css/core.css';?>" rel="stylesheet" type="text/css">
		<link href="<?php echo base_url().'assets/css/components.css';?>" rel="stylesheet" type="text/css">
		<link href="<?php echo base_url().'assets/css/colors.css';?>" rel="stylesheet" type="text/css">
		<!-- /global stylesheets -->
		<link href="<?php echo base_url().'assets/css/unicodefonts.css';?>" rel="stylesheet" type="text/css">
		<!-- sweet alerts -->
		<link href="<?php echo base_url().'assets/css/sweetalert.css';?>" rel="stylesheet" type="text/css">
		<!-- sweet alerts -->

		<style type="text/css">
		.bg-indigo
		{
			background:#F09800;
		}

		.navbar-default .navbar-nav > li > a
		{
			color: black;
		}

		.dropdown-menu
		{
			position: absolute;
			top: 100%;
			left: 0;
			z-index: 1000;
			display: none;
			float: left;
			min-width: 160px;
			padding: 5px 0;
			margin: 2px 0 0;
			margin-top: 2px;
			list-style: none;
			font-size: 13px;
			text-align: left;
			background-color: #006eea;
			border: 1px solid #ddd;
			border-top-width: 1px;
			border-radius: 3px;
			border-top-left-radius: 3px;
			border-top-right-radius: 3px;
			-webkit-box-shadow: 0 6px 12px rgba(0, 0, 0, 0.175);
			box-shadow: 0 6px 12px rgba(0, 0, 0, 0.175);
			background-clip: padding-box;
		}

		.dropdown-header
		{
			padding: 8px 15px;
			font-size: 11px;
			line-height: 1.82;
			color: #fff;
			text-transform: uppercase;
			margin-top: 7px;
		}

		.dropdown-menu > li > a
		{
			display: block;
			padding: 3px 20px;
			clear: both;
			font-weight: normal;
			line-height: 1.5384616;
			color: #fcf7f7;
			white-space: nowrap;
		}

		.navbar-inverse
		{
			background-color: #006eea;
			border-color: #006eea;
			border-bottom-color:;
		}

		.navbar-inverse .navbar-brand
		{
			color: #fefefe;
		}

		.bg-mybrwn-400
		{
			background-color: #006eea;
			border-color: #006eea;
			color: #fff;
		}

		.icon
		{
			color: #006eea;
		}

		.text-slate
		{
			color: #fff !important;
		}

		.text-muted
		{
			color: #fff;
		}

		.mystrip
		{
			background-color: #e0e0e0;
			color: black;
			padding: 1%;
		}

		.delvtrm
		{
			background-color:#ff6300;
			padding: 2%;
			color: #fff;
		}

		.bg-mybrwn-400-hed
		{
			background-color: #006eea;
			border-color: #006eea;
			color: #fff;
			padding: 1.3%;
		}

		label.error
		{
			float: none; color:#dd4b39;
		}
			
		.space
		{
			width: 100px;
		} 

		.glyphicon:hover
		{
			cursor: pointer;
		}
		</style>
	