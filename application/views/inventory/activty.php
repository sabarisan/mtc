<!DOCTYPE html>
<html>
<head>
	<title>MTC | Activity Management</title>
<?php 
	include('top.php');
	
 ?>
</head>
 <body>
 	<?php 
 		include('header.php');
 	 ?>

<!-- <br><br> -->
 	<?php 
      include('brdcrmb.php');
    ?>

 	 <?php 
 	 	include('side.php');
 	  ?>


         <!-- Sales stats -->
            <div class="panel panel-flat" id="actvtfrmins" style="display: none;">
               <!-- <br> -->
               <!-- /sales stats -->
               <div class="bg-mybrwn-400-hed">
                  <b>ACTIVITY MANAGEMENT</b>
               </div>
               <!-- panelbody -->
               <div class="panel-body">
                  <form id="actvitymgmtins" method="post" action="<?php echo base_url().'inventory/Activity/actvtins' ?>">

                     <!-- row1 -->
                     <div class="row">
                        <div class="col-md-12">
                           <div class="form-group">
                              <label>Activity Code</label>
                              <div class="input-group">
                                 <span class="input-group-addon"><span class="icon icon-podium"></span></span>
                                 <input type="text" name="actvcd" class="form-control" id="" value="">
                              </div>
                                
                            </div>
                        </div>
                     </div>
                     <!-- row1 Ending -->

                     <!-- division for row2 -->
                     <div class="row">
                        <div class="col-md-12">
                           <div class="form-group">
                              <label>Activity Name</label>
                                     <div class="input-group">
                                                <span class="input-group-addon"><span class="icon icon-pencil"></span></span>
                                                <input class="form-control" id="" name="actvnme" placeholder="Activity Name" type="text">
                                   </div>
                                  </div>
                        </div>
                     </div>
                     <!-- row2 Ending -->

                  </div>
               <!-- panelbody Ending -->
               <div class="panel-footer">
                  <div class="text-right">
                        <button type="submit" id="" class="btn btn-danger" style="margin-right: 2%;">Submit <i class="icon-arrow-right14 position-right"></i></button>
                     </div>
               </div>
            </form>
         </div>
 	  
 	  	 <!-- Sales stats -->
            <div class="panel panel-flat" style="display: none;" id="actvtfrmupdte">
               <!-- <br> -->
               <!-- /sales stats -->
               <div class="bg-mybrwn-400-hed">
                  <b>ACTIVITY MANAGEMENT</b>
               </div>
               <!-- panelbody -->
               <div class="panel-body">
                  <form id="actvitymgmt">

                     <!-- row1 -->
                     <div class="row">
                        <div class="col-md-12">
                           <div class="form-group">
                              <label>Activity Code</label>
                              <div class="input-group">
                                 <span class="input-group-addon"><span class="icon icon-podium"></span></span>
                                 <p id="actid"></p>
                              </div>
                                <input type="hidden" name="rwid" id="rwid" value="">
                            </div>
                        </div>
                     </div>
                     <!-- row1 Ending -->

                     <!-- division for row2 -->
                     <div class="row">
                        <div class="col-md-12">
                           <div class="form-group">
                              <label>Activity Name</label>
                                     <div class="input-group">
                                                <span class="input-group-addon"><span class="icon icon-pencil"></span></span>
                                                <input class="form-control" id="actvnme" name="actvnme" placeholder="Activity Name" type="text">
                                   </div>
                                  </div>
                        </div>
                     </div>
                     <!-- row2 Ending -->

                  </div>
               <!-- panelbody Ending -->
               <div class="panel-footer">
               		<div class="text-right">
                        <button type="button" id="actvtsbmt" class="btn btn-danger" style="margin-right: 2%;">Submit <i class="icon-arrow-right14 position-right"></i></button>
                     </div>
               </div>
            </form>
         </div>

          <!-- Basic initialization -->
        <div class="panel panel-flat">
          <div class="panel-heading">
            <h5 class="panel-title">Activity Management Details</h5>
            <div class="heading-elements">
              &nbsp;&nbsp;&nbsp;<div class="btn btn-info" onclick="actvtins()"><i class="icon icon-add" style="color: #fff; "></i> Add</div>
              <ul class="icons-list">
                        <li><a data-action="collapse"></a></li>
                        <!-- <li><a data-action="reload"></a></li> -->
                        <!-- <li><a data-action="close"></a></li> -->
                      </ul>
                    </div>
          </div>

          <div class="container-fluid">
              <table class="table datatable-button-init-basic" id="actvtfrm">
            <thead>
            <th>Sno</th>
            <th>Activity Code</th>
            <th>Activity Name</th>
            <th><center>Created At</center></th>
            <th><center>Operation</center></th>
         </thead>
          </table>  
          </div>
        </div>
        <!-- /basic initialization -->

 	  </div>
</div>

   
 	 <?php 
 	 	include('footer.php');
 	  ?>

 

  <?php 
 	 	include('bottom.php');
 	  ?>

   
</body>
</html>