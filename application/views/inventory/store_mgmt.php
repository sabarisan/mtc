 <!DOCTYPE html>
<html>
<head>
	<title>MTC | STORE MANAGEMENT</title>
	<?php include('top.php'); ?>
</head>
<body>
	<?php include('header.php'); ?>

	 <?php include('brdcrmb.php'); ?>

	   <?php include('side.php'); ?>

	   <!-- <div class="col-sm-9"> -->
         <div class="panel" style="position: static;display: none;" id="strefrmupdte">
            <!-- Sales stats -->
            <div class="panel panel-flat">
               <!-- /sales stats -->
               <div class="bg-mybrwn-400-hed">
                  <b>STORE MANAGEMENT</b>
               </div>
            </div>
            <div class="panel-body">
               <form action="#" method="post" id="stre_mgmt">
               <!-- container-fluid -->
               <div class="container-fluid">
                  <!-- 1st row -->
                  <div class="row">
                    <div class="col-md-6">
                       <div class="form-group">
                          <label>Type</label>
                             <div class="input-group">
                                     <span class="input-group-addon"><span class="icon icon-weather-windy"></span></span>
                                     <input class="form-control" value="" id="tb1" name="typ" placeholder="Type" type="text">
                                  </div>
                        </div>
                    </div>
                    <input type="hidden" value="" id="rwid" name="rwid">
                    <div class="col-md-6">
                       <div class="form-group">
                          <label>Name</label>
                                      <div class="input-group">
                                       <span class="input-group-addon"><span class="icon icon-user"></span></span>
                                       <input class="form-control" value="" id="tb2" name="nme" placeholder="Name" type="text">
                                    </div>
                        </div>
                    </div>
                  </div>
                  <!-- 1st row Ending -->

                  <!-- 2nd row -->
                  <div class="row">
                    <div class="col-md-6">
                       <div class="form-group">
                          <label>Name1</label>
                             <div class="input-group">
                                     <span class="input-group-addon"><span class="icon icon-user"></span></span>
                                     <input class="form-control" value="" id="tb3" name="nme1" placeholder="Name1" type="text">
                                  </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                       <div class="form-group">
                          <label>Address1</label>
                                      <div class="input-group">
                                       <span class="input-group-addon"><span class="icon icon-file-spreadsheet"></span></span>
                                       <input class="form-control" value="" id="tb4" name="addr1" placeholder="Address1" type="text">
                                    </div>
                        </div>
                    </div>
                  </div>
                  <!-- 2nd row Ending -->

                  <!-- 3rd row -->
                  <div class="row">
                    <div class="col-md-6">
                       <div class="form-group">
                          <label>Address2</label>
                             <div class="input-group">
                                     <span class="input-group-addon"><span class="icon icon-file-spreadsheet"></span></span>
                                     <input class="form-control" value="" id="tb5" name="addr2" placeholder="Address2" type="text">
                                  </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                       <div class="form-group">
                          <label>Address3</label>
                                      <div class="input-group">
                                       <span class="input-group-addon"><span class="icon icon-file-spreadsheet"></span></span>
                                       <input class="form-control" value="" id="tb6" name="addr3" placeholder="Address3" type="text">
                                    </div>
                        </div>
                    </div>
                  </div>
                  <!-- 3rd row Ending -->

                  <!-- 4th row -->
                  <div class="row">
                    <div class="col-md-6">
                       <div class="form-group">
                          <label>Address4</label>
                             <div class="input-group">
                                       <span class="input-group-addon"><span class="icon icon-file-spreadsheet"></span></span>
                                       <input class="form-control" value="" id="tb7" name="addr4" placeholder="Address4" type="text">
                                    </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                       <div class="form-group">
                          <label>Pincode</label>
                                      <div class="input-group">
                                       <span class="input-group-addon"><span class="icon icon-pin"></span></span>
                                       <input class="form-control" value="" id="tb8" name="pincd" placeholder="Pincode" type="text">
                                    </div>
                        </div>
                    </div>
                  </div>
                  <!-- 4th row Ending -->

                  <!-- 5th row -->
                  <div class="row">
                    <div class="col-md-6">
                       <div class="form-group">
                          <label>State</label>
                             <div class="input-group">
                                       <span class="input-group-addon"><span class="icon icon-pin-alt"></span></span>
                                       <input class="form-control" value="" id="tb9" name="state" placeholder="State" type="text">
                                    </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                       <div class="form-group">
                          <label>Stdcode</label>
                                     <div class="input-group">
                                       <span class="input-group-addon"><span class="icon icon-phone"></span></span>
                                       <input class="form-control" value="" id="tb10" name="stdcde" placeholder="Stdcode" type="text">
                                    </div>
                        </div>
                    </div>
                  </div>
                  <!-- 5th row Ending -->

                   <!-- 6th row -->
                  <div class="row">
                    <div class="col-md-6">
                       <div class="form-group">
                          <label>Phone1</label>
                             <div class="input-group">
                                       <span class="input-group-addon"><span class="icon icon-mobile"></span></span>
                                       <input class="form-control" value="" id="tb11" name="phne1" placeholder="Phone1" type="text">
                                    </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                       <div class="form-group">
                          <label>Phone2</label>
                                     <div class="input-group">
                                       <span class="input-group-addon"><span class="icon icon-mobile"></span></span>
                                       <input class="form-control" value="" id="tb12" name="phne2" placeholder="Phone2" type="text">
                                    </div>
                        </div>
                    </div>
                  </div>
                  <!-- 6th row Ending -->


                   <!-- 7th row -->
                  <div class="row">
                    <div class="col-md-6">
                       <div class="form-group">
                          <label>Phone3</label>
                             <div class="input-group">
                                       <span class="input-group-addon"><span class="icon icon-mobile"></span></span>
                                       <input class="form-control" value="" id="tb13" name="phne3" placeholder="Phone3" type="text">
                                    </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                       <div class="form-group">
                          <label>Fax</label>
                             <div class="input-group">
                                       <span class="input-group-addon"><span class="icon icon-printer"></span></span>
                                       <input class="form-control" value="" id="tb14" name="fax" placeholder="Fax" type="text">
                                    </div>
                        </div>
                    </div>
                  </div>
                  <!-- 7th row Ending -->


                  <!-- 8th row -->
                  <div class="row">
                    <div class="col-md-6">
                       <div class="form-group">
                          <label>Telex</label>
                             <div class="input-group">
                                       <span class="input-group-addon"><span class="icon icon-phone2"></span></span>
                                       <input class="form-control" value="" id="tb15" name="telx" placeholder="Telex" type="text">
                                    </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                       <div class="form-group">
                          <label>Vfrom</label>
                             <div class="input-group">
                                       <span class="input-group-addon"><span class="icon icon-drawer-in"></span></span>
                                       <input class="form-control" value="" id="tb16" name="vfrm" placeholder="Vfrom" type="text">
                                    </div>
                        </div>
                    </div>
                  </div>
                  <!-- 8th row Ending -->

                  <!-- 9th row -->
                  <div class="row">
                    <div class="col-md-6">
                       <div class="form-group">
                          <label>Vto</label>
                             <div class="input-group">
                                       <span class="input-group-addon"><span class="icon icon-drawer-out"></span></span>
                                       <input class="form-control" value="" id="tb17" name="vto" placeholder="Vto" type="text">
                                    </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                       <div class="form-group">
                          <label>Asrturef</label>
                             <div class="input-group">
                                       <span class="input-group-addon"><span class="icon icon-pushpin"></span></span>
                                       <input class="form-control" value="" id="tb18" name="asrtrf" placeholder="Asrturef" type="text">
                                    </div>
                        </div>
                    </div>
                  </div>
                  <!-- 9th row Ending -->

                  <!-- 10th row -->
                  <div class="row">
                    <div class="col-md-6">
                       <div class="form-group">
                          <label>Asrtudt</label>
                             <div class="input-group">
                                       <span class="input-group-addon"><span class="icon icon-pencil"></span></span>
                                       <input class="form-control" value="" id="tb19" name="asrtudt" placeholder="Asrtudt" type="text">
                                    </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                       <div class="form-group">
                          <label>Venrate</label>
                             <div class="input-group">
                                       <span class="input-group-addon"><span class="icon icon-pencil"></span></span>
                                       <input class="form-control" value="" id="tb20" name="venrte" placeholder="Venrate" type="text">
                                    </div>
                        </div>
                    </div>
                  </div>
                  <!-- 10th row Ending -->

                  <!-- 11th row -->
                  <div class="row">
                    <div class="col-md-6">
                       <div class="form-group">
                          <label>Acode</label>
                             <div class="input-group">
                                       <span class="input-group-addon"><span class="icon icon-pushpin"></span></span>
                                       <input class="form-control" value="" id="tb21" name="acde" placeholder="Acode" type="text">
                                    </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                       <div class="form-group">
                          <label>Tinno</label>
                             <div class="input-group">
                                       <span class="input-group-addon"><span class="icon icon-pushpin"></span></span>
                                       <input class="form-control" value="" id="tb22" name="tino" placeholder="Tinno" type="text">
                                    </div>
                        </div>
                    </div>
                  </div>
                  <!-- 11th row Ending -->

                  <!-- 12th row -->
                  <div class="row">
                    <div class="col-md-6">
                       <div class="form-group">
                          <label>Pan No</label>
                             <div class="input-group">
                                       <span class="input-group-addon"><span class="icon icon-credit-card2"></span></span>
                                       <input class="form-control" value="" id="tb23" name="pan" placeholder="Pan Number" type="text">
                                    </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                       <div class="form-group">
                          <label>Gstin</label>
                             <div class="input-group">
                                       <span class="input-group-addon"><span class="icon icon-pencil"></span></span>
                                       <input class="form-control" value="" id="tb24" name="gstin" placeholder="Gstin" type="text">
                                    </div>
                        </div>
                    </div>
                  </div>
                  <!-- 12th row Ending -->

              </div>
              <!-- container-fluid Ending -->
            </div>
            <div class="panel-footer">
            <div class="text-right">
            <button type="button" id="streupd" class="btn btn-danger" style="margin-right: 2%;">Submit <i class="icon-arrow-right14 position-right"></i></button>
            </div>   
            </div>
            </form>
         </div>
         <!-- div col-sm-10 Ending -->

         <!-- Basic datatable -->
            <div class="panel panel-flat">
               <div class="panel-heading">
                  <h5 class="panel-title">Store Management</h5>
                  <div class="heading-elements">
                     <button type="button" name="" id="grpmgmtentr" onclick="streins()" class="btn btn-info"><i class="icon icon-add" style="color: #fff;"> </i> ADD</button>
                     <ul class="icons-list">
                           <li><a data-action="collapse"></a></li>
                     <!--       <li><a data-action="reload"></a></li>
                           <li><a data-action="close"></a></li> -->
                        </ul>
                     </div>
               </div>

               <table id="DataTables_Table_0" class="table datatable-button-flash-name dataTable no-footer" role="grid" aria-describedby="DataTables_Table_0_info">
                  <thead>
                     <tr>
                        <th>Sno</th>
                        <th>Type</th>
                        <th>Name</th>
                        <th>Addr1</th>
                        <!-- <th>Addr2</th> -->
                        <!-- <th>Addr3</th> -->
                        <!-- <th>Addr4</th> -->
                        <th>Createddate</th>
                        <th>Operation</th>
                     </tr>
                  </thead>
                  <tbody>
                     <?php if (isset($stremgmt)) { ?>

                        <?php $i=1; foreach ($stremgmt as $stre) { ?>
                           <tr>
                              <td><?php echo $i; ?></td>
                              <td><?php echo $stre->type; ?></td>
                              <td><?php echo $stre->name; ?></td>
                              <td><?php echo $stre->add1; ?></td>
                              <!-- <td><?php echo $stre->add2; ?></td> -->
                              <!-- <td><?php echo $stre->add3; ?></td> -->
                              <?php /* ?><td><?php echo $stre->add4; ?></td> <?php */?>
                              <td><?php echo $stre->createddate; ?></td>
                              <!-- <td><?php echo $stre->updateddate; ?></td> -->
                              <td><div><center><i class="btn btn-danger glyphicon glyphicon-trash" style="font-size:12px;text-transform:lowercase;" onclick="strermv('<?php echo $stre->id;?>')"></i> <a href="#"><i class="btn btn-success glyphicon" style="font-size:12px;text-transform:lowecase;" onclick="streupdte('<?php echo $stre->id;?>','<?php echo $stre->type;?>','<?php echo $stre->name;?>','<?php echo $stre->name1;?>','<?php echo $stre->add1;?>','<?php echo $stre->add2;?>','<?php echo $stre->add3;?>','<?php echo $stre->add4;?>','<?php echo $stre->pincode;?>','<?php echo $stre->state;?>','<?php echo $stre->stdcode;?>','<?php echo $stre->phone1;?>','<?php echo $stre->phone2;?>','<?php echo $stre->phone3;?>','<?php echo $stre->fax;?>','<?php echo $stre->telex;?>','<?php echo $stre->vfrom;?>','<?php echo $stre->vto;?>','<?php echo $stre->asrturef;?>','<?php echo $stre->asrtudt;?>','<?php echo $stre->venrate;?>','<?php echo $stre->acode;?>','<?php echo $stre->tinno;?>','<?php echo $stre->panno;?>','<?php echo $stre->gstin;?>');"><i class="glyphicon glyphicon-pencil" style="font-size:12px;"></i></i></a></center></div></td>
                           </tr>

                        <?php $i++; } ?>
                   <?php } ?>
                  </tbody>
               </table>
            </div>
            <!-- /basic datatable -->

      </div>
      </div>
      <!-- Division for content wrapper Ending -->
</div>

	 <?php 
	 	include('footer.php'); 
	 ?>
</body>
	<?php 
		include('bottom.php');
	 ?>
</html>