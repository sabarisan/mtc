 <!DOCTYPE html>
<html>  
<?php include('top.php'); ?>   
      <?php include('header.php'); ?>
      <?php include('brdcrmb.php'); ?>
	   <?php include('side.php'); ?>

 	  <!-- <div class="col-sm-9"> -->
 	  	 <!-- Sales stats -->
            <div class="panel panel-flat" style="display: none;" id="grpmgmtfrm">
               <!-- <br> -->
               <!-- /sales stats -->
               <div class="bg-mybrwn-400-hed">
                  <b>GROUP MANAGEMENT</b>
               </div>
               <!-- panelbody -->
               <div class="panel-body">
               <!-- form start -->
                 <form action="#" method="post" id="grp_mgmt">
                  <div class="container-fluid">
                     <!-- row1 -->
                     <div class="row">
                        <div class="col-md-12">
                           <div class="form-group">
                              <label>Grp</label>
                                 <div class="input-group">
                                          <span class="input-group-addon"><span class="icon icon-bus"></span></span>
                                          <input class="form-control" name="grp" value="" id="grptxt" placeholder="Grp" type="text">
                                 </div>
                           </div>
                        </div>
                     </div>
                     <!-- row1 Ending -->
                     <input type="hidden" id="idtxt" name="rwid">
                     <!-- row2 -->
                     <div class="row">
                        <div class="col-md-12">
                           <div class="form-group">
                              <label>Grp Name</label>
                                 <div class="input-group">
                                          <span class="input-group-addon"><span class="icon icon-pencil"></span></span>
                                          <input class="form-control" name="grpnme" value="" id="grpnmetxt" placeholder="Folio Group" type="text">
                                 </div>
                           </div>
                        </div>
                     </div>
                     <!-- row2 Ending -->
                  </div>
                  <!-- container-fluid Ending -->
               </div>
               <!-- panelbody Ending -->
               <div class="panel-footer">
               		<div class="text-right">
                        <button type="button" id="grpdtasbmt" class="btn btn-danger" style="margin-right: 2%;">Submit <i class="icon-arrow-right14 position-right"></i></button>
                     </div>
                  </form>
                  <!-- form Ending -->
               </div>
            </div>

            <!-- Basic datatable -->
            <div class="panel panel-flat">
               <div class="panel-heading">
                  <h5 class="panel-title">Group Management</h5>
                  <div class="heading-elements">
                     <button type="button" name="" id="grpmgmtentr" onclick="grpins()" class="btn btn-info"><i class="icon icon-add" style="color: #fff;"> </i> ADD</button>
                     <ul class="icons-list">
                           <li><a data-action="collapse"></a></li>
                     <!--       <li><a data-action="reload"></a></li>
                           <li><a data-action="close"></a></li> -->
                        </ul>
                     </div>
               </div>

               <table id="DataTables_Table_0" class="table datatable-button-flash-name dataTable no-footer" role="grid" aria-describedby="DataTables_Table_0_info">
                  <thead>
                     <tr>
                        <th>Sno</th>
                        <th>Grp</th>
                        <th>Grpname</th>
                        <th>Createddate</th>
                        <th>Updateddate</th>
                        <th>Operation</th>
                     </tr>
                  </thead>
                  <tbody>
                     <?php if (isset($grpmgmt)) { ?>

                        <?php $i=1; foreach ($grpmgmt as $grp) { ?>
                           <tr>
                              <td><?php echo $i; ?></td>
                              <td><?php echo $grp->grp; ?></td>
                              <td><?php echo $grp->grpname; ?></td>
                              <td><?php echo $grp->createddate; ?></td>
                              <td><?php echo $grp->updateddate; ?></td>
                              <!-- <td><?php echo $grp->updateddate; ?></td> -->
                              <td><div><center><i class="btn btn-danger glyphicon glyphicon-trash" style="font-size:12px;text-transform:lowercase;" onclick="grprmv('<?php echo $grp->id;?>')"></i> <a href="#"><i class="btn btn-success glyphicon" style="font-size:12px;text-transform:lowecase;" onclick="grpupdte('<?php echo $grp->id;?>','<?php echo $grp->grp;?>','<?php echo $grp->grpname;?>');"><i class="glyphicon glyphicon-pencil" style="font-size:12px;"></i></i></a></center></div></td>
                           </tr>

                        <?php $i++; } ?>
                   <?php } ?>
                  </tbody>
               </table>
            </div>
            <!-- /basic datatable -->

            

 	  </div>
</div>



<?php include('footer.php'); ?>

<?php include('bottom.php'); ?>

</body>
</html>