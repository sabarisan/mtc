<!DOCTYPE html>
<html>
   <head>
      <title>MTC | Address Management</title>
      <?php include('top.php'); ?>
   </head>
   <body>
      <?php include('header.php'); ?>

      <?php include('brdcrmb.php'); ?>

	<?php 
		include('side.php');	
	 ?>


      <!-- <div class="container"> -->


      	<!-- col-sm-8 starting -->

         <!-- <div class="col-sm-9"> -->

         	<!-- address insert -->
         	<div class="panel" style="position: static;display: none;" id="addrfrmins">

						<!-- Sales stats -->
						<div class="panel panel-flat">
						<!-- <br> -->
							<div class="container-fluid">
								<div class="row text-center">
									<div class="col-md-4 bg-mybrwn-400">
									<br>
										<div class="content-group">
											<h5 class="text-semibold no-margin"><i class="icon-calendar5 position-left text-slate"></i> 100</h5>
											<span class="text-muted text-size-small">TOTAL REGISTERED ADDRESS</span>
										</div>
									</div>

									<div class="col-md-4 bg-mybrwn-400">
									<br>
										<div class="content-group">
											<h5 class="text-semibold no-margin"><i class="icon-calendar52 position-left text-slate"></i> 70</h5>
											<span class="text-muted text-size-small">LAST MONTH REGISTRATIONS</span>
										</div>
									</div>

									<div class="col-md-4 bg-mybrwn-400">
									<br>
										<div class="content-group">
											<h5 class="text-semibold no-margin"><i class="icon-cash3 position-left text-slate"></i> $23,464</h5>
											<span class="text-muted text-size-small">average revenue</span>
										</div>
									</div>
								</div>

						</div>
						<!-- /sales stats -->
						<div class="mystrip">
							<center><b>NEW ADDRESS REGISTRATION</b></center>	
						</div>
					</div>
					
					
               <div class="panel-body">
                  <form method="post" id="addrreg1" action="<?php echo base_url().'inventory/Address/addrins' ?>">
                  <div class="container-fluid">
                  <!-- row1 -->
  					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Street</label>
								<div class="input-group">
									<span class="input-group-addon"><span class="icon icon-location3"></span></span>
									<input class="form-control" name="stret" id="" placeholder="Street" type="text">
									<!-- <p id="addrid"></p> -->
								</div>
                                
                            </div>
						</div>

						<div class="col-md-6">
							<div class="form-group">
								<label>Location</label>
                               <div class="input-group">
			                                 <span class="input-group-addon"><span class="icon icon-person"></span></span>
			                                 <input class="form-control" name="loc" id="" placeholder="Location" type="text">
			                    </div>
                            </div>
						</div>
					</div>
					<!-- row1 Ending -->

					<!-- row2 -->
  					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Landmark</label>
								 	<div class="input-group">
		                                 <span class="input-group-addon"><span class="icon icon-map"></span></span>
		                                 <input class="form-control" name="lndmrk" id="" placeholder="Landmark" type="text">
		                             </div>
                            </div>
						</div>

						<div class="col-md-6">
							<div class="form-group">
								<label>City</label>
                           			<div class="input-group">
		                                 <span class="input-group-addon"><span class="icon icon-check"></span></span>
		                                 <input class="form-control" name="cty" id="" placeholder="City" type="text">
		                            </div>
                            </div>
						</div>
					</div>
					<!-- row2 Ending -->

					<!-- row3 -->
  					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>District</label>
								 	<div class="input-group">
		                                 <span class="input-group-addon"><span class="icon icon-file-stats"></span></span>
		                                 <input class="form-control" name="distrct" id="" placeholder="District" type="text">
		                              </div>
                            </div>
						</div>

						<div class="col-md-6">
							<div class="form-group">
								<label>State</label>
                           			 <div class="input-group">
		                                 <span class="input-group-addon"><span class="icon icon-file-spreadsheet"></span></span>
		                                 <input class="form-control" name="state" id="" placeholder="State" type="text">
		                              </div>
                            </div>
						</div>
					</div>
					<!-- row3 Ending -->

					<!-- row4 -->
  					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Pincode</label>
								 	<div class="input-group">
		                                 <span class="input-group-addon"><span class="icon icon-file-spreadsheet"></span></span>
		                                 <input class="form-control" name="pin" id="" placeholder="Pincode" type="text">
		                            </div>
                            </div>
						</div>

						<div class="col-md-6">
							<div class="form-group">
								<label>Country</label>
                           			  <div class="input-group">
		                                 <span class="input-group-addon"><span class="icon icon-list"></span></span>
		                                 <select name="slct" class="form-control select" id="cntryval">
		                                    <option value="" selected="selected">Select Country</option>
		                                    <option value="India">India</option>
		                                 </select>
		                              </div>
                            </div>
						</div>
					</div>
					<!-- row4 Ending -->

				</div>
				<!-- Container Ending -->
                   </div>
                     <div class="panel-footer">
                     	<div class="text-right">
                        <button type="submit" class="btn btn-danger" style="margin-right: 2%;">Submit <i class="icon-arrow-right14 position-right"></i></button>
                     </div>	
                     </div>
                     
                  </form>
               </div>
         	<!-- address insert -->


            <div class="panel" style="position: static;display: none;" id="addrfrmupdte">

						<!-- Sales stats -->
						<div class="panel panel-flat">
						<!-- <br> -->
							<div class="container-fluid">
								<div class="row text-center">
									<div class="col-md-4 bg-mybrwn-400">
									<br>
										<div class="content-group">
											<h5 class="text-semibold no-margin"><i class="icon-calendar5 position-left text-slate"></i> 100</h5>
											<span class="text-muted text-size-small">TOTAL REGISTERED ADDRESS</span>
										</div>
									</div>

									<div class="col-md-4 bg-mybrwn-400">
									<br>
										<div class="content-group">
											<h5 class="text-semibold no-margin"><i class="icon-calendar52 position-left text-slate"></i> 70</h5>
											<span class="text-muted text-size-small">LAST MONTH REGISTRATIONS</span>
										</div>
									</div>

									<div class="col-md-4 bg-mybrwn-400">
									<br>
										<div class="content-group">
											<h5 class="text-semibold no-margin"><i class="icon-cash3 position-left text-slate"></i> $23,464</h5>
											<span class="text-muted text-size-small">average revenue</span>
										</div>
									</div>
								</div>

						</div>
						<!-- /sales stats -->
						<div class="mystrip">
							<center><b>NEW ADDRESS REGISTRATION</b></center>	
						</div>
					</div>
					
					
               <div class="panel-body">
                  <form id="addrreg">
                  <div class="container-fluid">
                  <!-- row1 -->
  					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Street</label>
								<div class="input-group">
									<span class="input-group-addon"><span class="icon icon-location3"></span></span>
									<input class="form-control" name="stret" id="strttxt" placeholder="Street" type="text">
									<!-- <p id="addrid"></p> -->
									<input type="hidden" id="rwid" value="" name="rwid">
								</div>
                                
                            </div>
						</div>

						<div class="col-md-6">
							<div class="form-group">
								<label>Location</label>
                               <div class="input-group">
			                                 <span class="input-group-addon"><span class="icon icon-person"></span></span>
			                                 <input class="form-control" name="loc" id="loctxt" placeholder="Location" type="text">
			                    </div>
                            </div>
						</div>
					</div>
					<!-- row1 Ending -->

					<!-- row2 -->
  					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Landmark</label>
								 	<div class="input-group">
		                                 <span class="input-group-addon"><span class="icon icon-map"></span></span>
		                                 <input class="form-control" name="lndmrk" id="lndmtxt" placeholder="Landmark" type="text">
		                             </div>
                            </div>
						</div>

						<div class="col-md-6">
							<div class="form-group">
								<label>City</label>
                           			<div class="input-group">
		                                 <span class="input-group-addon"><span class="icon icon-check"></span></span>
		                                 <input class="form-control" name="cty" id="ctytxt" placeholder="City" type="text">
		                            </div>
                            </div>
						</div>
					</div>
					<!-- row2 Ending -->

					<!-- row3 -->
  					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>District</label>
								 	<div class="input-group">
		                                 <span class="input-group-addon"><span class="icon icon-file-stats"></span></span>
		                                 <input class="form-control" name="distrct" id="distxt" placeholder="District" type="text">
		                              </div>
                            </div>
						</div>

						<div class="col-md-6">
							<div class="form-group">
								<label>State</label>
                           			 <div class="input-group">
		                                 <span class="input-group-addon"><span class="icon icon-file-spreadsheet"></span></span>
		                                 <input class="form-control" name="state" id="stetxt" placeholder="State" type="text">
		                              </div>
                            </div>
						</div>
					</div>
					<!-- row3 Ending -->

					<!-- row4 -->
  					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Pincode</label>
								 	<div class="input-group">
		                                 <span class="input-group-addon"><span class="icon icon-file-spreadsheet"></span></span>
		                                 <input class="form-control" name="pin" id="pintxt" placeholder="Pincode" type="text">
		                            </div>
                            </div>
						</div>

						<div class="col-md-6">
							<div class="form-group">
								<label>Country</label>
                           			  <div class="input-group">
		                                 <span class="input-group-addon"><span class="icon icon-list"></span></span>
		                                 <select name="slct" class="form-control select" id="cntryval">
		                                    <option value="" selected="selected">Select Country</option>
		                                    <option value="India">India</option>
		                                 </select>
		                              </div>
                            </div>
						</div>
					</div>
					<!-- row4 Ending -->

				</div>
				<!-- Container Ending -->
                   </div>
                     <div class="panel-footer">
                     	<div class="text-right">
                        <button type="button" id="addrupd" class="btn btn-danger" style="margin-right: 2%;">Submit <i class="icon-arrow-right14 position-right"></i></button>
                     </div>	
                     </div>
                     
                  </form>
               </div>

                 <!-- Basic initialization -->
        <div class="panel panel-flat">
          <div class="panel-heading">
            <h5 class="panel-title">Address Management Details</h5>
            <div class="heading-elements">
              &nbsp;&nbsp;&nbsp;<div class="btn btn-info" onclick="addrins()"><i class="icon icon-add" style="color: #fff; "></i> Add</div>
              <ul class="icons-list">
                        <li><a data-action="collapse"></a></li>
                        <!-- <li><a data-action="reload"></a></li> -->
                        <!-- <li><a data-action="close"></a></li> -->
                      </ul>
                    </div>
          </div>

          <div class="container-fluid">
              <table class="table datatable-button-init-basic table-responsive" id="addrfrm">
            <thead>
            <th>Sno</th>
            <!-- <th>Address Code</th> -->
            <th>Stret</th>
            <th>Loc</th>
            <th>Lndmrk</th>
            <th>City</th>
            <th>Dist</th>
            <th>State</th>
            <!-- <th>Pincode</th> -->
            <!-- <th>Country</th> -->
            <!-- <th><center>Created At</center></th> -->
            <th><center>Operation</center></th>
         </thead>
          </table>  
          </div>
        </div>
        <!-- /basic initialization -->

            </div>
         <!-- div col-sm-8 Ending -->

       

      </div>

      


      
      <?php include('footer.php'); ?>	
      <?php include('bottom.php'); ?>
   </body>
