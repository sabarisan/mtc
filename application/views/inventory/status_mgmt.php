<!DOCTYPE html>
<html>
<head>
	<title>MTC | STATUS MANAGEMENT</title>
	<?php include('top.php'); ?>
</head>
<body>
	<?php include('header.php'); ?>

	 <?php include('brdcrmb.php'); ?>

	   <?php include('side.php'); ?>

	   <!-- <div class="col-sm-9"> -->
         <div class="panel" style="position: static;display: none;" id="stsfrm">
            <!-- Sales stats -->
            <div class="panel panel-flat">
               <!-- /sales stats -->
               <div class="bg-mybrwn-400-hed">
                  <b>STATUS MANAGEMENT</b>
               </div>
            </div>
            <div class="panel-body">
               <form action="#" id="status_mgmt">
                  <!-- container song -->
                  <div class="container-fluid">
                     <!-- row1 -->
                     <div class="row">
                        <div class="col-md-6">
                           <div class="form-group">
                              <label>Styp</label>
                                 <div class="input-group">
                                       <span class="input-group-addon"><span class="icon icon-weather-windy"></span></span>
                                       <input class="form-control" value="" name="styp" id="styptxt" placeholder="Styp" type="text">
                                    </div>
                           </div>
                        </div>
                        <input type="hidden" id="rwid" name="rwid">
                        <div class="col-md-6">
                           <div class="form-group">
                              <label>Nam</label>
                                 <div class="input-group">
                                       <span class="input-group-addon"><span class="icon icon-user"></span></span>
                                       <input class="form-control" value="" name="nam" id="namtxt" placeholder="Nam" type="text">
                                    </div>
                           </div>
                        </div>
                     </div>
                     <!-- row1 Ending -->

                      <!-- row2 -->
                     <div class="row">
                        <div class="col-md-6">
                           <div class="form-group">
                              <label>Name</label>
                                 <div class="input-group">
                                       <span class="input-group-addon"><span class="icon icon-user"></span></span>
                                       <input class="form-control" value="" name="name" id="nametxt" placeholder="Name" type="text">
                                    </div>
                           </div>
                        </div>

                        <div class="col-md-6">
                           <div class="form-group">
                              <label>Name1</label>
                                 <div class="input-group">
                                       <span class="input-group-addon"><span class="icon icon-user"></span></span>
                                       <input class="form-control" value="" name="name1" id="nametxt1" placeholder="Name1" type="text">
                                    </div>
                           </div>
                        </div>
                     </div>
                     <!-- row2 Ending -->

                      <!-- row3 -->
                     <div class="row">
                        <div class="col-md-12">
                           <div class="form-group">
                              <label>Name2</label>
                                 <div class="input-group">
                                       <span class="input-group-addon"><span class="icon icon-user"></span></span>
                                       <input class="form-control" value="" name="name2" id="nametxt2" placeholder="Name2" type="text">
                                    </div>
                           </div>
                        </div>
                     </div>
                     <!-- row3 Ending -->

                  </div>
                  <!-- container-fluid Ending -->

               
                  
            </div>
            <div class="panel-footer">
            <div class="text-right">
            <button type="button" id="stssbmt" class="btn btn-danger" style="margin-right: 2%;">Submit <i class="icon-arrow-right14 position-right"></i></button>
            </div>   
            </div>
            </form>
         </div>
         <!-- div col-sm-10 Ending -->

         <!-- Basic datatable -->
            <div class="panel panel-flat">
               <div class="panel-heading">
                  <h5 class="panel-title">Status Management</h5>
                  <div class="heading-elements">
                     <button type="button" name="" id="grpmgmtentr" onclick="stsins()" class="btn btn-info"><i class="icon icon-add" style="color: #fff;"></i>ADD</button>
                     <ul class="icons-list">
                           <li><a data-action="collapse"></a></li>
                     <!--       <li><a data-action="reload"></a></li>
                           <li><a data-action="close"></a></li> -->
                        </ul>
                     </div>
               </div>

               <table id="DataTables_Table_0" class="table datatable-button-flash-name dataTable no-footer" role="grid" aria-describedby="DataTables_Table_0_info">
                  <thead>
                     <tr>
                        <th>Sno</th>
                        <th>Styp</th>
                        <th>Nam</th>
                        <th>Name</th>
                        <th>Name1</th>
                        <!-- <th>Name2</th> -->
                        <th>Createddate</th>
                        <!-- <th>Updateddate</th> -->
                        <th>Operation</th>
                     </tr>
                  </thead>
                  <tbody>
                     <?php if (isset($stsmgmt)) { ?>

                        <?php $i=1; foreach ($stsmgmt as $stsmg) { ?>
                           <tr>
                              <td><?php echo $i; ?></td>
                              <td><?php echo $stsmg->styp; ?></td>
                              <td><?php echo $stsmg->nam; ?></td>
                              <td><?php echo $stsmg->name; ?></td>
                              <td><?php echo $stsmg->name1; ?></td>
                              <!-- <td><?php echo $stsmg->name2; ?></td> -->
                              <td><?php echo $stsmg->createddate; ?></td>
                              <!-- <td><?php echo $stsmg->updateddate; ?></td> -->
                              <!-- <td><?php echo $stsmg->updateddate; ?></td> -->

                            <td>
                              <div>
                                <center>
                                  <i class="btn btn-danger glyphicon glyphicon-trash" style="font-size:12px;" onclick="stsrmv('<?php echo $stsmg->id;?>')">
                                  </i>
                                  <a href="#">
                                    <i class="btn btn-success glyphicon" style="font-size:12px;text-transform:lowercase;" onclick="stsupdte('<?php echo $stsmg->id;?>','<?php echo $stsmg->styp;?>','<?php echo $stsmg->nam;?>','<?php echo $stsmg->name;?>','<?php echo $stsmg->name1;?>','<?php echo $stsmg->name2;?>');">
                                      <i class="glyphicon glyphicon-pencil" style="font-size:12px;">
                                      </i>
                                    </i>
                                  </a>
                                </center>
                              </div>
                            </td>
                           </tr>

                        <?php $i++; } ?>
                   <?php } ?>
                  </tbody>
               </table>
            </div>
            <!-- /basic datatable -->

      </div>
      </div>
      <!-- Division for content wrapper Ending -->

</div>

   

	 <?php 
	 	include('footer.php'); 
	 ?>
</body>
	<?php 
		include('bottom.php');
	 ?>
</html>