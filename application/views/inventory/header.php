
	
	<div class="navbar navbar-inverse bg-indigo">
		<div class="navbar-header">
					

						<a class="navbar-brand unicode-style91 fs24 txt-shad303 fw-600" href="<?php echo base_url(); ?>" style=
					"font-size:24px;"><!-- <img src="assets/images/logo_light.png" alt=""> --><b>மாநகர் போக்குவரத்து கழகம், சென்னை </b></a>

		</div>
		<div class="navbar-header pull-right">
					<ul class="nav navbar-nav navbar-right">
				<li>
					<div id="datetimedisplay" class="txt-shad003" style="margin-top:11px; padding:5px; font-family: 'LatoRegular','Lucida Grande','Lucida Sans Unicode',Helvetica,sans-serif !important; color:#FFF;"></div>
				</li>
				<li>
					<a href="">
						
						
						<span class="label label-inline position-right bg-success-400"> <i class="icon-user position-left"></i> Hi, <?php echo $this->session->userdata('username'); ?></span>
					</a>
				</li>

				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<i class="icon-cog3"></i>
						<span class="visible-xs-inline-block position-right">Share</span>
						<span class="caret"></span>
					</a>

					<ul class="dropdown-menu dropdown-menu-right">
						<li><a href="#"><i class="icon-user-lock"></i> Account security</a></li>
						<li><a href="#"><i class="icon-statistics"></i> Analytics</a></li>
						<li><a href="#"><i class="icon-accessibility"></i> Accessibility</a></li>
						<li class="divider"></li>
						<li><a href="#"><i class="icon-gear"></i> All settings</a></li>
						<li><a href="<?php echo base_url().'inventory/Login/userlogout/'.$this->session->userdata('userid'); ?>"><i class="icon-arrow-up-right2"></i> Logout</a></li>
					</ul>
				</li>
			</ul>
				
			
		</div>
	</div>
	<!-- /main navbar -->



	<!-- second navbar -->


	<div class="navbar navbar-default" id="navbar-second">
		<div class="navbar-header">
		<!-- Metropolitan Transport Corporation(Chennai) Ltd. -->

					<div class="col-sm-2">
						<img src="<?php echo base_url().'assets/images/mtclogo.png';?>" alt="" style="height: 60px;">
					</div>
					

		</div>
					
		<ul class="nav navbar-nav no-border visible-xs-block">
			<li><a class="text-center collapsed" data-toggle="collapse" data-target="#navbar-second-toggle"><i class="icon-menu7"></i></a></li>

		</ul>



		<div class="navbar-collapse collapse" id="navbar-second-toggle">
			<?php 
				if ($this->session->userdata('mymenu')) {

					
					$mydat  = array();
					$mydat =  $this->session->userdata('mymenu');

					// echo '<pre>'; print_r($mydat['menudata']);	echo '</pre>';
					//echo $mydat['menudata'][0]['menuname'];

					foreach ($mydat['menudata'] as $men) {


					?>


			<ul class="nav navbar-nav">

				<?php 
					if ($men['menulink'] != 'javascript:void(0)' && $men['menuaccess']=='1' || $men['menulink'] != 'javascript:void(0)' && $men['menuaccess']=='0') {

						// echo $men->menuicon;
				 ?>

				<li class=""><a href="<?php echo base_url(); ?>"><i class="<?php echo $men['menuicon'];?> position-left"></i> <?php echo $men['menuname']; ?></a></li>
			<?php } else{?>

				
				
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<i class="<?php echo $men['menuicon']; ?> position-left"></i> <?php echo $men['menuname']; ?> <span class="caret"></span>
					</a>
					
					<ul class="dropdown-menu width-250">

						<?php 
							foreach ($men['submenudata'] as $submnu) {?>
 
								<?php  if ($men['menuid']== $submnu['menuid'] && ($submnu['submenuaccess']!='' && $submnu['submenuaccess']=="0" || $submnu['submenuaccess']!='' && $submnu['submenuaccess']=="1")) { ?>

								<li class="dropdown-header">
									<a href="<?php echo base_url().'inventory/'.$submnu['submenulink'] ?>"><i class="<?php echo $submnu['submenuicon']; ?>"></i><?php echo $submnu['submenuname']; ?></a>
								</li>

					<?php }} ?>
						
					</ul>
				</li>
			 <?php }?>

			</ul>

			<?php } ?>
			

		<?php }?>
			
		</div>
	</div>


	