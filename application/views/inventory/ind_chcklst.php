<!DOCTYPE html>
<html>
<head>
	<title>MTC | Indent Checklist</title>
	<?php include('top.php'); ?>
</head>
<body>
	<?php include 'header.php'; ?>
	<!-- <?php //include 'brdcrmb.php'; ?> -->
	

	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">
			 <!-- Clickable title -->
	            <div class="panel panel-white">
					<div class="panel-heading">
						<h6 class="panel-title"><?php echo $prsntbrdcrmb; ?></h6>
						<div class="heading-elements">
							<ul class="icons-list">
		                		<li><a data-action="collapse"></a></li>
		                		<li><a data-action="reload"></a></li>
		                		<li><a data-action="close"></a></li>
		                	</ul>
	                	</div>
					</div>
					<div class="table-responsive">
								<?php 
									include 'ind_headr.php';
								 ?>
					</div>

							<div class="container">
								<div class="row">
									<!-- data tables -->
									<div class="panel panel-flat">
						<div class="panel-heading">
							<h5 class="panel-title">Indent Checklist</h5>
							<div class="heading-elements">
								<ul class="icons-list">
			                		<li><a data-action="collapse"></a></li>
			                		<li><a data-action="reload"></a></li>
			                		<li><a data-action="close"></a></li>
			                	</ul>
		                	</div>
						</div>

						<!-- <?php echo '<pre>'?> <?php print_r($chcklist_tabl); ?> <?php '</pre>' ?> -->
						 <!-- <?php 
							foreach ($chcklist_tabl as $mydat) {
								//print_r($mydat);
							}
						 ?> -->

						<!-- <div class="panel-body">
							By default the name of the file created by the <code>excelFlash</code>, <code>csvFlash</code> and <code>pdfFlash</code> button types will automatically be taken from the document's title element. It is also possible to set the file name to a specific value using the title option of these three button types. This example shows the <code>title</code> option being set for the <code>excelFlash</code> and <code>pdfFlash</code> buttons.
						</div> -->



						 <table class="table datatable-button-flash-name">
							<thead>
								<tr>
									<th>Sno</th>
									<th>Folio</th>
									<th>Part no</th>
									<th>Period</th>
									<th>Group Code</th>
									<th>Group Name</th>
								</tr>
							</thead>
							<tbody>

							

								<?php if(isset($chcklist_tabl)){$i=1; foreach($chcklist_tabl as $cktbl){?>
								<tr>
									<td><?php echo $i; ?></td>
									<td><?php echo $cktbl->FOL; ?></td>
									<td><?php echo $cktbl->PARTNO1; ?></td>
									<td><?php echo $cktbl->PERIOD; ?></td>
									<td><?php echo $cktbl->GROUP; ?></td>
									<td><?php echo $cktbl->NAM1; ?></td>
								</tr>

								<?php $i++;}} ?>
								</tbody>
							</tbody>
						</table>
					</div>
									<!-- data tables Ending -->
								</div>
							</div>
                			<br>
	            </div>
	            <!-- /clickable title -->
			</div>
			<!-- Main content -->
		</div>
		<!-- Page content -->
	</div>
	<!-- Page container -->
	


	<?php /* ?><?php include 'footer.php'; ?> <?php */ ?>
	<?php include 'bottom.php'; ?>
</body>