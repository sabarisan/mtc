// Testing 
 $(document).ready(function () {

    $('#cap_goods_cod').validate({
        ignore: ":hidden",
        rules: {
        descrip:{
          required:true,
          // aphanum:true,
        },
        folgrp:{
          required:true,
          // alpha:true,
        },

    },

      messages: {
        descrip: "Enter the Description",
        folgrp: "Enter the Folio Group",
      },

    errorPlacement: function(error, element) 
        {
          // This is the default behavior
          //error.appendTo( element.parents('.errorshow') );
          error.insertAfter( element.parents('.form-group'));
        },
             onfocusout: function(element) {
            this.element(element);
        },

    submitHandler: function (form) {
        form = $("#cap_goods_cod").serialize();
        // alert('hello world');
        // alert(form);

            $.ajax({
                type: "POST",
                url: base_url + "inventory/Capgoodscd/proced",
                data: form,

                success: function(data) {
                    if (data == '2') {
                        alert("Captial Goods Code Updated Successfully");
                        location.reload();
                    } else if(data == '1') {
                        alert("Captial Goods Code Inserted Successfully");
                        location.reload();
                    }
                }

            });
            return false; //stop the actual form post !important!

            },

            });

 });
 // Testing Ending


// ***** Capital Goods Management Js *****
// Datatables 
$(document).ready(function() {


    $('#captfrm').DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": base_url + "inventory/Capgoodscd/datatbls",
            "dataType": "json",
            "type": "POST",
            "data": {
                '<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>'
            }
        },
        "columns": [{
                "data": "id"
            },
            {
                "data": "code"
            },
            {
                "data": "description"
            },
            {
                "data": "foliogroup"
            },
            {
                "data": "createddate"
            },
            {
                "data": "remove"
            }
        ]

    });


});

function capgodupdte(id,cde,descr,folgrp) {
    var id = id;
    var cde = cde;
    var descr = descr;
    var folgrp = folgrp;

        if (id!="") {
            $('#captfrmupdte').show();
            $('#actid').text(cde);
            $('#desctxt').val(descr);
            $('#folgrptxt').val(folgrp);
            $('#rwid').val(id);
        }
}
// Capital Remove 

function captrmv(id) {

    var r = confirm("Are you sure want to Delete the Capital Goods Management!");
    if (r) {
        $.post(base_url + 'inventory/Capgoodscd/rmvcapt/' + id, function(data) {
            if (data == "") {
                alert("Capital removed Successfully");
                location.reload();
            }

        });
    }


}



function captins() {
    $('#captfrmupdte').show();
    // $('#captfrmins').show();
}

// Activity Remove Ending 
// ***** Capital Goods Management Js Ending *****

// ***** Delivery Terms Management Js *****
//  Datatables 

$(document).ready(function() {


    $('#dlvrfrm').DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": base_url + "inventory/Deliver/datatbls",
            "dataType": "json",
            "type": "POST",
            "data": {
                '<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>'
            }
        },
        "columns": [{
                "data": "id"
            },
            {
                "data": "code"
            },
            {
                "data": "descrip"
            },
            {
                "data": "name1"
            },
            {
                "data": "createddate"
            },
            {
                "data": "remove"
            }
        ]

    });


});

function dlvrupdte(id, cde, descr, nme1) {

    var id = id;
    var cde = cde;
    var descr = descr;
    var nme1 = nme1;

    // alert(id+'/'+cde+'/'+descr+'/'+nme1);

    if (id !="") {
        $('#dlvrupdte').show();  
        $('#nmetxt').val(nme1);
        $('#descrtxt').val(descr);
        $('#rwidtxt').val(id);  
    }

    
}



//  Data tables Ending 
//  update form 

function delvins() {

    $('#dlvrupdte').show();
    $('#nmetxt').val('');
    $('#descrtxt').val('');
    $('#rwidtxt').val('');

    // $('#dlvrins').show();
}

// ***** Delivery Terms Management Start *****


$(document).ready(function(){

     $('#delvr_trm_mgmt').validate({
        ignore: ":hidden",
        rules: {
        descrip:{
          required:true,
          // aphanum:true,
        },
        nme:{
          required:true,
          select:true,
        },

    },

      messages: {
        descrip: "Enter the Description",
        // nme: "Enter the Name",
      },

    errorPlacement: function(error, element) 
        {
          // This is the default behavior
          //error.appendTo( element.parents('.errorshow') );
          error.insertAfter( element.parents('.form-group'));
        },
             onfocusout: function(element) {
            this.element(element);
        },



    
    submitHandler: function (form) {
         form = $("#delvr_trm_mgmt").serialize();
         
         // alert(form);    
    $.ajax({
        type: "POST",
        url: base_url + "inventory/Deliver/proced",
        data: form,

        success: function(data) {
            // alert(data);
            if (data=="2") {
                // alert(data);
                alert("Delivery Terms Management Details Updated Successfully");
                location.reload();
                } else if(data=="1") {
                    alert("Delivery Terms Management Details Inserted Successfully");
                    location.reload();
            }
        }

    });
    // event.preventDefault();
    return false; //stop the actual form post !important!

            },

            });

 });

// ***** Delivery Terms Management Ending *****


//  update form Ending 

//  Deliver Management Remove 

function dlvrrmv(id) {

    var r = confirm("Are you sure want to Delete the Deliver Terms Management!");
    if (r) {
        $.post(base_url + 'inventory/Deliver/rmvdlvr/' + id, function(data) {
            if (data == "") {
                alert("Deliver Management removed Successfully");
                location.reload();
            }

        });
    }


}
//  Delivery Management Remove Ending 
// ***** Delivery Terms Management Js Ending *****

// ***** Depot Details Js *****
function deptins() {

    // $('#deptfrm').show();
     // $('#deptfrm').hide();

    $('#deptfrm').show();
    $('#wttxtid').show();
    $('#wottxtid').hide();
    $('#deprwid').val('');
    $('#dname').val('');
    $('#dopt').val('');
}


// Ajax data Submit

$(document).ready(function(){

     $('#depot_mgmt').validate({
        ignore: ":hidden",
        rules: {
        depid:{
          required:true,
          aphanum:true
        },
        depnme:{
          required:true,
          // alpha:true
        },

    },

      
    errorPlacement: function(error, element) 
        {
          // This is the default behavior
          //error.appendTo( element.parents('.errorshow') );
          error.insertAfter( element.parents('.form-group'));
        },
             onfocusout: function(element) {
            this.element(element);
        },

    
    submitHandler: function (form) {
         form = $("#depot_mgmt").serialize();
         
         // alert(form);    
    $.ajax({
        type: "POST",
        url: base_url + "inventory/Depot/proced",
        data: form,

        success: function(data) {
            if (data =='2') {
                // alert(data);
                alert("Depot Details Updated Successfully");
                location.reload();
            } else if(data =='1') {
                alert("Depot Details Inserted Successfully");
                location.reload();

            }
        }

    });
    return false; //stop the actual form post !important!

            },

            });

 });

// Ajax Data submit Ending


function deptupdte(rwid, id, name, opt) {

    // alert(rwid+'/'+id+'/'+name+'/'+opt);

    var rwid = rwid;
    var id = id;
    var name = name;
    var opt = opt;

    $('#wottxtid').show();
    $('#wttxtid').hide();
    // $('#deptfrm1').hide();

    if (rwid!="") {
    $('#deptfrm').show();
    $('#did').text(id);
    $('#deprwid').val(rwid);
    $('#dname').val(name);
    $('#dopt').val(opt);
    }

}


$(document).ready(function() {


    $('#posts').DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": base_url + "inventory/Depot/posts",
            "dataType": "json",
            "type": "POST",
            "data": {
                '<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>'
            }
        },


        "columns": [{
                "data": "id"
            },
            {
                "data": "depotid"
            },
            {
                "data": "depotname"
            },
            {
                "data": "depotopt"
            },
            {
                "data": "createddate"
            },
            {
                "data": "remove"
            }
        ]

    });


});






function rmve(id) {

    var r = confirm("Are you sure want to Delete the Depot!");
    if (r) {
        $.post(base_url + 'inventory/Depot/rmvdpte/' + id, function(data) {
            if (data == "") {
                alert("Depot removed Successfully");
                location.reload();
            }

        });
    } else {
        //do nothing
    }
}
// ***** Depot Details Js Ending*****

// ***** Depot Purchase Management
// ***** Datatables
$(document).ready(function() {
    $('#deptpurchfrm').DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": base_url + "inventory/Deptpurchmgmt/datatbls",
            "dataType": "json",
            "type": "POST",
            "data": {
                '<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>'
            }
        },
        "columns": [{
                "data": "id"
            },
            {
                "data": "code"
            },
            {
                "data": "name"
            },
            {
                "data": "name1"
            },
            {
                "data": "name2"
            },
            // {
            //     "data": "nm"
            // },
            {
                "data": "alpha"
            },
            {
                "data": "createddate"
            },
            {
                "data": "remove"
            }
        ]
    });
});

// ***** Depot Validations *****

// Ajax data Submit

$(document).ready(function(){

     $('#dept_prch').validate({
        ignore: ":hidden",
        rules: {
        dep:{
          required:true,
          apha:true
        },
        nam:{
          required:true,
          alpha:true
        },
        nam1:{
          required:true,
          alpha:true
        },
        nam2:{
          required:true,
          alpha:true
        },
        nm:{
          required:true,
          alpha:true
        },
        alp:{
          required:true,
          alpha:true
        },

    },

      
    errorPlacement: function(error, element) 
        {
          error.insertAfter( element.parents('.form-group'));
        },
             onfocusout: function(element) {
            this.element(element);
        },

    
    submitHandler: function (form) {
         form = $("#dept_prch").serialize();
         // alert(form);    
    $.ajax({
        type: "POST",
        url: base_url + "inventory/Deptpurchmgmt/proced",
        data: form,
        success: function(data) {
            if (data =='2') {
                // alert(data);
                alert("Depot Purchase Management Updated Successfully");
                location.reload();
            } else if(data =='1') {
                alert("Depot Purchase Management Inserted Successfully");
                location.reload();
            }
        }
    });
    return false; //stop the actual form post !important!
            },
            });
 });

// Ajax Data submit Ending

// ***** Depot Validations Ending *****


function deptpurupdte(id, cde, nme, nme1, nme2, nm, alpha) {
    $('#deptpurchupdte').show();
    $('#depttxt').text(cde);
    $('#nmetxt').val(nme);
    $('#nme1txt').val(nme1);
    $('#nme2txt').val(nme2);
    $('#nmtxt').val(nm);
    $('#alphtxt').val(alpha);
    $('#rwid').val(id);
    $('#depvlu').show();
    $('#deptxt').hide();
}



// ***** Data tables Ending
// ***** update form

function deptprchins() {

    $('#deptpurchupdte').show();
    $('#depttxt').text('');
    $('#nmetxt').val('');
    $('#nme1txt').val('');
    $('#nme2txt').val('');
    $('#nmtxt').val('');
    $('#alphtxt').val('');
    $('#rwid').val('');
    $('#depvlu').hide();
    $('#deptxt').show();
}


$('#dptprchfrmsbmt').click(function() {

    form = $("#dept_prch").serialize();

    $.ajax({
        type: "POST",
        url: base_url + "inventory/Deptpurchmgmt/updtedptprch",
        data: form,

        success: function(data) {
            if (data == '') {
                alert("Depot Purchase Section Updated Successfully");
                location.reload();
            } else {
                location.reload();
            }
        }

    });
    event.preventDefault();
    return false; //stop the actual form post !important!

});


// ***** update form Ending

// ***** Deliver Management Remove

function deptpurchrmv(id) {

    var r = confirm("Are you sure want to Delete the Depot Purchase Section Management!");
    if (r) {
        $.post(base_url + 'inventory/Deptpurchmgmt/rmvdptprch/' + id, function(data) {
            if (data == "") {
                alert("Depot Purchase Section Management removed Successfully");
                location.reload();
            }

        });
    }


}

// ***** Delivery Management Remove Ending
// ***** Depot Purchase Mangement Ending


// ***** Activity Management Js *****
// *** Activity Insert datas
function actvtins() {

    $('#actvtfrmupdte').hide();

    $('#actvtfrmins').show();
}


$('#actvtsbmt').click(function() {

    form = $("#actvitymgmt").serialize();

    $.ajax({
        type: "POST",
        url: base_url + "inventory/Activity/updteactvt",
        data: form,

        success: function(data) {
            if (data == '') {
                // alert(data);
                alert("Activity Details Updated Successfully");
                location.reload();
            } else {
                location.reload();
            }
        }

    });
    event.preventDefault();
    return false; //stop the actual form post !important!

});
// *** Activity Insert datas Ending
// alert(base_url);
// ***** Datatables javascripts
$(document).ready(function() {
    $('#actvtfrm').DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": base_url + "inventory/Activity/datatbls",
            "dataType": "json",
            "type": "POST",
            "data": {
                '<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>'
            }
        },
        "columns": [{
                "data": "id"
            },
            {
                "data": "activitycode"
            },
            {
                "data": "activityname"
            },
            {
                "data": "createddate"
            },
            {
                "data": "remove"
            }
        ]

    });


});

function actvtupdte(id, cde, nme) {

    var id = id;
    var cde = cde;
    var nme = nme;

    $('#actvtfrmins').hide();
    $('#actvtfrmupdte').show();

    $('#actid').text(cde);

    // $('#did').text(id);

    var tb2 = document.getElementById('actvnme');

    tb2.value = nme;

    var tb1 = document.getElementById('rwid');

    tb1.value = id;

}


// **** Data tables Ending 

// **** Activity Remove
function actvtrmv(id) {

    var r = confirm("Are you sure want to Delete the Activity!");
    if (r) {
        $.post(base_url + 'inventory/Activity/rmvactvt/' + id, function(data) {
            if (data == "") {
                alert("Activity removed Successfully");
                location.reload();
            }

        });
    }


}
// *** Activity Remove Ending ***

// ***** Activity Management Js *****


// ***** Fleet Management
// ****Datatables 
$(document).ready(function() {
    $('#fleetfrm').DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": base_url + "inventory/Fleet/datatbls",
            "dataType": "json",
            "type": "POST",
            "data": {
                '<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>'
            }
        },
        "columns": [{
                "data": "id"
            },
            {
                "data": "fleetid"
            },
            {
                "data": "depotid"
            },
            {
                "data": "regno"
            },
            {
                "data": "type"
            },
            {
                "data": "service"
            },
            {
                "data": "createddate"
            },
            {
                "data": "remove"
            }
        ]

    });
});


// Ajax data Submit

$(document).ready(function(){

     $('#dept_prch').validate({
        ignore: ":hidden",
        rules: {
        dep:{
          required:true,
          apha:true
        },
        nam:{
          required:true,
          alpha:true
        },
        nam1:{
          required:true,
          alpha:true
        },
        nam2:{
          required:true,
          alpha:true
        },
        nm:{
          required:true,
          alpha:true
        },
        alp:{
          required:true,
          alpha:true
        },

    },

      
    errorPlacement: function(error, element) 
        {
          error.insertAfter( element.parents('.form-group'));
        },
             onfocusout: function(element) {
            this.element(element);
        },

    
    submitHandler: function (form) {
         form = $("#fleet").serialize();

    $.ajax({
        type: "POST",
        url: base_url + "inventory/Fleet/proced",
        data: form,

        success: function(data) {
            if (data == "1") {
                alert("Fleet Inserted Successfully");
                // alert("Depot Purchase Section Updated Successfully");
                location.reload();
            } else if (data == "2") {
                alert("Fleet Updated Successfully");
                location.reload();
            }
        }

    });
    // event.preventDefault();
    return false; //stop the actual form post !important!

            },
            });
 });

// Ajax Data submit Ending


function fletins() {
    $('#fleetfm').show();
    $('#fletnotxt').show();
    $('#fltxtpra').hide();

    $('#dptidtxt').val('');
    $('#fltregtxt').val('');
    $('#typtxt').val('');
    $('#servtxt').val('');
    $('#rwid').val('');


}

function fletupdte(id, fltid, dptid, regno, typ, servc) {

    var id = id;
    var fltid = fltid;
    var dptid = dptid;
    var regno = regno;
    var typ = typ;
    var servc = servc;

    if (id != "") {
        $('#fltxtpra').show();
        $('#fletnotxt').hide();
        $('#fltxtpra').text(fltid);
        $('#dptidtxt').val(dptid);
        $('#fltregtxt').val(regno);
        $('#typtxt').val(typ);
        $('#servtxt').val(servc);
        $('#rwid').val(id);

    } else {
        $('#fletnotxt').show();
        $('#fltxtpra').hide();
    }

    $('#fleetfm').show();

    $('#depttxt').text(cde);

}


// ****Data tables Ending 

// ****update form 


$('#fltfrm').click(function() {

    form = $("#fleet").serialize();

    $.ajax({
        type: "POST",
        url: base_url + "inventory/Fleet/proced",
        data: form,

        success: function(data) {
            if (data == "1") {
                alert("Fleet Inserted Successfully");
                // alert("Depot Purchase Section Updated Successfully");
                location.reload();
            } else if (data == "2") {
                alert("Fleet Updated Successfully");
                location.reload();
            }
        }

    });
    // event.preventDefault();
    return false; //stop the actual form post !important!

});

// ****update form Ending 



function fltrmv(id) {

    var r = confirm("Are you sure want to Remove the Fleet!");
    if (r) {
        $.post(base_url + 'inventory/Fleet/rmvflt/' + id, function(data) {
            if (data == "") {
                alert("Fleet removed Successfully");
                location.reload();
            }

        });
    }


}

// ***** Fleet Management Ending



// ***** Payment Terms Management
      function pymntins(){
                 // $('#grpmgmtfrm').show();
                  $('#idtxt').val('');
                  $('#descrptxt').val('');
                  $('#nmetxt').val('');
                  $('#paymntfrm').show();
      }

       function pymntupdte(id,descrp,nme1){

                  if (id!="") {

                  $('#paymntfrm').show();
                  $('#idtxt').val(id);
                  $('#descrptxt').val(descrp);
                  $('#nmetxt').val(nme1);
                  
                  }else{
                     $('#paymntfrm').hide();                     
                  }
         }

    // *** update form
      
               
               $('#pymntsbmt').click(function() {
         
                  form = $("#paymnt").serialize();
         
          $.ajax({
            type: "POST",
            url: base_url+"inventory/Payment/proced",
            data: form,
         
            success: function(data){
                           if(data == "1")
                     {
                           // alert(data);
                          // alert("Folio Inserted Successfully");
                             alert("Payment Terms Management Inserted Successfully");
                             location.reload();
                     }
                     else if(data == "2")
                     {     alert("Payment Terms Management Updated Successfully");
                           location.reload();
                     }
            }
         
          });
          event.preventDefault();
          return false;  //stop the actual form post !important!
         
         });
           
   
      // *** remove the Payment
      
      function pymntrmv(id){

        var r=confirm("Are you sure want to Remove the Group!");
    if (r)
    {
          $.post(base_url+'inventory/Payment/rmvpymnt/'+id, function(data) {
      if(data == "")
      {
          alert("Payment term removed Successfully");
          location.reload();
      }
      
   });  
  }
}
    // *** remove the group Payment


// ***** Payment Terms Management Ending


// ***** Status Mangement js


      function stsins(){
                  $('#stsfrm').show();
                  $('#styptxt').val('');
                  $('#namtxt').val('');
                  $('#nametxt').val('');
                  $('#nametxt1').val('');
                  $('#nametxt2').val('');
      }

       function stsupdte(id,styp,nam,name,name1,name2){

                  if (id!="") {

                  $('#stsfrm').show();
                  $('#rwid').val(id);
                  $('#styptxt').val(styp);
                  $('#namtxt').val(nam);
                  $('#nametxt').val(name);
                  $('#nametxt1').val(name1);
                  $('#nametxt2').val(name2);
                  
                  }else{
                     $('#stsfrm').hide();                     
                  }
         }
   


     // ***** update form
      
               
               $('#stssbmt').click(function() {
         
                  form = $("#status_mgmt").serialize();
         
          $.ajax({
            type: "POST",
            url: base_url+"inventory/Stats/proced",
            data: form,
         
            success: function(data){
                           if(data == "1")
                     {
                           // alert(data);
                          alert("Folio Inserted Successfully");
                          location.reload();
                     }
                     else if(data == "2")
                     {     alert("Folio Updated Successfully");
                           location.reload();
                     }
            }
         
          });
          event.preventDefault();
          return false;  //stop the actual form post !important!
         
         });
           
      
 
       // ***** remove the Payment
      
      function stsrmv(id){

        var r=confirm("Are you sure want to Remove the Status!");
    if (r)
    {
          $.post(base_url+'inventory/Stats/rmvsts/'+id, function(data) {
      if(data == "")
      {
          alert("Payment term removed Successfully");
          location.reload();
      }
      
   });  
    }


      }
    
     // ***** remove the group Payment
// ***** Status Mangement js Ending



// ***** Tyre Mangement js


      function tyreins(){
                      $('#tyrefrm').show();
                      $('#rwidtxt').val('');
                      $('#codtxt').val('');
                      $('#foltxt').val('');
                      $('#tocdetxt').val('');
                      $('#dtetxt').val('');
                      $('#mtcnotxt').val('');
                      $('#manftxt').val('');
                      $('#maktxt').val('');
      }

       function tyrupdte(id,cod,fol,tocde,dte,mtcno,manf,make){

                  if (id!="") {
                      $('#tyrefrm').show();
                      $('#rwidtxt').val(id);
                      $('#codtxt').val(cod);
                      $('#foltxt').val(fol);
                      $('#tocdetxt').val(tocde);
                      $('#dtetxt').val(dte);
                      $('#mtcnotxt').val(mtcno);
                      $('#manftxt').val(manf);
                      $('#maktxt').val(make);
                  }else{
                     $('#tyrefrm').hide();                     
                  }
         }
   


     // ***** update form
      
               
               $('#tyrbtn').click(function() {
         
                  form = $("#tyre").serialize();
         
          $.ajax({
            type: "POST",
            url: base_url+"inventory/Tyre/proced",
            data: form,
         
            success: function(data){
                           if(data == "1")
                     {
                           // alert(data);
                          alert("Tyre Inserted Successfully");
                          location.reload();
                     }
                     else if(data == "2")
                     {     alert("Tyre Updated Successfully");
                           location.reload();
                     }
            }
         
          });
          event.preventDefault();
          return false;  //stop the actual form post !important!
         
         });
           
      
 
       // ***** remove the Payment
      
      function tyrrmv(id){

        var r=confirm("Are you sure want to Remove the Tyre!");
    if (r)
    {
          $.post(base_url+'inventory/Tyre/rmvtyr/'+id, function(data) {
      if(data == "")
      {
          alert("Tyre removed Successfully");
          location.reload();
      }
      
   });  
    }


      }
    
     // ***** remove the group Payment
// ***** Tyre Mangement js Ending

// ***** Address Management *****

// ***** Datatables 
   
       $(document).ready(function () {


        $('#addrfrm').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax":{
           "url": base_url+"inventory/Address/datatbls",
           "dataType": "json",
           "type": "POST",
           "data":{  '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>' }
                         },
       "columns": [
                { "data": "id" },
                // { "data": "code" },
                { "data": "street" },
                { "data": "location" },
                { "data": "landmark" },
                { "data": "city" },
                { "data": "dist" },
                { "data": "state" },
                // { "data": "pincode" },
                // { "data": "country" },
                { "data": "remove" },
                // { "data": "createddate" },
             ]  

       });

         
    });

       function addrupdte(cde,id,strt,loc,lndm,cty,dis,ste,cntry,pin){

          // alert(id+'/'+strt+'/'+loc+'/'+lndm+'/'+cty+'/'+dis+'/'+ste+'/'+cntry+'/'+pin);
           var cde  = cde;
             var id  = id;
             var strt = strt;
             var loc = loc;
             var lndm = lndm;
             var cty = cty;
             var dis = dis;
             var ste = ste;
             var cntry = cntry;
             var pin = pin;

              $('#addrfrmins').hide();
              $('#addrfrmupdte').show();

              $('#addrid').text(cde);

              // $('#did').text(id);
                

                var tb1 = document.getElementById('strttxt');
                tb1.value = strt;

                var tb2 = document.getElementById('loctxt');
                tb2.value = loc;

                var tb3 = document.getElementById('lndmtxt');              
                tb3.value = lndm;

                var tb4 = document.getElementById('ctytxt');              
                tb4.value = cty;

                var tb5 = document.getElementById('distxt');              
                tb5.value = dis;

                var tb6 = document.getElementById('stetxt');              
                tb6.value = ste;


                // $('[slct=options]').val( 1 );/

                var tb8 = document.getElementById('pintxt');              
                tb8.value = pin;
                
                var tb7 = document.getElementById('rwid');
                tb7.value = id;

       }


   
  // ***** Data tables Ending 


    // ***** data insert and update 

    

    function addrins(){

      $('#addrfrmupdte').hide();

      $('#addrfrmins').show();
    }


          $('#addrupd').click(function() {

             form = $("#addrreg").serialize();

     $.ajax({
       type: "POST",
       url: base_url+"inventory/Address/updteaddr",
       data: form,

       success: function(data){
                      if(data=='')
                {
                    // alert(data);
                      alert("Activity Details Updated Successfully");
                      location.reload();
                }
                else
                {
                      location.reload();
                }
       }

     });
     event.preventDefault();
     return false;  //stop the actual form post !important!

  });
     

    // ***** data insert and update Ending 


     // ***** Address Remove 
    
      function addrmv(id){

        var r=confirm("Are you sure want to Delete the Address!");
    if (r)
    {
          $.post(base_url+'inventory/Address/rmvaddr/'+id, function(data) {
      if(data == "")
      {
          alert("Address removed Successfully");
          location.reload();
      }
      
   });  
    }


      }
   
    // ***** Address Remove Ending 

// ***** Address Management Ending *****

// ***** Control Management *****

 
         $(document).ready(function () {
         
         
          $('#ctrlfrm').DataTable({
              "processing": true,
              "serverSide": true,
              "ajax":{
             "url": base_url+"inventory/Control/datatbls",
             "dataType": "json",
             "type": "POST",
             "data":{  '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>' }
                           },
         "columns": [
                  { "data": "id" },
                  { "data": "type" },
                  { "data": "descrip"},
                  { "data": "foliogroup"},
                  { "data": "createddate" },
                  { "data": "remove" }
               ]  
         
         });
         
           
         });
         
         function ctrlupdte(id,typ,descr,folgrp){
         
               var id     = id;
               var typ    = typ;
               var descr  = descr;
               var folgrp = folgrp;
         
                $('#captfrmins').hide();
                $('#ctrlupdte').show();
         
                // $('#actid').text(cde);
         
                // $('#did').text(id);
         
                var tb1 = document.getElementById('folgrp');
         
                  tb1.value = folgrp;
         
                var tb2 = document.getElementById('type');              
         
                  tb2.value = typ;
         
                 var tb3 = document.getElementById('descrip');              
         
                  tb3.value = descr;

                   var tb4 = document.getElementById('ctrlid');              
         
                  tb4.value = id;
         
         }
         
         
      
       // *** Data tables Ending
       // *** update form
      
         function ctrlins(){
         
           $('#ctrlupdte').hide();
         
           $('#captfrmins').show();
         }
         
         
               $('#ctrlfrmdt').click(function() {
         
                  form = $("#ctrl_mgmtupd").serialize();
         
          $.ajax({
            type: "POST",
            url: base_url+"inventory/Control/updtectrl",
            data: form,
         
            success: function(data){
                           if(data=='1')
                     {
                         // alert(data);
                           alert("Control Management Updated Successfully");
                           location.reload();
                     }
                     else
                     {
                           location.reload();
                     }
            }
         
          });
          event.preventDefault();
          return false;  //stop the actual form post !important!
         
         });
           
      
       // *** update form Ending

       // *** Control Management Remove
    
      function ctrlrmv(id){

        var r=confirm("Are you sure want to Delete the Control Management!");
    if (r)
    {
          $.post(base_url+'inventory/Control/rmvctrl/'+id, function(data) {
      if(data == "")
      {
          alert("Control Management removed Successfully");
          location.reload();
      }
      
   });  
    }


      }
    
     // *** Control Management Remove Ending

// ***** Control Management Ending *****


//  ***** Folio Management *****
    
    // **** Datatables ***
      
         $(document).ready(function () {
         
         
          $('#folfrm').DataTable({
              "processing": true,
              "serverSide": true,
              "ajax":{
             "url": base_url+"inventory/Folio/datatbls",
             "dataType": "json",
             "type": "POST",
             "data":{  '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>' }
                           },
         "columns": [
                  { "data": "id" },
                  { "data": "folio" },
                  { "data": "grp" },
                  { "data": "nom1"},
                  // { "data": "nom2"},
                  // { "data": "nom3"},
                  // { "data": "par1"},
                  // { "data": "par2"},
                  // { "data": "par3"},
                  // { "data": "lyc1"},
                  // { "data": "lyc2"},
                  // { "data": "lyc3"},
                  // { "data": "type"}, 
                  { "data": "class"},
                  // { "data": "ved"},
                  // { "data": "uom"},
                  // { "data": "max"},
                  // { "data": "min"},
                  // { "data": "icod"},
                  // { "data": "rcod"},
                  { "data": "rate"},
                  // { "data": "grp1"},
                  // { "data": "stkcode"},
                  // { "data": "isscode"},
                  // { "data": "newfield"},
                  // { "data": "createddate" },
                  { "data": "remove" }
               ]  
         
         });
         
           
         });

         function folins() {
                     $('#folidfrm').show();

                  // alert("Its Working");
                  // tb1.value = grp;
                  $('#grptxt').val('');
                  $('#nom1txt').val('');
                  $('#nom2txt').val('');
                  $('#nom3txt').val('');
                  $('#prt1txt').val('');
                  $('#prt2txt').val('');
                  $('#prt3txt').val('');
                  $('#lyc1txt').val('');
                  $('#lyc2txt').val('');
                  $('#lyc3txt').val('');
                  $('#typtxt').val('');
                  $('#clstxt').val('');
                  $('#vedtxt').val('');
                  $('#uomtxt').val('');
                  $('#maxtxt').val('');
                  $('#mintxt').val('');
                  $('#icodtxt').val('');
                  $('#rcodtxt').val('');
                  $('#ratetxt').val('');
                  $('#grp1txt').val('');
                  $('#stkcdtxt').val('');
                  $('#isscdtxt').val('');
                  $('#nwfldtxt').val('');
                  $('#rwidtxt').val('');
                  // var tb1 = document.getElementById('grptxt');              
                  // tb1.value = grp;

            
         }
         
         function folupdte(id,folid,grp,nom1,nom2,nom3,par1,par2,par3,lyc1,lyc2,lyc3,typ,cls,ved,uom,max,min,icod,rcod,rte,grp1,stkcde,isscde,nwfld){
               
               // alert(id+'/'+folid+'/'+grp+'/'+nom1+'/'+nom2+'/'+nom3+'/'+par1+'/'+par2+'/'+par3+'/'+lyc1+'/'+lyc2+'/'+lyc3+'/'+typ+'/'+cls+'/'+ved+'/'+uom+'/'+max+'/'+min+'/'+icod+'/'+rcod+'/'+rte+'/'+grp1+'/'+stkcde+'/'+isscde+'/'+nwfld);

               var id       = id;
               var folid    = folid;
               var grp      = grp;
               var nom1     = nom1;
               var nom2     = nom2;
               var nom3     = nom3;
               var par1     = par1;
               var par2     = par2;
               var par3     = par3;
               var lyc1     = lyc1;
               var lyc2     = lyc2;
               var lyc3     = lyc3;
               var typ      = typ;
               var cls      = cls;
               var ved      = ved;
               var uom      = uom;
               var max      = max;
               var min      = min;
               var icod     = icod;
               var rcod     = rcod;
               var rte      = rte;
               var grp1     = grp1;
               var stkcde   = stkcde;
               var isscde   = isscde;
               var nwfld    = nwfld;
         
                  if (id!="") {
                     $('#folidfrm').show();

                  // alert("Its Working");
                  // tb1.value = grp;
                  $('#grptxt').val(grp);
                  $('#nom1txt').val(nom1);
                  $('#nom2txt').val(nom2);
                  $('#nom3txt').val(nom3);
                  $('#prt1txt').val(par1);
                  $('#prt2txt').val(par2);
                  $('#prt3txt').val(par3);
                  $('#lyc1txt').val(lyc1);
                  $('#lyc2txt').val(lyc2);
                  $('#lyc3txt').val(lyc3);
                  $('#typtxt').val(typ);
                  $('#clstxt').val(cls);
                  $('#vedtxt').val(ved);
                  $('#uomtxt').val(uom);
                  $('#maxtxt').val(max);
                  $('#mintxt').val(min);
                  $('#icodtxt').val(icod);
                  $('#rcodtxt').val(rcod);
                  $('#ratetxt').val(rte);
                  $('#grp1txt').val(grp1);
                  $('#stkcdtxt').val(stkcde);
                  $('#isscdtxt').val(isscde);
                  $('#nwfldtxt').val(nwfld);
                  $('#rwidtxt').val(id);
                  // var tb1 = document.getElementById('grptxt');              
                  // tb1.value = grp;

                 

                  }else{
                     $('#fletnotxt').show();
                     $('#fltxtpra').hide();
                  }

         
         }
         
         
      
      // **** Data tables Ending ***

      // **** update form ***
      
               
               $('#folfrmsub').click(function() {
         
                  form = $("#folio").serialize();
         
          $.ajax({
            type: "POST",
            url: base_url+"inventory/Folio/proced",
            data: form,
         
            success: function(data){
                           if(data == "1")
                     {
                           // alert(data);
                          alert("Folio Inserted Successfully");
                         //   // alert("Depot Purchase Section Updated Successfully");
                           location.reload();
                     }
                     else if(data == "2")
                     {     alert("Folio Updated Successfully");
                           location.reload();
                     }
            }
         
          });
          event.preventDefault();
          return false;  //stop the actual form post !important!
         
         });
      // **** update form Ending ***
      
      function folrmv(id){

        var r=confirm("Are you sure want to Remove the Folio!");
    if (r)
    {
          $.post(base_url+'inventory/Folio/rmvfol/'+id, function(data) {
      if(data == "")
      {
          alert("Folio removed Successfully");
          location.reload();
      }
      
   });  
    }
      }
    

// ***** Folio Management Ending *****


//  *****  Group Management *****
  

   

      function grpins(){
                 $('#grpmgmtfrm').show();
                  $('#idtxt').val('');
                  $('#grptxt').val('');
                  $('#grpnmetxt').val('');
      }

       function grpupdte(id,grp,grpnme){

                  if (id!="") {
                     $('#grpmgmtfrm').show();

                  $('#idtxt').val(id);
                  $('#grptxt').val(grp);
                  $('#grpnmetxt').val(grpnme);
                  
                  }
         }
   


    // ***** update form 
      
               
               $('#grpdtasbmt').click(function() {
         
                  form = $("#grp_mgmt").serialize();
         
          $.ajax({
            type: "POST",
            url: base_url+"inventory/Grupmgmt/proced",
            data: form,
         
            success: function(data){
                     // alert(data);
                           if(data == "1")
                     {
                           // alert(data);
                           alert("Payment Terms Inserted Successfully");
                           location.reload();
                     }
                     else if(data == "2")
                     {     alert("Payment Terms Updated Successfully");
                           location.reload();
                     }
            }
         
          });
          event.preventDefault();
          return false;  //stop the actual form post !important!
         
         });
           
      
      // ***** update form Ending 


      
      function grprmv(id){

        var r=confirm("Are you sure want to Remove the Group!");
    if (r)
    {
          $.post(base_url+'inventory/Grupmgmt/rmvgrp/'+id, function(data) {
      if(data == "")
      {
          alert("Group removed Successfully");
          location.reload();
      }
      
   });  
    }


      }
    

// ***** Group Management Ending *****



// ***** Store Management *****


    // ***** data insert and update 
    function streins(){
      $('#strefrmupdte').show();
      $('#rwid').val('');
      $('#tb1').val('');
      $('#tb2').val('');
      $('#tb3').val('');
      $('#tb4').val('');
      $('#tb5').val('');
      $('#tb6').val('');
      $('#tb7').val('');
      $('#tb8').val('');
      $('#tb9').val('');
      $('#tb10').val('');
      $('#tb11').val('');
      $('#tb12').val('');
      $('#tb13').val('');
      $('#tb14').val('');
      $('#tb15').val('');
      $('#tb16').val('');
      $('#tb17').val('');
      $('#tb18').val('');
      $('#tb19').val('');
      $('#tb20').val('');
      $('#tb21').val('');
      $('#tb22').val('');
      $('#tb23').val('');
      $('#tb24').val('');

    }

    function streupdte(id,type,name,name1,add1,add2,add3,add4,pincode,state,stdcode,phone1,phone2,phone3,fax,telex,vfrom,vto,asrturef,asrtudt,venrate,acode,tinno,panno,gstin){
      // alert(id+'/'+type+'/'+name+'/'+name1+'/'+add1+'/'+add2+'/'+add3+'/'+add4+'/'+pincode+'/'+state+'/'+stdcode+'/'+phone1+'/'+phone2+'/'+phone3+'/'+fax+'/'+telex+'/'+vfrom+'/'+vto+'/'+asrturef+'/'+asrtudt+'/'+venrate+'/'+acode+'/'+tinno+'/'+panno+'/'+gstin);

      if (id!="") {

        $('#strefrmupdte').show();
// ,,,,,,gstin
      $('#rwid').val(id);
      $('#tb1').val(type);
      $('#tb2').val(name);
      $('#tb3').val(name1);
      $('#tb4').val(add1);
      $('#tb5').val(add2);
      $('#tb6').val(add3);
      $('#tb7').val(add4);
      $('#tb8').val(pincode);
      $('#tb9').val(state);
      $('#tb10').val(stdcode);
      $('#tb11').val(phone1);
      $('#tb12').val(phone2);
      $('#tb13').val(phone3);
      $('#tb14').val(fax);
      $('#tb15').val(telex);
      $('#tb16').val(vfrom);
      $('#tb17').val(vto);
      $('#tb18').val(asrturef);
      $('#tb19').val(asrtudt);
      $('#tb20').val(venrate);
      $('#tb21').val(acode);
      $('#tb22').val(tinno);
      $('#tb23').val(panno);
      $('#tb24').val(gstin);

      }

      

    }

          $('#streupd').click(function() {
             form = $("#stre_mgmt").serialize();
     $.ajax({
       type: "POST",
       url: base_url+"inventory/Store/proced",
       data: form,
       success: function(data){
                      if(data=='2')
                {
                    // alert(data);
                      alert("Store Updated Successfully");
                      location.reload();
                }
                else if(data=='1')
                {
                      alert("Store Inserted Successfully");
                      location.reload();
                }
       }

     });
     event.preventDefault();
     return false;  //stop the actual form post !important!
  });
    // ***** data insert and update Ending 

    // ***** Store Remove 
      function strermv(id){
        var r=confirm("Are you sure want to Delete the Store!");
    if (r)
    {
          $.post(base_url+'inventory/Store/rmvstre/'+id, function(data) {
      if(data == "0")
      {
          alert("Store removed Successfully");
          location.reload();
      }
   });  
    }
}
    // ***** Store Remove Ending 

// ***** Store Management Ending *****

// ***** Work code Management *****


    // ***** data insert and update 
    function wrkins(){
      $('#wrkfrmupdte').show();
      $('#rwid').val('');
      $('#usr1txt').val('');
      $('#usrqtxt').val('');
      $('#usr2txt').val('');
      $('#usr3txt').val('');
      $('#nom1txt').val('');

    }

    function wrkupdte(id,usr1,usrq,usr2,usr3,nom1){
      // alert(id+'/'+usr1+'/'+usrq+'/'+usr2+'/'+usr3+'/'+nom1);

      if (id!="") {

        $('#wrkfrmupdte').show();
// ,,,,,,gstin
      $('#rwid').val(id);
      $('#usr1txt').val(usr1);
      $('#usrqtxt').val(usrq);
      $('#usr2txt').val(usr2);
      $('#usr3txt').val(usr3);
      $('#nom1txt').val(nom1);
      }

      

    }

          $('#wrkupd').click(function() {
             form = $("#wrk_cde").serialize();
     $.ajax({
       type: "POST",
       url: base_url+"inventory/Work/proced",
       data: form,
       success: function(data){
                      if(data=='2')
                {
                    // alert(data);
                      alert("Work code Updated Successfully");
                      location.reload();
                }
                else if(data=='1')
                {
                      alert("Work code Inserted Successfully");
                      location.reload();
                }
       }

     });
     event.preventDefault();
     return false;  //stop the actual form post !important!
  });
    // ***** data insert and update Ending 

    // ***** Work code Remove 
      function wrkrmv(id){
        var r=confirm("Are you sure want to Delete the Work code!");
    if (r)
    {
          $.post(base_url+'inventory/Work/rmvwrk/'+id, function(data) {
      if(data == "0")
      {
          alert("Work code removed Successfully");
          location.reload();
      }
   });  
    }
}
    // ***** Work code Remove Ending 

// ***** Work code Management Ending *****




    

    // // alert("hello world");
    //   $.ajax({
    //      type: "GET",
    //      dataType:"html",
    //      url:base_url+"IndentPurchase/getdata/" + $("#mysearch").val(),
    //      success:function(result){
    //      $("#searchresults").html(result);
    //   }});

