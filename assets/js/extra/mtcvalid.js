

// ***** Activity Management form Validation *****
$("#actvitymgmtins").validate({
      rules: {
        actvcd:{
          required:true,
          // aphanum:true,
        },
        actvnme:{
          required:true,
          // alpha:true,
        },
    },
    errorPlacement: function(error, element) 
        {
          // This is the default behavior
          //error.appendTo( element.parents('.errorshow') );
          error.insertAfter( element.parents('.form-group'));
        },
             onfocusout: function(element) {
            this.element(element);
        },
   });
// ***** Activity Management form Validation Ending *****


// ***** First form Validation *****
$("#actvitymgmt").validate({
      rules: {
        actvcd:{
          required:true,
          // aphanum:true,
        },
        actvnme:{
          required:true,
          // alpha:true,
        },
    },
    errorPlacement: function(error, element) 
        {
          // This is the default behavior
          //error.appendTo( element.parents('.errorshow') );
          error.insertAfter( element.parents('.form-group'));
        },
             onfocusout: function(element) {
            this.element(element);
        },
   });
// ***** First form Validation Ending *****
        
// ***** Second form Validation *****        
$('#addrreg').validate({
      rules: {
        stret:{
          required:true,
          // aphanum:true,
        },
        loc:{
          required:true,
          // aphanum:true,
        },
        lndmrk:{
          required:true,
          // aphanum:true,
        },
        cty:{
          required:true,
          // alpha:true,
        },
        distrct:{
          required:true,
          // alpha:true,
        },
        state:{
          required:true,
          // alpha:true,
        },
        pin:{
          required:true,
          // number:true,
          maxlength:6,
        },
        slct:{
          required:true,
          select:true,
        }

      },
      // messages: {
      //   stret:  "Enter the Street",
      //   loc: "Enter the Location",
      //   lndmrk: "Enter the Landmark",
      //   cty: "Enter the City",
      //   distrct: "Enter the District",
      //   state: "Enter the State",
      //   pin: "Enter the Pincode",
      //   slct: "Select the Country",
      //   //description: "Enter Description"
      // },
       errorPlacement: function(error, element) 
        {
          // This is the default behavior
          //error.appendTo( element.parents('.errorshow') );
          error.insertAfter( element.parents('.form-group'));
        },
             onfocusout: function(element) {
            this.element(element);
        },
});
// ***** Second form Validation Ending *****     


// ***** Second form Validation *****        
$('#addrreg1').validate({
      rules: {
        stret:{
          required:true,
          // aphanum:true,
        },
        loc:{
          required:true,
          // aphanum:true,
        },
        lndmrk:{
          required:true,
          // aphanum:true,
        },
        cty:{
          required:true,
          // alpha:true,
        },
        distrct:{
          required:true,
          // alpha:true,
        },
        state:{
          required:true,
          // alpha:true,
        },
        pin:{
          required:true,
          // number:true,
          maxlength:6,
        },
        slct:{
          required:true,
          select:true,
        }

      },
      // messages: {
      //   stret:  "Enter the Street",
      //   loc: "Enter the Location",
      //   lndmrk: "Enter the Landmark",
      //   cty: "Enter the City",
      //   distrct: "Enter the District",
      //   state: "Enter the State",
      //   pin: "Enter the Pincode",
      //   slct: "Select the Country",
      //   //description: "Enter Description"
      // },
       errorPlacement: function(error, element) 
        {
          // This is the default behavior
          //error.appendTo( element.parents('.errorshow') );
          error.insertAfter( element.parents('.form-group'));
        },
             onfocusout: function(element) {
            this.element(element);
        },
});
// ***** Second form Validation Ending *****     



// *****Capital goods Insert Third form Validation *****
$("#cap_goods_cod1").validate({
      rules: {
        descrip:{
          required:true,
          // aphanum:true,
        },
        folgrp:{
          required:true,
          // alpha:true,
        },

    },

      messages: {
        descrip: "Enter the Description",
        folgrp: "Enter the Folio Group",
      },

    errorPlacement: function(error, element) 
        {
          // This is the default behavior
          //error.appendTo( element.parents('.errorshow') );
          error.insertAfter( element.parents('.form-group'));
        },
             onfocusout: function(element) {
            this.element(element);
        },
   });
// *****Capital goods Insert Third form Validation Ending*****


// ***** Third form Validation *****
// $("#cap_goods_cod").validate({
//       rules: {
//         descrip:{
//           required:true,
//           // aphanum:true,
//         },
//         folgrp:{
//           required:true,
//           // alpha:true,
//         },

//     },

//       messages: {
//         descrip: "Enter the Description",
//         folgrp: "Enter the Folio Group",
//       },

//     errorPlacement: function(error, element) 
//         {
//           // This is the default behavior
//           //error.appendTo( element.parents('.errorshow') );
//           error.insertAfter( element.parents('.form-group'));
//         },
//              onfocusout: function(element) {
//             this.element(element);
//         },
//    });
// ***** Third form Validation Ending *****

// ***** Fourth form Validation *****
$("#ctrl_mgmt").validate({
      rules: {
        descrip:{
          required:true,
          // aphanum:true,
        },
        type:{
          required:true,
          // alpha:true,
        },
        folgrp:{
          required:true
        }

    },

      messages: {
        descrip: "Enter the Description",
        type: "Enter the Type",
        folgrp: "Enter the Folio Group",
      },

    errorPlacement: function(error, element) 
        {
          // This is the default behavior
          //error.appendTo( element.parents('.errorshow') );
          error.insertAfter( element.parents('.form-group'));
        },
             onfocusout: function(element) {
            this.element(element);
        },
   });
// ***** Fourth form Validation Ending *****

// ***** Fourth form Validation *****
$("#ctrl_mgmt1").validate({
      rules: {
        descrip:{
          required:true,
          // aphanum:true,
        },
        type:{
          required:true,
          // alpha:true,
        },
        folgrp:{
          required:true
        }

    },

      messages: {
        descrip: "Enter the Description",
        type: "Enter the Type",
        folgrp: "Enter the Folio Group",
      },

    errorPlacement: function(error, element) 
        {
          // This is the default behavior
          //error.appendTo( element.parents('.errorshow') );
          error.insertAfter( element.parents('.form-group'));
        },
             onfocusout: function(element) {
            this.element(element);
        },
   });
// ***** Fourth form Validation Ending *****


// ***** Fifth form Validation *****
// $("#delvr_trm_mgmt").validate({
//       rules: {
//         descrip:{
//           required:true,
//           // aphanum:true,
//         },
//         nme:{
//           required:true,
//           select:true,
//         },

//     },

//       messages: {
//         descrip: "Enter the Description",
//         type: "Enter the Name",
//       },

//     errorPlacement: function(error, element) 
//         {
//           // This is the default behavior
//           //error.appendTo( element.parents('.errorshow') );
//           error.insertAfter( element.parents('.form-group'));
//         },
//              onfocusout: function(element) {
//             this.element(element);
//         },
//    });
// ***** Fifth form Validation Ending *****

// ***** Sixth form Validation *****
// $("#depot_mgmt1").validate({
//       rules: {
//         depid:{
//           required:true,
//           aphanum:true
//         },
//         depnme:{
//           required:true,
//           // alpha:true
//         },
//         // depopt:{
//         //   required:true,
//         //   // alpha:true
//         // }

//     },

//     errorPlacement: function(error, element) 
//         {
//           // This is the default behavior
//           //error.appendTo( element.parents('.errorshow') );
//           error.insertAfter( element.parents('.form-group'));
//         },
//              onfocusout: function(element) {
//             this.element(element);
//         },
//    });
// ***** Sixth form Validation Ending *****


// ***** Sixth form Validation *****
// $("#depot_mgmt").validate({
//       rules: {
//         depid:{
//           required:true,
//           aphanum:true
//         },
//         depnme:{
//           required:true,
//           // alpha:true
//         },
//         // depopt:{
//         //   required:true,
//         //   // alpha:true
//         // }

//     },

//     errorPlacement: function(error, element) 
//         {
//           // This is the default behavior
//           //error.appendTo( element.parents('.errorshow') );
//           error.insertAfter( element.parents('.form-group'));
//         },
//              onfocusout: function(element) {
//             this.element(element);
//         },
//    });
// ***** Sixth form Validation Ending *****


// ***** Seventh form Validation *****
// $("#dept_prch").validate({
//       rules: {
//         dep:{
//           required:true,
//           apha:true
//         },
//         nam:{
//           required:true,
//           alpha:true
//         },
//         nam1:{
//           required:true,
//           alpha:true
//         },
//         nam2:{
//           required:true,
//           alpha:true
//         },
//         nm:{
//           required:true,
//           alpha:true
//         },
//         alp:{
//           required:true,
//           alpha:true
//         }

//     },

//     errorPlacement: function(error, element) 
//         {
//           // This is the default behavior
//           //error.appendTo( element.parents('.errorshow') );
//           error.insertAfter( element.parents('.form-group'));
//         },
//              onfocusout: function(element) {
//             this.element(element);
//         },
//    });
// // ***** Seventh form Validation Ending *****


// ***** Eight th form Validation *****
$("#fleet").validate({
      rules: {
        fleetno:{
          required:true,
          // aphanum:true
        },
        deptid:{
          required:true,
          // aphanum:true
        },
        regno:{
          required:true,
          // number:true
        },
        typ:{
          required:true,
          // alpha:true
        },
        serv:{
          required:true,
          // alpha:true
        }

    },

    errorPlacement: function(error, element) 
        {
          // This is the default behavior
          //error.appendTo( element.parents('.errorshow') );
          error.insertAfter( element.parents('.form-group'));
        },
             onfocusout: function(element) {
            this.element(element);
        },
   });
// ***** Eight th form Validation Ending *****


// ***** Ninth form Validation *****
$("#folio").validate({
      rules: {
        grp:{
          required:true,
        },
        nom1:{
          required:true,
        },
        nom2:{
          required:true,
        },
        nom3:{
          required:true,
        },
        prt1:{
          required:true,
        },
        prt2:{
          required:true,
        },
        prt3:{
          required:true,
        },
        lyc1:{
          required:true,
        },
        lyc2:{
          required:true,
        },
        lyc3:{
          required:true,
        },
        typ:{
          required:true,
        },
        cls:{
          required:true,
        },
        ved:{
          required:true,
        },
        uom:{
          required:true,
        },
        maxi:{
          required:true,
        },
        mini:{
          required:true,
        },
        icod:{
          required:true,
        },
        rcod:{
          required:true,
        },
        rate:{
          required:true,
        },
        grp1:{
          required:true,
        },  
        stkcd:{
          required:true,
        },
        isscd:{
          required:true,
        },      
        nwfld:{
          required:true,
        },
    },

    errorPlacement: function(error, element) 
        {
          // This is the default behavior
          //error.appendTo( element.parents('.errorshow') );
          error.insertAfter( element.parents('.form-group'));
        },
             onfocusout: function(element) {
            this.element(element);
        },
   });
// ***** Ninth form Validation Ending *****

// ***** Tenth form Validation *****
$("#grp_mgmt").validate({
      rules: {
        grp:{
          required:true,
        },
        grpnme:{
          required:true,
        },

    },

    errorPlacement: function(error, element) 
        {
          // This is the default behavior
          //error.appendTo( element.parents('.errorshow') );
          error.insertAfter( element.parents('.form-group'));
        },
             onfocusout: function(element) {
            this.element(element);
        },
   });
// ***** Tenth form Validation Ending *****


// ***** Eleventh form Validation *****
$("#jobordr").validate({
      rules: {
        dt:{
          required:true,
        },
        typ:{
          required:true,
        },
        frm:{
          required:true,
        },
        bomno:{
          required:true,
        },
        fleet:{
          required:true,
        },
        chsno:{
          required:true,
        },
        engno:{
          required:true,
        },
        dtpn:{
          required:true,
        },
        manhr:{
          required:true,
        },
        grpnme:{
          required:true,
        },
        pmac:{
          required:true,
        },
        pmcc:{
          required:true,
        },
        grp:{
          required:true,
        },
        cmac:{
          required:true,
        },
        cmcc:{
          required:true,
        },
        dtcmp:{
          required:true,
        },
        lbcst:{
          required:true,
        },
        oh:{
          required:true,
        },
        cnt:{
          required:true,
        },
        totcst:{
          required:true,
        },

    },

    errorPlacement: function(error, element) 
        {
          // This is the default behavior
          //error.appendTo( element.parents('.errorshow') );
          error.insertAfter( element.parents('.form-group'));
        },
             onfocusout: function(element) {
            this.element(element);
        },
   });
// ***** Eleventh form Validation Ending *****

  
  // ***** Twelveth form Validation *****
$("#paymnt").validate({
      rules: {
        nme:{
          required:true,
        },
        descrp:{
          required:true,
        },

    },

    errorPlacement: function(error, element) 
        {
          // This is the default behavior
          //error.appendTo( element.parents('.errorshow') );
          error.insertAfter( element.parents('.form-group'));
        },
             onfocusout: function(element) {
            this.element(element);
        },
   });
// ***** Twelveth form Validation Ending *****



// ***** 13th form Validation *****
$("#status_mgmt").validate({
      rules: {
        styp:{
          required:true,
        },
        nam:{
          required:true,
        },
        name:{
          required:true,
        },
        name1:{
          required:true,
        },
        name2:{
          required:true,
        }

    },

    errorPlacement: function(error, element) 
        {
          // This is the default behavior
          //error.appendTo( element.parents('.errorshow') );
          error.insertAfter( element.parents('.form-group'));
        },
             onfocusout: function(element) {
            this.element(element);
        },
   });
// ***** 13th form Validation Ending *****


// ***** 14th form Validation *****
$("#stre_mgmt").validate({
      rules: {
        typ:{
          required:true,
        },
        nme:{
          required:true,
        },
        nme1:{
          required:true,
        },
        addr1:{
          required:true,
        },
        addr2:{
          required:true,
        },
        addr3:{
          required:true,
        },
        addr4:{
          required:true,
        },
        pincd:{
          required:true,
        },
        state:{
          required:true,
        },
        stdcde:{
          required:true,
        },
        phne1:{
          required:true,
        },
        phne2:{
          required:true,
        },name2:{
          required:true,
        },
        phne3:{
          required:true,
        },name2:{
          required:true,
        },
        fax:{
          required:true,
        },name2:{
          required:true,
        },
        telx:{
          required:true,
        },
        vfrm:{
          required:true,
        },
        vto:{
          required:true,
        },
        asrtrf:{
          required:true,
        },
        asrtudt:{
          required:true,
        },
        venrte:{
          required:true,
        },
        acde:{
          required:true,
        },
        tino:{
          required:true,
        },
        pan:{
          required:true,
        },
        gstin:{
          required:true,
        }


    },

    errorPlacement: function(error, element) 
        {
          // This is the default behavior
          //error.appendTo( element.parents('.errorshow') );
          error.insertAfter( element.parents('.form-group'));
        },
             onfocusout: function(element) {
            this.element(element);
        },
   });
// ***** 14th form Validation Ending *****


// ***** 15th form Validation *****
$("#tyre").validate({
      rules: {
        cde:{
          required:true,
        },
        fol:{
          required:true,
        },
        tocde:{
          required:true,
        },
        dte:{
          required:true,
        },
        mtcno:{
          required:true,
        },
        manf:{
          required:true,
        },
        mak:{
          required:true,
        }

    },

    errorPlacement: function(error, element) 
        {
          // This is the default behavior
          //error.appendTo( element.parents('.errorshow') );
          error.insertAfter( element.parents('.form-group'));
        },
             onfocusout: function(element) {
            this.element(element);
        },
   });
// ***** 15th form Validation Ending *****


// ***** 16th form Validation *****
$("#wrk_cde").validate({
      rules: {
        usr1:{
          required:true,
        },
        usrq:{
          required:true,
        },
        usr2:{
          required:true,
        },
        usr3:{
          required:true,
        },
        nom1:{
          required:true,
        }

    },

    errorPlacement: function(error, element) 
        {
          // This is the default behavior
          //error.appendTo( element.parents('.errorshow') );
          error.insertAfter( element.parents('.form-group'));
        },
             onfocusout: function(element) {
            this.element(element);
        },
   });
// ***** 16th form Validation Ending *****


             $.validator.addMethod("select",
               function(){
               return $('').val()!="none";
            });

            

          $.validator.addMethod("number",
               function(value,element){
                     return /^[0-9]+$/.test(value);
               },
               "Allowed number value only");

          $.validator.addMethod("alpha",
               function(value,element){
                     return /^[a-z A-Z]+$/.test(value);
               },
               "Allowed Alphets only");


           $.validator.addMethod("aphanum",
               function(value,element){
                     return /^[0-9a-zA-Z]+$/.test(value);
               },
               "Allowed alphanumeric only");